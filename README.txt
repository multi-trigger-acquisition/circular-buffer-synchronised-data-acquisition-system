Author : Pawel Kozlowski
Supervisor : Alexandre Lasheen
Affiliation : CERN and Warsaw University of Technology


The following repository should be located in the default ADQAPI folder with examples to fully utilise spdevices library:
ADQAPI_python\FWDAQ

I was trying to fit the whole code for a program in one file for simplicity. Nevertheless in some cases object oriented approach has been taken
so that the code can be easily split into many files.

The repo contains the following scripts:

ADQ14_streamingPawel.py - A script for acquisition of 3d gradient data by means of ADQ14 digitizer. It works like the following - at first 
			sets all the necessary registers to enable acquisiton with external trigger. Then it allocates space in memory
			to make an array contiguous - instead of list. The list degrades the performance so that it is not used as a main
			container. Nevertheless it can be used as a temporary data space for one packet from digitizer. Note that every packet
			from digitizer is of different size so it has to temporarily fit into elastic object. It doesn't have a high overhead
			as the size of one packet is not very big. This doesn't apply to the main container which aggregates data from all different
			packets for future plotting. There are three methods presented for aggregation of data one is appending every packet to a 
			main list, but as it is above mentioned the performance degrades rapidly from about 127 MB/s and diverges to zero. Then
			there is another solution - using loop to manually assign each sample of a packet to a big main previously preallocated
			array. The access to memory is improved, quite decent throughput has been obtained - 7MB/s and was stable. Unfortunately
			looping over numpy array is not highly efficient therefore some more optimizations were possible - for instance vectorisation
			of numpy arrays operations. C counterpart of Python loops is way faster. That's why numpy offers vectorisation features. 
			This approach is stable and efficient - up to  170MB/s (the max speed without copying the packet to array is up to 300MB/s)
			- highly dependent on the speed of data acquisition by digitizer. The mentioned speed of acquisition is driven by frequency of
			triggers, size and number of data frames.All the tests were done when using USB3.

ADQ14acquisition_pipe_final.py - At first the data is acquired by main process from digitizer. Then the data is streamed through
			multiprocessing pipe into another process - reader which is simply 2d image of situation in PS.
			The script extends ADQ14_streamingPawel.py by adding paralleism. Matplotlib is no more used for plotting
			because performance is not enough for real time plotting. PYQTgraph library is involved instead so that the 
			application is able to consume data fast enough. The solution is based on manual preparation of appropriate size
			matrices by producer. The size of matrix is for instance 2300x2300 (frames x samples). It has to be aggregated
			at first as the packet size varies over acquisition time. This operation turned out to have quite high overhead.
			The final throughput is about 4.3 MB/s and is stable.

ADQ14acquisition_pipe_enhanced_prototype.py - This script extends ADQ14acquisition_pipe_final.py. Similar improvement to the one
			in ADQ14_streamingPawel.py has been introduced namely vectorisation. Acquisition speed - up to 150 MB/s, 
			dependent on the consumption. The performance is improved by implicit use of c during assigning parts of 
			data to array (vectorisation).


circ_buffer_implementation.py - This script is supposed to be a base for another approach - one producer and many consumers. This is simulated environment 
				where a producer creates some dummy data and consumer consumes it. Each of them work in separate process. The data is shared 
				among the processes through multiprocessing rawArray object. It doesn't have any synchronisation mechanisms build-in, but that's
				fine because if there is only one producer one don't have to care about synchronisation of array itself. The only atomic 
				variable which is shared is a pointer on which the producer finished writing the last sample. As the shared array is expected
				to be large - few GB of memory it is not of a problem to guarantee correct work. The atomic variable - lockArrInd is used by 
				consumer to not to overtake the producer. This way one can have many consumers and the production can be controlled both by
				turning off some consumers or just decreasing number of data to be produced. This can be done by turning off triggers or decreasing
				size of data.

circ_buffer_gui_simulation.py - This script extends circ_buffer_implementation.py by adding a special monitoring of circular buffer. It not only used as functional
				testing but can also by helpful in the future to monitor the state of the buffer. The monitoring windows shows a clock like plot
				with hands of the clock corresponding to producer and consumer. Instead of hours the labels describe indices on which producer 
				produces the data and consumer consumes. Consumer should never takeover the producer. The script worked correctly. As complexity of 
				the script have increased I have decided to make it object oriented.

ADQ14acquisition_multiprocArr_fina.py - The basic change in comparison to circ_buffer_gui_simulation.py is that the data is no more dummy. The ADQ14 class which 
					inherits from Writer class acquires data and writes to shared memory. The monitoring process has been left. One more 
					consumer has been added  - the already used gui for fast image plotting - PyQt5 (used previously in ADQ14acquisition_pipe_final.py).
					As there were two consumers I could measure the influence of consumers on the acquisition rate. I didn't notice any.
					The producer is independent on the other consumers. Nevertheless the acquisition speed was not very high - about 20MB/s.
					This means that some more improvements can be done (even though it is enough for proper acquisition of few cycles with
					multitrigger approach). One of improvements to be tested is trying to vectorise modulo operation fr circular buffer or jus
					using circular buffer from boost library for c++. 

ADQ14acquisition_multiprocArr_fina_v2 - Animation of the plot added. This is the last prototyping version, the code was then converted to final version.

/DAQLongBeam/* - The final version of data acquisition system along the full supercycle. Object oriented and split into many files. Consumer for single
		and continuous writing to disk added.

/CppCircBuff/* - The folder contains implementation of circular buffer implemented in c++ with interface to python. It works but I was not able to 
		use it as shared memory container with multiprocessing python library. There is need to move multiprocessing to c++. This is already
		added to final solution but there is need for some more investigation if it is feasible to merge multiprocessing python and c++.

In all the scripts ADQ14 react on external multitrigger (what decreases significantly the amount of data).

ADQ14_Benchmark - the last script performs benchmark of the throughput of digitizer

/multiproc_tests/ - tests of multiprocessing/multithreading capabilities of python
