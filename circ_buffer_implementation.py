'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN

Script for acquisition and consumption of longitudinal beam profile data from Proton Synchrotron through ADQ14.

Note: The multiprocessing features sometimes doesn't work perfectly on Windows machines, the solution can be using PyCharm instead of
Spyder.
'''

from multiprocessing import Process, Value
import numpy as np
import time
import multiprocessing as mp
import random
import ctypes
import sys
import matplotlib.pyplot as plt

# import matplotlib.animation

'''
The process below is responsible for moving data from digitizer memory into circular buffer which is available for other processes.
The availability is due to the fact the the circular buffer is based on shared multiprocessing array. The multiprocessing array
should be wide enough to fully utilize local computer memory.
'''


def writer(lockArrInd, name, shared_array):
    nameP = name
    print(nameP)
    index = 0
    maxLen = len(shared_array)
    for x in range(0, 300):
        time.sleep(0.1)
        print('produce')
        # sys.stdout.flush()
        # generate random length of array to add
        sizeOfNewPacket = random.randint(5500, 5900)
        # check if the data shouldn't be passed to the beinning of the circular buffer (split between beginning and the end)
        if (index + sizeOfNewPacket <= maxLen):
            # the longer the parts of data the less overhead of slicing the data
            # write some new data
            # print(len(shared_array[index:index+sizeOfNewPacket]))
            # print(len(x))
            shared_array[index:index + sizeOfNewPacket] = [x] * len(shared_array[index:index + sizeOfNewPacket])
            index = index + sizeOfNewPacket
            # take lock and assign the new value
            with lockArrInd.get_lock():
                lockArrInd.value = index
        else:  # there is a need for slicing - the circular buffer is full, go to the beginning
            toNextIter = sizeOfNewPacket - (maxLen - index)
            # thisIter = sizeOfNewPacket-toNextIter
            # write some new data to fill the buffer to the end
            shared_array[index:maxLen] = [x] * len(shared_array[index:maxLen])
            # write the rest of new data to the beginning of the buffer
            shared_array[0:toNextIter] = [x] * len(shared_array[0:toNextIter])

            index = toNextIter
            # take lock and increment counter
            with lockArrInd.get_lock():
                lockArrInd.value = index


'''
The process below is simply a consumer of data produced by writer. The only information it receives is the index on which the
writer has written data lately. On the base of the information reader tries to catch up with writer but not overtake the writer.
This is a kind of consumption driven by writer. The data is consumed from shared circular buffer.
'''


def reader(lockArrInd, name, shared_array, noOfSamples, noOfRecords):
    '''
    Initialize variables necessary for the circular buffer.
    '''
    oneFrame = noOfSamples * noOfRecords
    data_16bit = np.zeros(oneFrame, dtype=np.int16)  # prefilled array
    data_16bit = np.ascontiguousarray(data_16bit, dtype=np.int16)  # making it contiguous
    lenArray = len(shared_array)
    nameP = name
    print(nameP)
    # index of the reader
    indexRead = 0

    '''
    Initialize variables needed to update animation of the buffer.
    '''
    # dot
    r = 0  # 90  * (np.pi/180)
    # the length of the arrow
    t = 50000
    # max as a rotation angle
    maxN = 2 * np.pi

    fig = plt.figure()
    ax = fig.gca(projection='polar')
    fig.canvas.set_window_title('Circular Buffer')
    ax.plot(r, t, color='b', marker='o', markersize='3')
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(-1)
    ax.set_ylim(0, 1.02 * t)
    # set labels
    label_pos = np.linspace(0.0, 2 * np.pi, 4, endpoint=False)
    ax.set_xticks(label_pos)
    label_cols = [0, lenArray / 4, lenArray / 2, lenArray - lenArray / 4]
    ax.set_xticklabels(label_cols, size=24)

    # defining two lines corresponding to writer and reader indices
    line1, = ax.plot([0, 0], [0, t], color='b', linewidth=1, label='Producer')
    line2, = ax.plot([0, 0], [0, t], color='r', linewidth=1, label='Consumer')
    ax.legend(loc=4)

    def updateWriter(angle):
        plt.pause(0.05)
        newvalue = (maxN) / (lenArray) * (angle - lenArray) + maxN
        line1.set_data([newvalue, newvalue], [0, t])
        return line1,

    def updateReader(angle):
        plt.pause(0.05)
        newvalue = (maxN) / (lenArray) * (angle - lenArray) + maxN
        line2.set_data([newvalue, newvalue], [0, 40000])
        return line2,

    plt.ion()
    plt.show()

    while True:
        # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
        time.sleep(0.1)
        print('trying to get lock')
        with lockArrInd.get_lock():
            # currIndex - index of the location in memory where the last produced samples are situated
            indexWrit = lockArrInd.value
            print('lock taken')
            updateWriter(indexWrit)

        # 1. check if there is enough samples for acquisition
        # if it is not split
        print('indexRead ', indexRead, 'indexWrit', indexWrit)
        if (indexRead <= indexWrit):
            print('not split')
            producedSamples = indexWrit - indexRead
            # 2. If produced samples are sufficient to proceed
            if (producedSamples > oneFrame):
                # consume them in an simple way
                # simple operation, just copy data from big buffer to a temporary one
                data_16bit[0:oneFrame] = shared_array[indexRead:indexRead + oneFrame]
                print('simple case')
                sys.stdout.flush()
                # increase the pointer of consumer to the array
                indexRead = indexRead + oneFrame
                updateReader(indexRead)
        # If the data is split
        if (indexRead > indexWrit):
            print('split')
            producedSamples = (lenArray - indexRead) + indexWrit
            # 2 If produced samples are sufficient to proceed
            if (producedSamples > oneFrame):
                # 3. check if the interesting part is split
                # if it is not the case
                print('interest. part split')
                predictedIndex = indexRead + oneFrame
                if (predictedIndex <= lenArray):
                    # proceed in the same way as before
                    data_16bit[0:oneFrame] = shared_array[indexRead:indexRead + oneFrame]
                    # increase the pointer of consumer to the array
                    indexRead = indexRead + oneFrame
                    updateReader(indexRead)
                    print('split buffer but array complete')
                # if so
                elif (predictedIndex > lenArray):
                    # first_part & second_part are indices
                    # find the first part - the end of the buffer/beginning of temporary array
                    first_part = lenArray - indexRead
                    print(first_part)
                    data_16bit[0:first_part] = shared_array[indexRead:lenArray]
                    # find the second part - the the beginning of the buffer/ the end of the temporary array
                    second_part = oneFrame - first_part
                    print('second_part', second_part)
                    data_16bit[first_part:oneFrame] = shared_array[0:second_part]
                    indexRead = second_part
                    updateReader(indexRead)
                    print('both buffer and array split')


if __name__ == '__main__':
    __spec__ = None
    # dimensions of record list
    noOfSamples = 100  # [samples]
    noOfRecords = 100  # [records]
    # shared variable index
    pointerToLastProduced = Value('i', 0)
    # big array of samples
    global ArrayOfSamples
    ArrayOfSamples = mp.RawArray(ctypes.c_ushort, 500000)
    # producer which writes  (Shared memory objects must be used as initialization arguments to the Process object.)
    # start prducer
    p1 = Process(target=writer, args=(pointerToLastProduced, 'writer', ArrayOfSamples))
    # start consumer
    p2 = Process(target=reader, args=(pointerToLastProduced, 'reader', ArrayOfSamples, noOfSamples, noOfRecords))
    print('started')
    p1.start()
    p2.start()
    p1.join()
    p2.join()

'''
Benchmarks
'''

# packetSize=10
# arraySize=90
# indexOfProducer=16
# index = 84
#
# start_time = time.time()
# for x in range(0, 300000):
#    (arraySize - index + indexOfProducer)%arraySize
# elapsed_time = time.time() - start_time
# print(elapsed_time)
# start_time = time.time()
#
#
##sligthly slower solution but better in this case because it directly splits into two different situations
# for x in range(0, 300000):
#    if(index+packetSize<=arraySize):
#        indexOfProducer-index
#    else:
#        arraySize-index + indexOfProducer
# elapsed_time = time.time() - start_time
# print(elapsed_time)

'''
Animation of circular buffer
'''
# https://stackoverflow.com/questions/47557130/matplotlib-line-rotation-or-animation

# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation
# import time
#
# #dot
# r = 0#90  * (np.pi/180)
# #the length of the arrow
# t = 50000
#
# fig = plt.figure()
# ax = fig.gca(projection = 'polar')
# fig.canvas.set_window_title('Circular Buffer')
# ax.plot(r, t, color ='b', marker = 'o', markersize = '3')
# ax.set_theta_zero_location('N')
# ax.set_theta_direction(-1)
# ax.set_ylim(0,1.02*t)
#
# line1, = ax.plot([0, 0],[0,t], color = 'b', linewidth = 1)
# line2, = ax.plot([0, 0],[0,t], color = 'r', linewidth = 1)
#
# def updateWriter(angle):
#     line1.set_data([angle, angle],[0,t])
#     return line1,
#
# def updateReader(angle):
#     line2.set_data([angle, angle],[0,40000])
#     return line2,
#
# frames = np.array([np.linspace(0,2*np.pi,1000),np.linspace(2*np.pi,0,1000)])
# print(np.shape(frames))
#
# plt.show()
#
# #plt.ion()
# #ani = matplotlib.animation.FuncAnimation(fig, update, frames=frames, blit=True, interval=10)
# for x in range(0, 500):
#     plt.pause(0.1)
#     for y in range(0,2):
#         if(y==0):
#             updateWriter(frames[y][x])
#         if(y==1):
#             updateReader(frames[y][x])
