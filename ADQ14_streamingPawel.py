<<<<<<< HEAD
<<<<<<< HEAD
#!/usr/bin/env python3
#
# Copyright 2015 Signal Processing Devices Sweden AB. All rights reserved.
#
# Description:    ADQ14 FWDAQ streaming example
# Documentation:
#
=======
>>>>>>> 2cb50c2... Initial commit
=======
>>>>>>> 2cb50c2d56c27eb6b8d6378ce508fe26ae7367eb
'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN, Warsaw University Of Technology

Disclaimer : the data acquisition part of the code was written and belongs to SP-Devices (Teledyne Signal Processing Devices Sweden AB).

Comments:
    
This mode is prefered if acquiring small records, the total size is higher than ADQ DRAM - DRAM not utilised!
Data rate has to be kept below the speed  of the read out process - with usb3 impossible to get a good quality
Record size has to be smaller than FPGA FIFO.
'''
import numpy as np
import ctypes as ct
import matplotlib.pyplot as plt
import sys
import time
import os

sys.path.insert(1, os.path.dirname(os.path.realpath(__file__))+'/..')
from modules.example_helpers import *

# Record settings
number_of_records  = 1# 900
samples_per_record = 2300*200000# 2300

# Plot data if set to True
plot_data = False
plot_data_gradient = True
store_data = True
# Print metadata in headers
print_headers = False

# DMA transfer buffer settings
transfer_buffer_size = 65536
num_transfer_buffers = 8

# DMA flush timeout in seconds
flush_timeout        = 0.5

# Load ADQAPI
ADQAPI = adqapi_load()

# Create ADQControlUnit
adq_cu = ct.c_void_p(ADQAPI.CreateADQControlUnit())

# Enable error logging from ADQAPI
ADQAPI.ADQControlUnit_EnableErrorTrace(adq_cu, 3, '.')

# Find ADQ devices
ADQAPI.ADQControlUnit_FindDevices(adq_cu)
n_of_ADQ  = ADQAPI.ADQControlUnit_NofADQ(adq_cu)
print('Number of ADQ found:  {}'.format(n_of_ADQ))

# Exit if no devices were found
if n_of_ADQ < 1:
    print('No ADQ connected.')
    ADQAPI.DeleteADQControlUnit(adq_cu)
    adqapi_unload(ADQAPI)
    sys.exit(1)

# Select ADQ
if n_of_ADQ > 1:
    adq_num = int(input('Select ADQ device 1-{:d}: '.format(n_of_ADQ)))
else:
    adq_num = 1

print_adq_device_revisions(ADQAPI, adq_cu, adq_num)

# Set clock source
ADQ_CLOCK_INT_INTREF = 0
ADQAPI.ADQ_SetClockSource(adq_cu, adq_num, ADQ_CLOCK_INT_INTREF)

# Maximum number of channels for ADQ14 FWPD is four but we use only one
max_number_of_channels = 1

# Setup test pattern
#     0 enables the analog input from the ADCs
#   > 0 enables a specific test pattern
# Note: Default is to enable a test pattern (4) and disconnect the
#       analog inputs inside the FPGA.
ADQAPI.ADQ_SetTestPatternMode(adq_cu, adq_num, 0)

'''
Set gain and offset.
unsigned int SetGainAndOﬀset (unsigned char Channel, int Gain, int Oﬀset)
'''
ch=1
ADQAPI.ADQ_SetGainAndOffset(adq_cu, adq_num,ch,
                                         1024,
                                         0)
'''
Set bias offset - The DC-biasing circuitry is heavily ﬁltered, and a change in bias level will typically take around 3-4
seconds to take eﬀect. Also limits settling time to 100ms.
'''
adjustable_bias = -17000#-32768#-32768#14000 # Codes
ADQAPI.ADQ_SetAdjustableBias(adq_cu, adq_num,1, adjustable_bias)


# Set trig mode
SW_TRIG = 1
EXT_TRIG_1 = 2
EXT_TRIG_2 = 7
EXT_TRIG_3 = 8
LVL_TRIG = 3
INT_TRIG = 4
LVL_FALLING = 0
LVL_RISING = 1
trig_type = EXT_TRIG_1
success = ADQAPI.ADQ_SetTriggerMode(adq_cu, adq_num, trig_type)
if (success == 0):
    print('ADQ_SetTriggerMode failed.')
success = ADQAPI.ADQ_SetLvlTrigLevel(adq_cu, adq_num, 0)
if (success == 0):
    print('ADQ_SetLvlTrigLevel failed.')
success = ADQAPI.ADQ_SetTrigLevelResetValue(adq_cu, adq_num, 1000)
if (success == 0):
    print('ADQ_SetTrigLevelResetValue failed.')
success = ADQAPI.ADQ_SetLvlTrigChannel(adq_cu, adq_num, 1)
if (success == 0):
    print('ADQ_SetLvlTrigChannel failed.')
success = ADQAPI.ADQ_SetLvlTrigEdge(adq_cu, adq_num, LVL_RISING)
if (success == 0):
    print('ADQ_SetLvlTrigEdge failed.')

# Setup acquisition
channels_mask = 0x1
# noOfHoldOffSamples may be useful to shift every acquisition for some samplesto center on the beam profile
noOfHoldOffSamples = 0
# may be useful when triggering on the threshold of the beam profile
noOfPreTriggerSamples = 0
ADQAPI.ADQ_TriggeredStreamingSetup(adq_cu, adq_num, number_of_records, samples_per_record, noOfPreTriggerSamples, noOfHoldOffSamples, channels_mask)

success = ADQAPI.ADQ_SetStreamStatus(adq_cu, adq_num, 1);
if (success == 0):
    print('setting stream status to 1 (turning on) failed.')
#success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 1,0);
#if (success == 0):
#    print('using DRAM as FIFO failed')
#success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 2,1);
#if (success == 0):
#    print('turning off headers - setting to raw data failed')
#success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 3,1);
#if (success == 0):
#    print('setting the stream channel mask failed')

# Get number of channels from device
number_of_channels = 1 # ADQAPI.ADQ_GetNofChannels(adq_cu, adq_num)

# Setup size of transfer buffers
print('Setting up streaming...')
ADQAPI.ADQ_SetTransferBuffers(adq_cu, adq_num, num_transfer_buffers, transfer_buffer_size)

# Start streaming
print('Collecting data, please wait...')
ADQAPI.ADQ_StopStreaming(adq_cu, adq_num)
ADQAPI.ADQ_StartStreaming(adq_cu, adq_num)

# Allocate target buffers for intermediate data storage one buffer for one channel
# 
Int16Arr = ct.c_int16 * transfer_buffer_size
# pointer to the array
Int16ArrPtr = ct.POINTER(Int16Arr)
# array of pointers (of buffers)
Int16ArrPtrArr = Int16ArrPtr * number_of_channels
# new type is defined (human readable name)
BUFFERS = Int16ArrPtrArr
# instantiate the type  
target_buffers = BUFFERS()
print('len of target buffers', len(target_buffers))

for bufp in target_buffers:
    bufp.contents = (ct.c_int16*transfer_buffer_size)()


# Create some buffers for the full records
data_16bit = [np.array([], dtype=np.int16)]

## 2D array of 
## Allocate target buffers for headers
#headerbuf_list = [(HEADER*number_of_records)()]
## Create an C array of pointers to header buffers (by ensuring a space)
#headerbufp_list = ((ct.POINTER(HEADER*number_of_records)))()

# create array of headers
HeadArr = HEADER*number_of_records

# Allocate target buffers for headers
# instantiate header array
HeaderArray = HeadArr()
headerbuf_list = [HeaderArray for ch in range(number_of_channels)]

# Create an C array of pointers to header buffers
# create pointer to the array
HeadArrPtr = ct.POINTER(HeadArr)
# create array of pointers to headers
HeadArrPtrArr = HeadArrPtr * number_of_channels
# new type is defined (human readable name)
HEADER_BUFFERS = HeadArrPtrArr
# instantiate the type  
headerbufp_list = HEADER_BUFFERS()
# Initiate pointers with allocated header buffers
for ch,headerbufp in enumerate(headerbufp_list):
    headerbufp.contents = headerbuf_list[ch]

# Create a second level pointer to each buffer pointer,
# these will only be used to change the bufferp_list pointer values
headerbufvp_list = [ct.cast(ct.pointer(headerbufp_list[ch]), ct.POINTER(ct.c_void_p)) for ch in range(number_of_channels)]
        


# Allocate length output variable
# make a space for 4 elements and populate the elements
# create type
samples_added = (1*ct.c_uint)()
# populate the type
for ind in range(len(samples_added)):
  samples_added[ind] = 0

headers_added = (1*ct.c_uint)()
for ind in range(len(headers_added)):
  headers_added[ind] = 0

header_status = (1*ct.c_uint)()
for ind in range(len(header_status)):
  header_status[ind] = 0

# Generate triggers if software trig is used
# why the trigger is not armed beforehand?
#if (trig_type == 1):
#  for trig in range(number_of_records):
#    ADQAPI.ADQ_SWTrig(adq_cu, adq_num)
#
#print('Waiting for data...')

# Collect data until all requested records have been recieved
records_completed = 0
headers_completed = 0
records_completed_cnt = 0
ltime = time.time()
buffers_filled = ct.c_uint(0)


# Preallocate memory
entries = range(number_of_records*samples_per_record) # 1 million entries
data_16bit = np.zeros((len(entries)),dtype=np.int16) # prefilled array
data_16bit = np.ascontiguousarray(data_16bit, dtype=np.int16) #making it contiguous
# the general counter used when filling the data buffer
general_counter = 0





# Read out data until records_completed for ch A is number_of_records
while (number_of_records > records_completed):
  buffers_filled.value = 0
  collect_result = 1
  poll_time_diff_prev = time.time()
  # Wait for next data buffer
  while ((buffers_filled.value == 0) and (collect_result)):
    collect_result = ADQAPI.ADQ_GetTransferBufferStatus(adq_cu, adq_num,
                                                        ct.byref(buffers_filled))
    poll_time_diff = time.time()

    if ((poll_time_diff - poll_time_diff_prev) > flush_timeout):
      # Force flush
      print('No data for {}s, flushing the DMA buffer.'.format(flush_timeout))
      status = ADQAPI.ADQ_FlushDMA(adq_cu, adq_num);
      print('ADQAPI.ADQ_FlushDMA returned {}'.format(adq_status(status)))
      poll_time_diff_prev = time.time()


  # Fetch data and headers into target buffers
  status = ADQAPI.ADQ_GetDataStreaming(#  adq_cu, adq_num - handles to the device cu - control unit
                                       adq_cu, adq_num,
                                       target_buffers,
                                       #  The data and metadata is delivered to the user buﬀers: target_buﬀers and target_headers, respectively.
                                       headerbufp_list,
                                       #  suppress data collection from specific channels
                                       channels_mask,
                                       # byref works almost like pointer() , but with when we dont want to access it from python
                                       # samples_added - how many samples that were added to the user data buffers
                                       # This allows the user to
                                       # increment the data buﬀer pointers accordingly before the next call to the GetDataStreaming() function.
                                       ct.byref(samples_added),
                                       # headers_added and header_status provide the user with information to determine if the header
                                       # buﬀer pointers should be incremented.
                                       # The parameter headers_added contains the number of initialized
                                       # headers as a result of this call to GetDataStreaming()
                                       ct.byref(headers_added),
                                       ct.byref(header_status))
                                       # The header_status parameter is used to denote if the
                                       # last header is complete or not. E.g. a return value of headers_added = 4 and header_status = 1 signiﬁes 4
                                       # complete headers, while headers_added = 4 and header_status = 0 would signify 3 complete headers and one
                                       # incomplete header. 
  
  
  
  if status == 0:
    print('GetDataStreaming failed!')
    sys.exit()
  ch=0
  # for ch in range(number_of_channels):
  if (headers_added[ch] > 0):
      # The last call to GetDataStreaming has generated header data
      if (header_status[ch]):
        headers_done = headers_added[ch]
      else:
        # One incomplete header
        headers_done = headers_added[ch]-1
      # Update counter counting completed records
      headers_completed += headers_done

      # Update the number of completed records if at least one header has completed
      if (headers_done > 0):
        records_completed = headerbuf_list[ch][headers_completed-1].RecordNumber + 1

      # Update header pointer so that it points to the current header
      headerbufvp_list[ch].contents.value += headers_done*ct.sizeof(headerbuf_list[ch]._type_)
      if (headers_done > 0) and (records_completed-records_completed_cnt) > 1000:
          dtime = time.time()-ltime
          if (dtime > 0):
              print('{:d} {:.2f} MB/s'.format(records_completed,
                    ((samples_per_record
                      *2
                      *(records_completed-records_completed_cnt))
                      /(dtime))/(1024*1024)))
          sys.stdout.flush()
          records_completed_cnt = records_completed
          ltime = time.time()

  if (samples_added[ch] > 0):
      # Copy channel data to continuous buffer
      #data_buf          = np.frombuffer(target_buffers[ch].contents, dtype=np.int16, count=samples_added[ch])
      # print(len(data_buf))
      # highly uneffective 
      '''
      Appending is highly uneffective - drops from 127 MB/s and fastly diverges to zero
      '''
      # data_16bit    = np.append(data_16bit, data_buf)
      '''
      This way is stable but also inefficient (numpy interpreted - dont use loops) about 7MB/s
      '''
      # for idx, entry in enumerate(data_buf):
          #data_16bit[general_counter] = entry
          #general_counter=general_counter+1
      '''
      This way is stable and efficient - up to  170MB/s (the max without copying the data to array is up to 300MB/s) - highly dependent on the speed of data acquisition by digitizer!
      Python for loops are inherently slower than their C counterpart.
      This is why numpy offers vectorized actions on numpy arrays. 
      It pushes the for loop you would usually do in Python down to the C level, 
      which is much faster. numpy offers vectorized ("C level for loop")
      '''   
      data_16bit[general_counter:general_counter+samples_added[ch]] = np.frombuffer(target_buffers[ch].contents, dtype=np.int16, count=samples_added[ch])
      general_counter=general_counter+samples_added[ch]

print(len(data_16bit))

# Stop streaming
ADQAPI.ADQ_StopStreaming(adq_cu, adq_num)

# Print recieved headers
if print_headers:
    if number_of_records > 0:
#      print('------------------')
##      print('Headers channel {}'.format(ch))
#      print('------------------')
      for rec in range(number_of_records):
        header = headerbuf_list[ch][rec]
#        print('RecordStatus:  {}'.format(header.RecordStatus))
#        print('UserID:        {}'.format(header.UserID))
#        print('SerialNumber:  {}'.format(header.SerialNumber))
#        print('Channel:       {}'.format(header.Channel))
#        print('DataFormat:    {}'.format(header.DataFormat))
#        print('RecordNumber:  {}'.format(header.RecordNumber))
        print('Timestamp:     {} ns'.format(header.Timestamp * 0.125))
        print('RecordStart:   {} ns'.format(header.RecordStart * 0.125))
        print('SamplePeriod:  {} ns'.format(header.SamplePeriod * 0.125))
        print('RecordLength:  {} ns'.format(header.RecordLength * (header.SamplePeriod* 0.125)))
#        print('------------------')

# Plot data
if plot_data:
    if number_of_records > 0:
      widths = np.array([], dtype=np.uint32)
      record_end_offset = 0
      # Extract record lengths from headers
      for rec in range(number_of_records):
        header = headerbuf_list[ch][rec]
        widths = np.append(widths, header.RecordLength)

      # Get new figure
      plt.figure(ch)
      plt.clf()
      # Plot data
      plt.plot(data_16bit.T, '.-')
      # Set window title
      plt.gcf().canvas.set_window_title('Channel {}'.format(ch))
      # Set grid mode
      plt.grid(which='Major')
      # Mark records in plot
      alternate_background(plt.gca(), 0, widths, labels=True)
      # Show plot
      plt.show()

if plot_data_gradient:
    if number_of_records > 0:
        reshapedData=data_16bit.T.reshape(number_of_records, samples_per_record)
        print(type(reshapedData))
        print(np.shape(reshapedData))
        print(reshapedData)
        fig = plt.figure('inner plot 1')
        plt.clf()
        plt.pause(1)
        ###fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set_aspect('equal')
        plt.imshow(reshapedData, interpolation='nearest', cmap=plt.cm.ocean, aspect='auto', origin ='bottom')
        plt.colorbar()
        plt.tight_layout()
        #plt.show()
        #fig.savefig('gradient.png')   # save the figure to file
        plt.pause(1)
        plt.show()
        #plt.close(fig)    # close the figure

if store_data:
    if number_of_records > 0:
        import datetime
        FileName='rapidStream'
        # print('len of acq arr ', len(data))
        dirname = os.path.dirname(__file__)
        UnixTime = time.time()
        dateX = datetime.datetime.fromtimestamp(UnixTime).strftime('%Y-%m-%d_%H-%M-%S')
        name = FileName + '_' + str(dateX)
        filename = os.path.join(dirname, '/StoredData/', name)
        pathset = os.path.join(dirname, "/StoredData/")
        # check the directory does not exist
        if not (os.path.exists(pathset)):
            # create the directory you want to save to
            os.mkdir(pathset)
        np.save(filename, data_16bit)



# Delete ADQ device handle
ADQAPI.ADQControlUnit_DeleteADQ(adq_cu, adq_num)
# Delete ADQControlunit
ADQAPI.DeleteADQControlUnit(adq_cu)

print('Done.')