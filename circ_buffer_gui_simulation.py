'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN, Warsaw University Of Technology

Script for acquisition and consumption of longitudinal beam profile data from Proton Synchrotron through ADQ14. Object oriented version.

Note: The multiprocessing features sometimes doesn't work perfectly on Windows machines, the solution can be using PyCharm instead of
Spyder.
'''

from multiprocessing import Process, Value
import numpy as np
import time
import multiprocessing as mp
import random
import ctypes
import sys
import matplotlib.pyplot as plt
# import matplotlib.animation
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg

'''
The process below is responsible for moving data from digitizer memory into circular buffer which is available for other processes.
The availability is due to the fact the the circular buffer is based on shared multiprocessing array. The multiprocessing array
should be wide enough to fully utilize local computer memory.
'''


class Writer(Process):

    def __init__(self, lockArrInd, name, shared_array):
        super(Writer, self).__init__()
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array

    def run(self):
        """Build some CPU-intensive tasks to run via multiprocessing here."""
        print(self.name)
        index = 0
        maxLen = len(self.shared_array)
        for x in range(0, 300):
            time.sleep(0.1)
            # print('produce')
            # sys.stdout.flush()
            # generate random length of array to add
            sizeOfNewPacket = random.randint(18234, 23432)
            # check if the data shouldn't be passed to the beinning of the circular buffer (split between beginning and the end)
            if (index + sizeOfNewPacket <= maxLen):
                # the longer the parts of data the less overhead of slicing the data
                # write some new data
                # print(len(shared_array[index:index+sizeOfNewPacket]))
                # print(len(x))
                self.shared_array[index:index + sizeOfNewPacket] = [random.randint(1000, 16000)] * len(
                    self.shared_array[index:index + sizeOfNewPacket])
                index = index + sizeOfNewPacket
                # take lock and assign the new value
                with self.lockArrInd.get_lock():
                    self.lockArrInd.value = index
            else:  # there is a need for slicing - the circular buffer is full, go to the beginning
                toNextIter = sizeOfNewPacket - (maxLen - index)
                # thisIter = sizeOfNewPacket-toNextIter
                # write some new data to fill the buffer to the end
                self.shared_array[index:maxLen] = [x] * len(self.shared_array[index:maxLen])
                # write the rest of new data to the beginning of the buffer
                self.shared_array[0:toNextIter] = [x] * len(self.shared_array[0:toNextIter])

                index = toNextIter
                # take lock and increment counter
                with self.lockArrInd.get_lock():
                    self.lockArrInd.value = index


'''
The process below is simply a consumer of data produced by writer. The only information it receives is the index on which the
writer has written data lately. On the base of the information reader tries to catch up with writer but not overtake the writer.
This is a kind of consumption driven by writer. The data is consumed from shared circular buffer.
'''


class Reader(Process):

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords):
        super(Reader, self).__init__()
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords

    def run(self):
        # initialisation done
        print(self.name)


# The following class extends the reader class therefore it can use the default Reader interface
class ReaderPerf(Reader):

    def run(self):
        """Build some CPU-intensive tasks to run via multiprocessing here."""
        print('Performance analysis of circular buffer')

        '''
        Initialize variables necessary for the circular buffer.
        '''
        oneFrame = self.noOfSamples * self.noOfRecords
        data_16bit = np.zeros(oneFrame, dtype=np.int16)  # prefilled array
        data_16bit = np.ascontiguousarray(data_16bit, dtype=np.int16)  # making it contiguous
        lenArray = len(self.shared_array)
        # index of the reader
        indexRead = 0

        '''
        Initialize variables needed to update animation of the buffer.
        '''
        # dot
        r = 0  # 90  * (np.pi/180)
        # the length of the arrow
        t = 50000
        # max as a rotation angle
        maxN = 2 * np.pi

        fig = plt.figure()
        ax = fig.gca(projection='polar')
        fig.canvas.set_window_title('Circular Buffer')
        ax.plot(r, t, color='b', marker='o', markersize='3')
        ax.set_theta_zero_location('N')
        ax.set_theta_direction(-1)
        ax.set_ylim(0, 1.02 * t)
        ax.set_yticklabels([])
        # set labels
        label_pos = np.linspace(0.0, 2 * np.pi, 4, endpoint=False)
        ax.set_xticks(label_pos)
        label_cols = [0, lenArray / 4, lenArray / 2, lenArray - lenArray / 4]
        ax.set_xticklabels(label_cols, size=24)

        # defining two lines corresponding to writer and reader indices
        line1, = ax.plot([0, 0], [0, t], color='b', linewidth=1, label='Producer')
        line2, = ax.plot([0, 0], [0, t], color='r', linewidth=1, label='Consumer')
        ax.legend(loc=4)


        def updateWriter(angle):
            plt.pause(0.05)
            newvalue = (maxN) / (lenArray) * (angle - lenArray) + maxN
            line1.set_data([newvalue, newvalue], [0, t])
            return line1,

        def updateReader(angle):
            plt.pause(0.05)
            newvalue = (maxN) / (lenArray) * (angle - lenArray) + maxN
            line2.set_data([newvalue, newvalue], [0, 40000])
            return line2,

        plt.ion()
        plt.show()

        while True:
            # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
            time.sleep(0.1)
            # print('trying to get lock')
            with self.lockArrInd.get_lock():
                # currIndex - index of the location in memory where the last produced samples are situated
                indexWrit = self.lockArrInd.value
                # print('lock taken')
                updateWriter(indexWrit)

            # 1. check if there is enough samples for acquisition
            # if it is not split
            # print('indexRead ', indexRead , 'indexWrit', indexWrit)
            if (indexRead <= indexWrit):
                # print('not split')
                producedSamples = indexWrit - indexRead
                # 2. If produced samples are sufficient to proceed
                if (producedSamples > oneFrame):
                    # consume them in an simple way
                    # simple operation, just copy data from big buffer to a temporary one
                    data_16bit[0:oneFrame] = self.shared_array[indexRead:indexRead + oneFrame]
                    # print('simple case')
                    sys.stdout.flush()
                    # increase the pointer of consumer to the array
                    indexRead = indexRead + oneFrame
                    updateReader(indexRead)
            # If the data is split
            elif (indexRead > indexWrit):
                # print('split')
                producedSamples = (lenArray - indexRead) + indexWrit
                # 2 If produced samples are sufficient to proceed
                if (producedSamples > oneFrame):
                    # 3. check if the interesting part is split
                    # if it is not the case
                    # print('interest. part split')
                    predictedIndex = indexRead + oneFrame
                    if (predictedIndex <= lenArray):
                        # proceed in the same way as before
                        data_16bit[0:oneFrame] = self.shared_array[indexRead:indexRead + oneFrame]
                        # increase the pointer of consumer to the array
                        indexRead = indexRead + oneFrame
                        updateReader(indexRead)
                        # print('split buffer but array complete')
                    # if so
                    elif (predictedIndex > lenArray):
                        # first_part & second_part are indices
                        # find the first part - the end of the buffer/beginning of temporary array
                        first_part = lenArray - indexRead
                        # print(first_part)
                        data_16bit[0:first_part] = self.shared_array[indexRead:lenArray]
                        # find the second part - the the beginning of the buffer/ the end of the temporary array
                        second_part = oneFrame - first_part
                        # print('second_part',second_part)
                        data_16bit[first_part:oneFrame] = self.shared_array[0:second_part]
                        indexRead = second_part
                        updateReader(indexRead)
                        # print('both buffer and array split')


'''
Class which is necessary for using PyQt5 widget. It defines refresh rate and the data to show. 
The library has been choosen for the sake of high performance way higher than matplotlib offers.
'''


class SecondWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SecondWindow, self).__init__(parent)
        self.setupUi(self)

    def setSource(self, shared_array, lockArrInd, noOfSamples, noOfRecords):
        self.shared_array = shared_array
        self.lockArrInd = lockArrInd
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords

        '''
                Initialize variables necessary for the circular buffer.
        '''
        self.oneFrame = self.noOfSamples * self.noOfRecords
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        self.lenArray = len(self.shared_array)
        # index of the reader
        self.indexRead = 0
        print('Gradient Plot : all necessary variables are created')

        data = np.arange(0, self.noOfSamples * self.noOfRecords)
        data = (data / (self.noOfSamples * self.noOfRecords)) * 28000
        self.im_widget.setImage((data.reshape(self.noOfSamples, self.noOfRecords)), autoLevels=False,
                                levels=[1000, 28000], autoHistogramRange=False)

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)

        self.im_widget = pg.ImageView(self)
        # uncomment to hide histogram

        self.im_widget.ui.histogram.hide()
        view = self.im_widget.getView()
        view.setRange(xRange=[1000, 16000])
        self.im_widget.setPredefinedGradient('flame')

        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.initialisationFigure()

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.majFigure)
        self.timer.start(10000)

        self.timer2 = QtCore.QTimer(self)
        self.timer2.timeout.connect(self.NumberRefreshPerSecond)
        self.timer2.start(10000)

    def NumberRefreshPerSecond(self):
        # print(self.count)
        self.count = 0

    def majFigure(self):
        self.count = self.count + 1

        indexWrit = 0
        """
        Try to acquire data until it is acquired
        """
        done = False
        print('Gradient plot: start iteration')
        while done == False:
            # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
            time.sleep(0.1)
            print('Gradient plot: trying to get lock')
            with self.lockArrInd.get_lock():
                # currIndex - index of the location in memory where the last produced samples are situated
                indexWrit = self.lockArrInd.value
                print('Gradient plot: lock taken')

            # 1. check if there is enough samples for acquisition
            # if it is not split
            print('Gradient plot: indexRead ', self.indexRead, 'Gradient plot: indexWrit', indexWrit)
            if (self.indexRead <= indexWrit):
                print('Gradient plot: not split')
                producedSamples = indexWrit - self.indexRead
                # 2. If produced samples are sufficient to proceed
                if (producedSamples > self.oneFrame):
                    # consume them in an simple way
                    # simple operation, just copy data from big buffer to a temporary one
                    self.data_16bit[0:self.oneFrame] = self.shared_array[self.indexRead:self.indexRead + self.oneFrame]
                    print('Gradient plot: simple case')
                    # increase the pointer of consumer to the array
                    self.indexRead = self.indexRead + self.oneFrame
                    done = True
            # If the data is split
            elif (self.indexRead > indexWrit):
                print('Gradient plot: split')
                producedSamples = (self.lenArray - self.indexRead) + indexWrit
                # 2 If produced samples are sufficient to proceed
                if (producedSamples > self.oneFrame):
                    # 3. check if the interesting part is split
                    # if it is not the case
                    print('Gradient plot: interest. part split')
                    predictedIndex = self.indexRead + self.oneFrame
                    if (predictedIndex <= self.lenArray):
                        # proceed in the same way as before
                        self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                           self.indexRead:self.indexRead + self.oneFrame]
                        # increase the pointer of consumer to the array
                        self.indexRead = self.indexRead + self.oneFrame
                        print('Gradient plot: split buffer but array complete')
                        done = True
                    # if so
                    elif (predictedIndex > self.lenArray):
                        # first_part & second_part are indices
                        # find the first part - the end of the buffer/beginning of temporary array
                        first_part = self.lenArray - self.indexRead
                        print(first_part)
                        self.data_16bit[0:first_part] = self.shared_array[self.indexRead:self.lenArray]
                        # find the second part - the the beginning of the buffer/ the end of the temporary array
                        second_part = self.oneFrame - first_part
                        print('Gradient plot: second_part', second_part)
                        self.data_16bit[first_part:self.oneFrame] = self.shared_array[0:second_part]
                        self.indexRead = second_part
                        print('Gradient plot: both buffer and array split')
                        done = True

        print('Gradient plot: ready to plot')
        # some optimizations during setting the image
        self.im_widget.setImage((self.data_16bit.reshape(self.noOfSamples, self.noOfRecords)), autoLevels=False,
                                levels=[1000, 28000], autoHistogramRange=False)

    def initialisationFigure(self):
        self.count = 0
        self.im_widget.show()

    def closeEvent(self, event):
        self.timer.stop()


'''
The following class extends the reader class and is used for plotting 2d graph as a gradient of
beam profile intensity during many turns.
'''


class ReaderGradientPlot(Reader):

    def run(self):
        app = QtWidgets.QApplication(sys.argv)
        form = SecondWindow()
        print(self.name)
        form.setSource(self.shared_array, self.lockArrInd, self.noOfSamples, self.noOfRecords)
        print('form show')
        form.show()
        sys.exit(app.exec_())


if __name__ == '__main__':
    __spec__ = None
    # dimensions of record list
    noOfSamples = 1000  # [samples]
    noOfRecords = 1000  # [records]
    # shared variable index
    pointerToLastProduced = Value('i', 0)
    # big array of samples
    global ArrayOfSamples
    ArrayOfSamples = mp.RawArray(ctypes.c_ushort, 5000000)
    # producer which writes  (Shared memory objects must be used as initialization arguments to the Process object.)
    # start prducer 1
    p1 = Writer(lockArrInd=pointerToLastProduced, name='ADQ14', shared_array=ArrayOfSamples)
    # start consumer 1
    p2 = ReaderPerf(lockArrInd=pointerToLastProduced, name='Perf. Analysis', shared_array=ArrayOfSamples,
                    noOfSamples=noOfSamples, noOfRecords=noOfRecords)
    # start consumer 2
    p3 = ReaderGradientPlot(lockArrInd=pointerToLastProduced, name='Plot Gradient Process', shared_array=ArrayOfSamples,
                            noOfSamples=noOfSamples, noOfRecords=noOfRecords)
    p3.daemon = True
    print('started')
    p1.start()
    p2.start()
    p3.start()
    p1.join()
    p2.join()
    p3.join()

'''
Benchmarks
'''

# packetSize=10
# arraySize=90
# indexOfProducer=16
# index = 84
#
# start_time = time.time()
# for x in range(0, 300000):
#    (arraySize - index + indexOfProducer)%arraySize
# elapsed_time = time.time() - start_time
# print(elapsed_time)
# start_time = time.time()
#
#
##sligthly slower solution but better in this case because it directly splits into two different situations
# for x in range(0, 300000):
#    if(index+packetSize<=arraySize):
#        indexOfProducer-index
#    else:
#        arraySize-index + indexOfProducer
# elapsed_time = time.time() - start_time
# print(elapsed_time)

'''
Animation of circular buffer
'''
# https://stackoverflow.com/questions/47557130/matplotlib-line-rotation-or-animation

# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation
# import time
#
# #dot
# r = 0#90  * (np.pi/180)
# #the length of the arrow
# t = 50000
#
# fig = plt.figure()
# ax = fig.gca(projection = 'polar')
# fig.canvas.set_window_title('Circular Buffer')
# ax.plot(r, t, color ='b', marker = 'o', markersize = '3')
# ax.set_theta_zero_location('N')
# ax.set_theta_direction(-1)
# ax.set_ylim(0,1.02*t)
#
# line1, = ax.plot([0, 0],[0,t], color = 'b', linewidth = 1)
# line2, = ax.plot([0, 0],[0,t], color = 'r', linewidth = 1)
#
# def updateWriter(angle):
#     line1.set_data([angle, angle],[0,t])
#     return line1,
#
# def updateReader(angle):
#     line2.set_data([angle, angle],[0,40000])
#     return line2,
#
# frames = np.array([np.linspace(0,2*np.pi,1000),np.linspace(2*np.pi,0,1000)])
# print(np.shape(frames))
#
# plt.show()
#
# #plt.ion()
# #ani = matplotlib.animation.FuncAnimation(fig, update, frames=frames, blit=True, interval=10)
# for x in range(0, 500):
#     plt.pause(0.1)
#     for y in range(0,2):
#         if(y==0):
#             updateWriter(frames[y][x])
#         if(y==1):
#             updateReader(frames[y][x])
