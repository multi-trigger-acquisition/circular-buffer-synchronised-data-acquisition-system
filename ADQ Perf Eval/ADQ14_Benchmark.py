'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN, Warsaw University Of Technology

Disclaimer : the acquisition part of the code was written and belongs to SP-Devices (Teledyne Signal Processing Devices Sweden AB).
'''


# -*- coding: utf-8 -*-
import numpy as np
#required only for digitizers
import ctypes as ct
import matplotlib.pyplot as pyplot
import matplotlib.pyplot as plt
from datetime import datetime
import sys
import os
#import visa
import pylab
import time

#comment out the 2 following lines for the scopes if any error will appear
sys.path.insert(1, os.path.dirname(os.path.realpath(__file__))+'/../..')
from modules.example_helpers import *

'''
The set of functions which are dependent on a device/

WARNING!!! - the testing script doesn't include setting the triggering device.
It has to be done manually.
'''

#To be finished - may work with the default DPO7254 function
class TDS5104(object):
    
    def __init__(self,scope):
        print("initialized")
        self.scope = scope

    def connect(self):
        # Checking if scope is ok
        print(self.scope.query('*IDN?'))
        
    def acquisition(self, noOfPoints):
        #timeout
        self.scope.timeout = 0
        self.scope.write('HORizontal:FASTframe:COUNt %d' %1)
        self.scope.write('HORizontal.MAIn:SAMPLERate 4E10')
        self.scope.write('HORizontal:RECOrdlength %d' % noOfPoints)
        self.scope.write('HORizontal:RESOlution %d' % noOfPoints)
        
    def transfer(self, noOfPoints):
        self.data = self.scope.query_binary_values('CURV?', datatype='h')
 
#works but unreliably, please check the messages if there is no inconsistences       
class DPO7254(object):
    
    def __init__(self,scope):
        print("initialized")
        self.scope = scope

    def connect(self):
        print(self.scope.query('*IDN?'))
        
    def acquisition(self, noOfPoints,noOfFrames):
        self.noOfPoints = noOfPoints
        #timeout
        self.scope.timeout = None
        self.noOfFrames = noOfFrames
        
        #Calculate a correct scale (period / 10 divisions)
        scale = (self.noOfPoints/4E10)/10
        #scale = (self.noOfPoints/float(4E10))/10
                
        self.scope.write('HORizontal:FASTframe:COUNt %d' %self.noOfFrames)
        self.scope.write('HORizontal:MAIn:SAMPLERate 4E10')
        self.scope.write('HORizontal:RECOrdlength %d' % noOfPoints)
        self.scope.write('HORizontal:MAIn:SCAle %E' %scale)
        #self.scope.write('HORizontal:RESOlution %d' % noOfPoints)
        #self.scope.write('HORizontal:FASTframe:LENgth %d' % noOfPoints)
        
        #check if the settings have been applied correctly
        
           
        self.scope.write('acquire:state 1')
        expectedTime = 100
        start_time = time.time()
        #print(start_time)
        while True:
            elapsed_time = time.time() - start_time
            if(elapsed_time <= expectedTime):
            #exit from the loop if the data is in
                if(int(self.scope.ask("BUSY?")) == 0):
                    print("break")
                    break
                    time.sleep(.2)
                    numberofpoints = int(self.scope.ask('wfmoutpre:nr_pt?'))
                    if(numberofpoints!=noOfPoints*self.noOfFrames): 
                        print("the settings haven't been accepted")
                        print(numberofpoints)
            else:
                self.scope.clear()
                print("break and clear")
                break
        print("acquired")
        
  
        
    def transfer(self):
        start = time.time()
        self.data = np.array((self.scope.query_binary_values('CURV?', datatype='B', is_big_endian=False)), dtype=np.uint8)
        #self.data = data1
        end = time.time()
        if(self.noOfPoints*self.noOfFrames!=len(self.data)):
            print('not correct no of samples : len ', len(self.data))
        timeOfTransfer = end - start
        return timeOfTransfer
    
    def destructor(self):
        destr=1
        
#%%       
class ADQ214(object):
    
    def __init__(self):
        self.name = 'ADQ14'
        
#%%       
class ADQ14(object):
    
    def __init__(self):
        # Gain - normalized to 10 bits. 1024 - unity gain. Allowed range is -32768 to 32767
        # Offset - value of 8 will change the offset by 8 codes (multiplied by the gain setting). Allowed range is -32768 to 32767
        # Record settings
        self.number_of_records  = 4
        self.samples_per_record = 2048
        #Especially for streaming applications it may be desired to reduce the rate of data. For this, data decimation or
        #sample skip may be used
        self.sample_skip = 1;
        #a short pulse issued in advance to a longer one 
        self.pretrigger = 0;
        self.triggerdelay = 0;
        #1111
        self.channel_mask = 0x1;
        # Plot data if set to True
        self.plot_data = True
        # Print metadata in headers
        self.print_headers = True
        # Load ADQAPI
        self.ADQAPI = adqapi_load()
        # Create ADQControlUnit
        self.adq_cu = ct.c_void_p(self.ADQAPI.CreateADQControlUnit())
        # Enable error logging from ADQAPI
        self.ADQAPI.ADQControlUnit_EnableErrorTrace(self.adq_cu, 65536, '.')
        # Find ADQ devices
        self.ADQAPI.ADQControlUnit_FindDevices(self.adq_cu)
        # gets number of ADQs
        self.n_of_ADQ  = self.ADQAPI.ADQControlUnit_NofADQ(self.adq_cu)
        # gets number of units that failed startup
        self.n_of_failed_ADQ  = self.ADQAPI.ADQControlUnit_GetFailedDeviceCount(self.adq_cu)     
        print("initialized")

    def connect(self):
        if self.n_of_failed_ADQ > 0:
          print(self.n_of_failed_ADQ, 'connected devices failed initialization.')
        print('Number of ADQ found:  {}'.format(self.n_of_ADQ))
        # Exit if no devices were found
        if self.n_of_ADQ < 1:
            print('No ADQ connected.')
            self.ADQAPI.DeleteADQControlUnit(self.adq_cu)
            sys.exit(1)
        # Select ADQ
        if self.n_of_ADQ > 1:
            self.adq_num = int(input('Select ADQ device 1-{:d}: '.format(self.n_of_ADQ)))
        else:
            self.adq_num = 1
        print_adq_device_revisions(self.ADQAPI, self.adq_cu, self.adq_num)
        # reference clock -  the clock signal used to synchronise and schedule operations
        self.ADQ_CLOCK_INT_INTREF = 0
        self.ADQAPI.ADQ_SetClockSource(self.adq_cu, self.adq_num, self.ADQ_CLOCK_INT_INTREF)
        # Setup test pattern
        #0: Normal operation mode (direct data)
        self.ADQAPI.ADQ_SetTestPatternMode(self.adq_cu, self.adq_num, 0)      
        # Set trig mode
        #  software trigger only
        self.SW_TRIG = 1
        # set one of the types of the trigger
        self.trig_type = self.SW_TRIG
        self.success = self.ADQAPI.ADQ_SetTriggerMode(self.adq_cu, self.adq_num, self.trig_type)
        if (self.success == 0):
            print('ADQ_SetTriggerMode failed.')
        # Setup data processing (should be done before multirecord setup)
        self.ADQAPI.ADQ_SetSampleSkip(self.adq_cu, self.adq_num, self.sample_skip)
        self.ADQAPI.ADQ_SetPreTrigSamples(self.adq_cu, self.adq_num, self.pretrigger)
        self.ADQAPI.ADQ_SetTriggerHoldOffSamples(self.adq_cu, self.adq_num, self.triggerdelay);
        print("connect routine")
        
    def acquisition(self, samples_per_record_in, number_of_records_in):
        
        print(type(samples_per_record_in))
        print(type(number_of_records_in))
        # Setup multirecord
        self.samples_per_record = int(samples_per_record_in)
        self.number_of_records = number_of_records_in
        
        print(type(samples_per_record_in))
        print(type(number_of_records_in))
        
        self.ADQAPI.ADQ_MultiRecordSetChannelMask(self.adq_cu, self.adq_num, self.channel_mask);
        self.ADQAPI.ADQ_MultiRecordSetup(self.adq_cu, self.adq_num, self.number_of_records, self.samples_per_record)
        
        # Get number of channels from device
        self.number_of_channels = 1 #self.ADQAPI.ADQ_GetNofChannels(self.adq_cu, self.adq_num)
        
        
        # Arm acquisition
        print('Arming device')
        self.ADQAPI.ADQ_DisarmTrigger(self.adq_cu, self.adq_num)
        self.ADQAPI.ADQ_ArmTrigger(self.adq_cu, self.adq_num)
        
        # Allocate target buffers for intermediate data storage
        self.target_buffers = (ct.POINTER(ct.c_int16*(self.number_of_records*self.samples_per_record))*self.number_of_channels)()
        for bufp in self.target_buffers:
          bufp.contents = (ct.c_int16*(self.number_of_records*self.samples_per_record))()
        
        # Create some buffers for the full records
        self.data_numpy = [np.zeros(self.number_of_records*self.samples_per_record, dtype=np.int16)]
        print(np.shape(self.data_numpy))
        
        # Allocate target buffers for headers
        self.header_list = (HEADER*self.number_of_records)()
        self.target_headers = ct.POINTER(HEADER*self.number_of_records)()
        self.target_headers.contents = self.header_list
        self.target_headers_vp = ct.cast(ct.pointer(self.target_headers), ct.POINTER(ct.c_void_p))
        
        # Generate triggers if software trig is used (for each record separately)
        if (self.trig_type == 1):
          for trig in range(self.number_of_records):
            self.ADQAPI.ADQ_SWTrig(self.adq_cu, self.adq_num)
        
        print('Waiting for data...')
        # Collect data until all requested records have been recieved

    def transfer(self):  
        self.records_completed = 0
        self.records_available = 0;
        start_time = float(time.time())
        # Read out data until records_completed for ch A is number_of_records
        while (self.records_completed < self.number_of_records):
          self.records_available = self.ADQAPI.ADQ_GetAcquiredRecords(self.adq_cu, self.adq_num)
          self.new_records = self.records_available - self.records_completed
        
          if self.new_records > 0:      
            # Fetch data and headers into target buffers
            self.status = self.ADQAPI.ADQ_GetDataWHTS(self.adq_cu, self.adq_num,
                                                 self.target_buffers,
                                                 self.target_headers,
                                                 None,
                                                 self.number_of_records*self.samples_per_record,
                                                 2,
                                                 self.records_completed,
                                                 self.new_records,
                                                 self.channel_mask,
                                                 0,
                                                 self.samples_per_record,
                                                 0x00)
        
            if self.status == 0:
              print('GetDataWH failed!')
              self.ADQAPI.DeleteADQControlUnit(self.adq_cu)
              sys.exit()
        
            for ch in range(0,self.number_of_channels):
              data_buf = np.frombuffer(self.target_buffers[ch].contents, dtype=np.int16, count=(self.samples_per_record*self.new_records))
              for rec in range(0,self.new_records):
                for s in range(0,self.samples_per_record):
                    # dividing the data among choosen channels
                  self.data_numpy[ch][(self.records_completed+rec)*self.samples_per_record + s] = data_buf[rec*self.samples_per_record + s]
        
            self.records_completed += self.new_records
            self.target_headers_vp.contents.value += self.new_records*ct.sizeof(HEADER)
        
            print('Records read out:',self.records_completed)
        
        end_time = float(time.time())
        
        print('time of acquisition')
        trtim = end_time - start_time
        print(trtim)
        print(type(trtim))
        
        if self.plot_data:
          for ch in range(self.number_of_channels):
            if self.number_of_records > 0:        
              widths = np.array([], dtype=np.uint32)
              self.record_end_offset = 0
              # Extract record lengths from headers
              for rec in range(self.number_of_records):
                header = self.header_list[rec]    
                widths = np.append(widths, header.RecordLength) 
        
              # Get new figure
              plt.figure(ch)
              plt.clf()
              # Plot data
              plt.plot(self.data_numpy[ch].T, '.-')
              # Set window title
              plt.gcf().canvas.set_window_title('Channel {}'.format(ch))
              # Set grid mode
              plt.grid(which='Major')
              # Mark records in plot
              alternate_background(plt.gca(), 0, widths, labels=True)  
              # Show plot
              plt.show()      
        
        
        # Close multirecord
        self.ADQAPI.ADQ_MultiRecordClose(self.adq_cu, self.adq_num)
        
        return trtim
        
        
    def destructor(self):
        # Delete ADQControlunit
        self.ADQAPI.DeleteADQControlUnit(self.adq_cu)
        
        

class test(object):    
    #integrated with inspector
    def test1(scope = None,debuggingTools = None ,setupObject = None):
        
        #choose the device
        dev_ADQ14 = 1
        dev_ADQ214 = 2
        dev_TDS5104 = 3
        dev_DPO7254 = 4
        
        dev = 1
        
        '''
        Running the tests
        '''

        if(dev == dev_ADQ14):
            device = ADQ14()
            device.connect()
            noOfFrames = 1
            #Number of samples
            noOfSamples = np.array([1024, 2048, 8192, 65536, 262144, 524288,1048576], np.int32)
        elif(dev == dev_ADQ214):
            device = ADQ214()
            device.connect()
            noOfFrames = 1
            #Number of samples
            noOfSamples = np.array([1024, 2048, 8192, 65536], np.int32)        
        elif(dev == dev_TDS5104):
            noOfFrames = 1
            #noOfSamples = np.array([500, 2000, 5000, 10000, 20000, 100000, 500000, 1000000, 5000000, 10000000], np.int32)
            noOfSamples = np.array([1000, 4000, 10000, 40000, 100000], np.int32)
            device = DPO7254(scope)
            device.connect()
        elif(dev == dev_DPO7254):
            noOfFrames = 1
            #noOfSamples
            noOfSamples = np.array([1000, 4000, 10000, 40000, 100000], np.int32)
            device = DPO7254(scope)
            device.connect()
            
        #Repeat experiment 3 times, load the results to separate arrays
        '''
        There are many attempts to measure one scenario in order to establish the deviation
        '''
        #results = np.array([[2.12, 5.6, 1.99], [12.33, 10.23, 14.22], [40.12,47.22,44.1]], np.float32)
        noOfTests = 5
        noOfCases = len(noOfSamples)
        
        
        results = np.empty(shape=[noOfCases, noOfTests])
        for attempt in range(0, noOfTests):
            for x in range(0, noOfCases):
                #test with 100 frames
                device.acquisition(noOfSamples[x],noOfFrames)
                timeTrans = device.transfer()
                results[x,attempt] = float(timeTrans)
                #print(timeTrans)
                if(debuggingTools is not None):
                    debuggingTools.takeScreenshot(setupObject)

        '''
        Visualising the results of transmission time
        '''

        mean_results = np.mean(results, axis=1)
        #print(mean_results)
        min_results = np.min(results, axis=1)
        #print(min_results)
        max_results = np.max(results, axis=1)
        #print(max_results)
        
        def r_squared(actual, ideal):
            actual_mean = np.mean(actual)
            ideal_dev = np.sum([(val - actual_mean)**2 for val in ideal])
            actual_dev = np.sum([(val - actual_mean)**2 for val in actual])
        
            return ideal_dev / actual_dev
        
        def temp_plot(noOfSamples, mean_results, min_results = None, max_results = None):
        
            #year_start = datetime(2012, 1, 1)
            #days = np.array([(d - year_start).days + 1 for d in noOfSamples])
            fig = pyplot.figure()
            pyplot.title('Measurement of the time of transmission for %d frames'% (noOfFrames) )
            pyplot.ylabel('Mean time [s]')
            pyplot.xlabel('The number of samples per one frame')
        
            if (max_results is None or min_results is None):
                # Normal plot without error bars
                pyplot.plot(noOfSamples, mean_results, marker='o')
            else:
                # Compute min/max temperature difference from the mean
                temp_err = np.row_stack((mean_results - min_results,
                                         max_results - mean_results))
        
                # Make line plot with error bars to show temperature range
                pyplot.errorbar(noOfSamples, mean_results, marker='o', yerr=temp_err)
                pyplot.title('Time of transmission (max/min)')
        
            slope, intercept = np.polyfit(noOfSamples, mean_results, 1)
            ideal_temps = intercept + (slope * noOfSamples)
            r_sq = r_squared(mean_results, ideal_temps)
        
            fit_label = 'Linear fit ({0:.2f})'.format(slope)
            pyplot.plot(noOfSamples, ideal_temps, color='red', linestyle='--', label=fit_label)
            pyplot.annotate('r^2 = {0:2f}'.format(r_sq), (0.05, 0.9), xycoords='axes fraction')
            pyplot.legend(loc='lower right')
        
            return fig
        
        #--------------------------------------------------
        data = [noOfSamples, mean_results, min_results, max_results]
        
        
        if not os.path.exists('plots'):
            os.mkdir('plots')
        
        # Plot without error bars
        #fig = temp_plot(noOfSamples, mean_results)
        #fig.savefig('plots/day_vs_temp.png')
        
        # Plot with error bars
        fig = temp_plot(noOfSamples, mean_results, min_results, max_results)
        fig.savefig('plots/transmission_time.png')
        
        
        
        '''
        Visualising the results of transmission speed
        '''
        
        
        def humanbytes(B):
            'Return the given bytes as a human friendly KB, MB, GB, or TB string'
            B = float(B)
            KB = float(1024)
            MB = float(KB ** 2) # 1,048,576
            GB = float(KB ** 3) # 1,073,741,824
            TB = float(KB ** 4) # 1,099,511,627,776
            
#            if B < KB:
#                print('{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte'))
#                return B
#            elif KB <= B < MB:
#                print('{0:.2f} KB/s'.format(B/KB))
#                return B/KB
#            elif MB <= B < GB:
#                print('{0:.2f} MB'.format(B/MB))
#                return B/MB
#            elif GB <= B < TB:
#                print('{0:.2f} GB'.format(B/GB))
#                return B/GB
#            elif TB <= B:
#                print('{0:.2f} TB'.format(B/TB))
#                return B/TB
            return float(B/MB)
        
        
        if(dev == dev_ADQ14):
            sizeSample = 2 #Bytes
        elif(dev == dev_ADQ214):
            sizeSample = 2 #Bytes       
        elif(dev == dev_TDS5104):
            sizeSample = 1 #Bytes
        elif(dev == dev_DPO7254):
            sizeSample = 1 #Bytes
        
        humanbytes_vect = np.vectorize(humanbytes, otypes=[np.float] )
        
        mean_results = humanbytes_vect(noOfSamples*sizeSample)/np.mean(results, axis=1)
        #print(mean_results)
        min_results = humanbytes_vect(noOfSamples*sizeSample)/np.min(results, axis=1)
        #print(min_results)
        max_results = humanbytes_vect(noOfSamples*sizeSample)/np.max(results, axis=1)
        #print(max_results)
        
        def r_squared(actual, ideal):
            actual_mean = np.mean(actual)
            ideal_dev = np.sum([(val - actual_mean)**2 for val in ideal])
            actual_dev = np.sum([(val - actual_mean)**2 for val in actual])
        
            return ideal_dev / actual_dev
        
        def temp_plot(noOfSamples, mean_results, min_results = None, max_results = None):
        
            #year_start = datetime(2012, 1, 1)
            #days = np.array([(d - year_start).days + 1 for d in noOfSamples])
            fig = pyplot.figure()
            pyplot.title('Measurement of the speed of transmission for %d frames'% (noOfFrames) )
            pyplot.ylabel('Mean speed [MB/s]')
            pyplot.xlabel('The number of samples per one frame')
        
            if (max_results is None or min_results is None):
                # Normal plot without error bars
                pyplot.plot(noOfSamples, mean_results, marker='o')
            else:
                # Compute min/max temperature difference from the mean
                temp_err = np.row_stack((mean_results - min_results,
                                         max_results - mean_results))
        
                # Make line plot with error bars to show temperature range
                pyplot.errorbar(noOfSamples, mean_results, marker='o', yerr=temp_err)
                pyplot.title('Speed of transmission (max/min)')
        
            slope, intercept = np.polyfit(noOfSamples, mean_results, 1)
            ideal_temps = intercept + (slope * noOfSamples)
            r_sq = r_squared(mean_results, ideal_temps)
        
            fit_label = 'Linear fit ({0:.2f})'.format(slope)
            pyplot.plot(noOfSamples, ideal_temps, color='red', linestyle='--', label=fit_label)
            pyplot.annotate('r^2 = {0:2f}'.format(r_sq), (0.05, 0.9), xycoords='axes fraction')
            pyplot.legend(loc='lower right')
        
            return fig
        
        #--------------------------------------------------
        data = [noOfSamples, mean_results, min_results, max_results]
        
        
        if not os.path.exists('plots'):
            os.mkdir('plots')
        
        # Plot without error bars
        #fig = temp_plot(noOfSamples, mean_results)
        #fig.savefig('plots/day_vs_temp.png')
        
        # Plot with error bars
        fig = temp_plot(noOfSamples, mean_results, min_results, max_results)
        fig.savefig('plots/transmission_speed.png')
        
        
        
        
        device.destructor()

test = test()
test.test1()