'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN

Version: v.2.0
Improvements: Visualisation of Circular buffer is now available to each readProcess to run as addition. Two read methods added - one with
visualisation and one without. Now every class which inherits from Reader will have access to it.

Script for acquisition and consumption of longitudinal beam profile data from Proton Synchrotron through ADQ14. Object oriented version.

Note: The multiprocessing features sometimes doesn't work perfectly on Windows machines, the solution can be using PyCharm instead of
Spyder.

Disclaimer : the acquisition part of the code was written and belongs to SP-Devices (Teledyne Signal Processing Devices Sweden AB).

'''

from multiprocessing import Process, Value
import numpy as np
import time
import multiprocessing as mp
import ctypes  as ct
import sys
import matplotlib.pyplot as plt
import os
import matplotlib
from matplotlib.widgets import Button, TextBox

#GUI libraries
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg

#ADQ14 libraries
sys.path.insert(1, os.path.dirname(os.path.realpath(__file__))+'/../../')
from modules.example_helpers import *

'''
The process below is responsible for moving data from digitizer memory into circular buffer which is available for other processes.
The availability is due to the fact that the circular buffer is based on shared multiprocessing array. The multiprocessing array
should be wide enough to fully utilize local computer memory.
'''

class Writer(Process):

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords):
        super(Writer, self).__init__()
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.index = 0
        self.maxLen=len(self.shared_array)

    # a function to be overwritten
    def run(self):
        print(self.name)

    def addToSharedArray(self,dataPacket):
        '''
        The function adds a snippet of irregular data to the circular buffer.
        dataPacket - int16 array
        '''
        # print('produce')
        # sys.stdout.flush()
        # generate random length of array to add
        sizeOfNewPacket = len(dataPacket)
        #print('size of new packet ', sizeOfNewPacket)
        # check if the data shouldn't be passed to the beinning of the circular buffer (split between beginning and the end)
        if (self.index + sizeOfNewPacket <= self.maxLen):
            # the longer the parts of data the less overhead of slicing the data
            # write some new data
            # print(len(shared_array[index:index+sizeOfNewPacket]))
            # print(len(x))
            self.shared_array[self.index:self.index + sizeOfNewPacket] = dataPacket
            #print('added to shared array : ',self.index, ' : ', self.index + sizeOfNewPacket )
            self.index = self.index + sizeOfNewPacket
            # take lock and assign the new value
            with self.lockArrInd.get_lock():
                self.lockArrInd.value = self.index

        else:  # there is a need for slicing - the circular buffer is full, go to the beginning
            toNextIter = sizeOfNewPacket - (self.maxLen - self.index)
            thisIter = sizeOfNewPacket-toNextIter
            # write some new data to fill the buffer to the end
            self.shared_array[self.index:self.maxLen] = dataPacket[0:thisIter]
            # write the rest of new data to the beginning of the buffer
            self.shared_array[0:toNextIter] = dataPacket[thisIter:sizeOfNewPacket]
            self.index = toNextIter
            # take lock and increment counter
            with self.lockArrInd.get_lock():
                self.lockArrInd.value = self.index



class WriterADQ14(Writer):

    def run(self):
        print(self.name)
        # Record settings
        '''
        Note: self.noOfRecords in readers correspond to the matrix image vertical size. Here It is more of the length
        of execution - how long the digitizer has to read
        '''
        number_of_records = 500000
        samples_per_record = self.noOfSamples  # 2300

        # Print metadata in headers
        print_headers = False

        # DMA transfer buffer settings
        transfer_buffer_size = 65536
        num_transfer_buffers = 8

        # DMA flush timeout in seconds
        flush_timeout = 0.5

        # Load ADQAPI
        ADQAPI = adqapi_load()

        # Create ADQControlUnit
        adq_cu = ct.c_void_p(ADQAPI.CreateADQControlUnit())

        # Enable error logging from ADQAPI
        ADQAPI.ADQControlUnit_EnableErrorTrace(adq_cu, 3, '.')

        # Find ADQ devices
        ADQAPI.ADQControlUnit_FindDevices(adq_cu)
        n_of_ADQ = ADQAPI.ADQControlUnit_NofADQ(adq_cu)
        print('Number of ADQ found:  {}'.format(n_of_ADQ))

        # Exit if no devices were found
        if n_of_ADQ < 1:
            print('No ADQ connected.')
            ADQAPI.DeleteADQControlUnit(adq_cu)
            adqapi_unload(ADQAPI)
            sys.exit(1)

        # Select ADQ
        if n_of_ADQ > 1:
            adq_num = int(input('Select ADQ device 1-{:d}: '.format(n_of_ADQ)))
        else:
            adq_num = 1

        print_adq_device_revisions(ADQAPI, adq_cu, adq_num)

        # Set clock source
        ADQ_CLOCK_INT_INTREF = 0
        ADQAPI.ADQ_SetClockSource(adq_cu, adq_num, ADQ_CLOCK_INT_INTREF)

        # Maximum number of channels for ADQ14 FWPD is four but we use only one
        max_number_of_channels = 1

        # check if usb3 is on
        usb3 = ADQAPI.ADQ_IsUSB3Device(adq_cu, adq_num)
        if (usb3 == 1):
            print('USB3 connected')
        else:
            print('USB3 not connected')

        # Setup test pattern
        #     0 enables the analog input from the ADCs
        #   > 0 enables a specific test pattern
        # Note: Default is to enable a test pattern (4) and disconnect the
        #       analog inputs inside the FPGA.
        ADQAPI.ADQ_SetTestPatternMode(adq_cu, adq_num, 0)

        '''
        Set gain and offset.
        unsigned int SetGainAndOﬀset (unsigned char Channel, int Gain, int Oﬀset)
        The gain value to set, normalized to 10 bits. A value of 1024 corresponds to unity gain. The allowed range is -32768 to 32767
        The oﬀset value to set. An oﬀset value of 8 will changed the oﬀset by 8 codes (multiplied by the gain setting). The allowed range is -32768 to 32767.
        '''
        ch = 1
        ADQAPI.ADQ_SetGainAndOffset(adq_cu, adq_num, ch,
                                    1024,
                                    0)
        '''
        Set bias offset - The DC-biasing circuitry is heavily ﬁltered, and a change in bias level will typically take around 3-4
        seconds to take eﬀect. Also limits settling time to 100ms.
        '''
        adjustable_bias = -17000  # Codes
        ADQAPI.ADQ_SetAdjustableBias(adq_cu, adq_num, 1, adjustable_bias)

        # Set trig mode
        SW_TRIG = 1
        EXT_TRIG_1 = 2
        EXT_TRIG_2 = 7
        EXT_TRIG_3 = 8
        LVL_TRIG = 3
        INT_TRIG = 4
        LVL_FALLING = 0
        LVL_RISING = 1
        trig_type = EXT_TRIG_1
        success = ADQAPI.ADQ_SetTriggerMode(adq_cu, adq_num, trig_type)
        if (success == 0):
            print('ADQ_SetTriggerMode failed.')
        success = ADQAPI.ADQ_SetLvlTrigLevel(adq_cu, adq_num, 0)
        if (success == 0):
            print('ADQ_SetLvlTrigLevel failed.')
        success = ADQAPI.ADQ_SetTrigLevelResetValue(adq_cu, adq_num, 1000)
        if (success == 0):
            print('ADQ_SetTrigLevelResetValue failed.')
        success = ADQAPI.ADQ_SetLvlTrigChannel(adq_cu, adq_num, 1)
        if (success == 0):
            print('ADQ_SetLvlTrigChannel failed.')
        success = ADQAPI.ADQ_SetLvlTrigEdge(adq_cu, adq_num, LVL_RISING)
        if (success == 0):
            print('ADQ_SetLvlTrigEdge failed.')

        # Setup acquisition
        channels_mask = 0x1
        # noOfHoldOffSamples may be useful to shift every acquisition for some samplesto center on the beam profile
        noOfHoldOffSamples = 0
        # may be useful when triggering on the threshold of the beam profile
        noOfPreTriggerSamples = 0
        ADQAPI.ADQ_TriggeredStreamingSetup(adq_cu, adq_num, number_of_records, samples_per_record,
                                           noOfPreTriggerSamples, noOfHoldOffSamples, channels_mask)

        success = ADQAPI.ADQ_SetStreamStatus(adq_cu, adq_num, 1);
        if (success == 0):
            print('setting stream status to 1 (turning on) failed.')
        # success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 1,0);
        # if (success == 0):
        #    print('using DRAM as FIFO failed')
        # success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 2,1);
        # if (success == 0):
        #    print('turning off headers - setting to raw data failed')
        # success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 3,1);
        # if (success == 0):
        #    print('setting the stream channel mask failed')

        # Get number of channels from device
        number_of_channels = 1  # ADQAPI.ADQ_GetNofChannels(adq_cu, adq_num)

        # Setup size of transfer buffers
        print('Setting up streaming...')
        ADQAPI.ADQ_SetTransferBuffers(adq_cu, adq_num, num_transfer_buffers, transfer_buffer_size)

        # Start streaming
        print('Collecting data, please wait...')
        ADQAPI.ADQ_StopStreaming(adq_cu, adq_num)
        ADQAPI.ADQ_StartStreaming(adq_cu, adq_num)

        # Allocate target buffers for intermediate data storage one buffer for one channel
        #
        Int16Arr = ct.c_int16 * transfer_buffer_size
        # pointer to the array
        Int16ArrPtr = ct.POINTER(Int16Arr)
        # array of pointers (of buffers)
        Int16ArrPtrArr = Int16ArrPtr * number_of_channels
        # new type is defined (human readable name)
        BUFFERS = Int16ArrPtrArr
        # instantiate the type
        target_buffers = BUFFERS()
        print('len of target buffers', len(target_buffers))

        for bufp in target_buffers:
            bufp.contents = (ct.c_int16 * transfer_buffer_size)()

        # Create some buffers for the full records
        data_16bit = [np.array([], dtype=np.int16)]

        ## 2D array of
        ## Allocate target buffers for headers
        # headerbuf_list = [(HEADER*number_of_records)()]
        ## Create an C array of pointers to header buffers (by ensuring a space)
        # headerbufp_list = ((ct.POINTER(HEADER*number_of_records)))()

        # create array of headers
        HeadArr = HEADER * number_of_records

        # Allocate target buffers for headers
        # instantiate header array
        HeaderArray = HeadArr()
        headerbuf_list = [HeaderArray for ch in range(number_of_channels)]

        # Create an C array of pointers to header buffers
        # create pointer to the array
        HeadArrPtr = ct.POINTER(HeadArr)
        # create array of pointers to headers
        HeadArrPtrArr = HeadArrPtr * number_of_channels
        # new type is defined (human readable name)
        HEADER_BUFFERS = HeadArrPtrArr
        # instantiate the type
        headerbufp_list = HEADER_BUFFERS()
        # Initiate pointers with allocated header buffers
        for ch, headerbufp in enumerate(headerbufp_list):
            headerbufp.contents = headerbuf_list[ch]

        # Create a second level pointer to each buffer pointer,
        # these will only be used to change the bufferp_list pointer values
        headerbufvp_list = [ct.cast(ct.pointer(headerbufp_list[ch]), ct.POINTER(ct.c_void_p)) for ch in
                            range(number_of_channels)]

        # Allocate length output variable
        # make a space for 4 elements and populate the elements
        # create type
        samples_added = (1 * ct.c_uint)()
        # populate the type
        for ind in range(len(samples_added)):
            samples_added[ind] = 0

        headers_added = (1 * ct.c_uint)()
        for ind in range(len(headers_added)):
            headers_added[ind] = 0

        header_status = (1 * ct.c_uint)()
        for ind in range(len(header_status)):
            header_status[ind] = 0

        # Generate triggers if software trig is used
        # why the trigger is not armed beforehand?
        # if (trig_type == 1):
        #  for trig in range(number_of_records):
        #    ADQAPI.ADQ_SWTrig(adq_cu, adq_num)
        #
        # print('Waiting for data...')

        # Collect data until all requested records have been recieved
        records_completed = 0
        headers_completed = 0
        records_completed_cnt = 0
        ltime = time.time()
        buffers_filled = ct.c_uint(0)


        # Read out data until records_completed for ch A is number_of_records
        while (number_of_records > records_completed):
            buffers_filled.value = 0
            collect_result = 1
            poll_time_diff_prev = time.time()
            # Wait for next data buffer
            while ((buffers_filled.value == 0) and (collect_result)):
                collect_result = ADQAPI.ADQ_GetTransferBufferStatus(adq_cu, adq_num,
                                                                    ct.byref(buffers_filled))
                poll_time_diff = time.time()

                if ((poll_time_diff - poll_time_diff_prev) > flush_timeout):
                    # Force flush
                    print('No data for {}s, flushing the DMA buffer.'.format(flush_timeout))
                    status = ADQAPI.ADQ_FlushDMA(adq_cu, adq_num);
                    print('ADQAPI.ADQ_FlushDMA returned {}'.format(adq_status(status)))
                    poll_time_diff_prev = time.time()

            # Fetch data and headers into target buffers
            status = ADQAPI.ADQ_GetDataStreaming(  # adq_cu, adq_num - handles to the device cu - control unit
                adq_cu, adq_num,
                target_buffers,
                #  The data and metadata is delivered to the user buﬀers: target_buﬀers and target_headers, respectively.
                headerbufp_list,
                #  suppress data collection from specific channels
                channels_mask,
                # byref works almost like pointer() , but with when we dont want to access it from python
                # samples_added - how many samples that were added to the user data buffers
                # This allows the user to
                # increment the data buﬀer pointers accordingly before the next call to the GetDataStreaming() function.
                ct.byref(samples_added),
                # headers_added and header_status provide the user with information to determine if the header
                # buﬀer pointers should be incremented.
                # The parameter headers_added contains the number of initialized
                # headers as a result of this call to GetDataStreaming()
                ct.byref(headers_added),
                ct.byref(header_status))
            # The header_status parameter is used to denote if the
            # last header is complete or not. E.g. a return value of headers_added = 4 and header_status = 1 signiﬁes 4
            # complete headers, while headers_added = 4 and header_status = 0 would signify 3 complete headers and one
            # incomplete header.

            if status == 0:
                print('GetDataStreaming failed!')
                sys.exit()
            ch = 0
            # for ch in range(number_of_channels):
            if (headers_added[ch] > 0):
                # The last call to GetDataStreaming has generated header data
                if (header_status[ch]):
                    headers_done = headers_added[ch]
                else:
                    # One incomplete header
                    headers_done = headers_added[ch] - 1
                # Update counter counting completed records
                headers_completed += headers_done

                # Update the number of completed records if at least one header has completed
                if (headers_done > 0):
                    records_completed = headerbuf_list[ch][headers_completed - 1].RecordNumber + 1

                # Update header pointer so that it points to the current header
                headerbufvp_list[ch].contents.value += headers_done * ct.sizeof(headerbuf_list[ch]._type_)
                if (headers_done > 0) and (records_completed - records_completed_cnt) > 1000:
                    dtime = time.time() - ltime
                    if (dtime > 0):
                        print('{:d} {:.2f} MB/s'.format(records_completed,
                                                        ((samples_per_record
                                                          * 2
                                                          * (records_completed - records_completed_cnt))
                                                         / (dtime)) / (1024 * 1024)))
                    sys.stdout.flush()
                    records_completed_cnt = records_completed
                    ltime = time.time()

            if (samples_added[ch] > 0):
                # Copy channel data to continuous buffer
                data_buf = np.frombuffer(target_buffers[ch].contents, dtype=np.int16, count=samples_added[ch])
                #print('size of dat buff ', len(data_buf))
                self.addToSharedArray(data_buf)




'''
The process below is simply a consumer of data produced by writer. The only information it uses is the index on which the
writer has written data lately. On the base of the information reader tries to catch up with writer but not overtake the writer.
This is a kind of consumption driven by writer. The data is consumed from shared circular buffer.
'''
class Reader(Process):

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords, modeVis):
        super(Reader, self).__init__()
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords
        self.modeVis = modeVis

        '''
                Initialize variables necessary for the circular buffer.
        '''
        self.oneFrame = self.noOfSamples * self.noOfRecords
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        self.lenArray = len(self.shared_array)
        # index of the reader
        self.indexRead = 0

    def run(self):
        #initialisation done
        print(self.name)

    '''
    This is a default - optimized version of reading operation from shared array. The reader reads a given part of shared array.
    The number of records to be read is defined during initialisation of the reader class.
    '''
    def read(self):
        """
        Try to acquire data until it is acquired
        """
        done = False
        # print('Gradient plot: start iteration')
        while done == False:
            # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
            # time.sleep(0.01)
            # print('Gradient plot: trying to get lock')
            with self.lockArrInd.get_lock():
                # currIndex - index of the location in memory where the last produced samples are situated
                indexWrit = self.lockArrInd.value
                # print('Gradient plot: lock taken')

            # 1. check if there is enough samples for acquisition
            # if it is not split
            # print('Gradient plot: indexRead ', self.indexRead, 'Gradient plot: indexWrit', indexWrit)
            if (self.indexRead <= indexWrit):
                # print('Gradient plot: not split')
                producedSamples = indexWrit - self.indexRead
                # 2. If produced samples are sufficient to proceed
                if (producedSamples > self.oneFrame):
                    # consume them in an simple way
                    # simple operation, just copy data from big buffer to a temporary one
                    self.data_16bit[0:self.oneFrame] = self.shared_array[self.indexRead:self.indexRead + self.oneFrame]
                    # print(np.shape(self.shared_array[self.indexRead:self.indexRead + self.oneFrame]))
                    # print(np.shape(self.data_16bit))
                    # print('Gradient plot: simple case')
                    # increase the pointer of consumer to the array
                    self.indexRead = self.indexRead + self.oneFrame
                    done = True
            # If the data is split
            else:
                print('Gradient plot: split')
                producedSamples = (self.lenArray - self.indexRead) + indexWrit
                # 2 If produced samples are sufficient to proceed
                if (producedSamples > self.oneFrame):
                    # 3. check if the interesting part is split
                    # if it is not the case
                    # print('Gradient plot: interest. part split')
                    predictedIndex = self.indexRead + self.oneFrame
                    if (predictedIndex <= self.lenArray):
                        # proceed in the same way as before
                        self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                           self.indexRead:self.indexRead + self.oneFrame]
                        # increase the pointer of consumer to the array
                        self.indexRead = self.indexRead + self.oneFrame
                        # print('Gradient plot: split buffer but array complete')
                        done = True
                    # if so
                    elif (predictedIndex > self.lenArray):
                        # first_part & second_part are indices
                        # find the first part - the end of the buffer/beginning of temporary array
                        first_part = self.lenArray - self.indexRead
                        # print(first_part)
                        self.data_16bit[0:first_part] = self.shared_array[self.indexRead:self.lenArray]
                        # find the second part - the the beginning of the buffer/ the end of the temporary array
                        second_part = self.oneFrame - first_part
                        # print('Gradient plot: second_part', second_part)
                        self.data_16bit[first_part:self.oneFrame] = self.shared_array[0:second_part]
                        self.indexRead = second_part
                        # print('Gradient plot: both buffer and array split')
                        done = True

        return self.data_16bit



    '''
    The following method enable not only to read data from shared circular buffer but also plot the state of pointer of consumer relative
    to the pointer of producer. This will also do some performance calculations. Make sure that Circ. Buffer Visualiser object has been initialised before.
    bufferStVis = BufferStateVisualiser(lockArrInd, name, shared_array, noOfSamples, noOfRecords)
    '''
    def readWithBufVis(self, TB=None):
        print('try to read with vis')
        if(TB==None):
            print('Initialize Buffer beforehand')
        else:
            #print('TB!=None')
            """
                    Try to acquire data until it is acquired
            """
            done = False
            # print('Gradient plot: start iteration')
            while done == False:
                # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
                # time.sleep(0.01)
                with self.lockArrInd.get_lock():
                    # currIndex - index of the location in memory where the last produced samples are situated
                    indexWrit = self.lockArrInd.value
                    TB.updateWriter(indexWrit)
                    # print('indexWrit loop')
                # 1. check if there is enough samples for acquisition
                # if it is not split
                # print('Gradient plot: indexRead ', self.indexRead, 'Gradient plot: indexWrit', indexWrit)
                if (self.indexRead <= indexWrit):
                    # print('Gradient plot: not split')
                    producedSamples = indexWrit - self.indexRead
                    # 2. If produced samples are sufficient to proceed
                    if (producedSamples >= self.oneFrame):
                        # consume them in an simple way
                        # simple operation, just copy data from big buffer to a temporary one
                        self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                           self.indexRead:self.indexRead + self.oneFrame]
                        # increase the pointer of consumer to the array
                        self.indexRead = self.indexRead + self.oneFrame
                        done = True
                        TB.updateReader(self.indexRead)
                # If the data is split
                else:
                    # print('Gradient plot: split')
                    producedSamples = (self.lenArray - self.indexRead) + indexWrit
                    # print('producedSamples', producedSamples)
                    # print('self.lenArray', self.lenArray)
                    # print('self.indexRead', self.indexRead)
                    # print('indexWrit', indexWrit)
                    # 2 If produced samples are sufficient to proceed
                    if (producedSamples > self.oneFrame):
                        print('producedSamples',producedSamples)
                        # 3. check if the interesting part is split
                        # if it is not the case
                        # print('Gradient plot: interest. part split')
                        predictedIndex = self.indexRead + self.oneFrame
                        if (predictedIndex <= self.lenArray):
                            # print('predictedIndex', predictedIndex)
                            # proceed in the same way as before
                            self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                               self.indexRead:self.indexRead + self.oneFrame]
                            # increase the pointer of consumer to the array
                            self.indexRead = self.indexRead + self.oneFrame
                            # print('Gradient plot: split buffer but array complete')
                            done = True
                            TB.updateReader(self.indexRead)
                        # if so
                        elif (predictedIndex > self.lenArray):
                            # first_part & second_part are indices
                            # find the first part - the end of the buffer/beginning of temporary array
                            first_part = self.lenArray - self.indexRead
                            # print(first_part)
                            self.data_16bit[0:first_part] = self.shared_array[self.indexRead:self.lenArray]
                            # find the second part - the the beginning of the buffer/ the end of the temporary array
                            second_part = self.oneFrame - first_part
                            # print('Gradient plot: second_part', second_part)
                            self.data_16bit[first_part:self.oneFrame] = self.shared_array[0:second_part]
                            self.indexRead = second_part
                            # print('Gradient plot: both buffer and array split')
                            done = True
                            TB.updateReader(self.indexRead)

            return self.data_16bit


'''
Circ. Buffer Visualiser class.
'''
class BufferStateVisualiser():

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords):

        '''
        Initialize variables necessary for the circular buffer.
        '''
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords

        self.oneFrame = noOfSamples * noOfRecords
        self.lenArray = len(shared_array)



        # index of the reader
        indexRead = 0

        '''
        Initialize variables needed to update animation of the buffer.
        '''
        # dot
        self.r = 0  # 90  * (np.pi/180)
        # the length of the arrow
        self.t = 50000
        # max as a rotation angle
        self.maxN = 2 * np.pi

        self.fig = plt.figure()
        self.ax = self.fig.gca(projection='polar')
        self.fig.canvas.set_window_title('Circular Buffer')
        self.ax.plot(self.r, self.t, color='b', marker='o', markersize='3')
        self.ax.set_theta_zero_location('N')
        self.ax.set_theta_direction(-1)
        self.ax.set_ylim(0, 1.02 * self.t)
        self.ax.set_yticklabels([])

        # set labels
        self.label_pos = np.linspace(0.0, 2 * np.pi, 4, endpoint=False)
        self.ax.set_xticks(self.label_pos)
        self.label_cols = [0, self.lenArray / 4, self.lenArray / 2, self.lenArray - self.lenArray / 4]
        self.ax.set_xticklabels(self.label_cols, size=24)

        # defining two lines corresponding to writer and reader indices
        self.line1, = self.ax.plot([0, 0], [0, self.t], color='b', linewidth=1, label='Producer')
        self.line2, = self.ax.plot([0, 0], [0, self.t], color='r', linewidth=1, label='Consumer')
        self.ax.legend(loc=4)

        plt.ion()
        plt.show()

    def updateWriter(self,angle):
        plt.pause(0.05)
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line1.set_data([newvalue, newvalue], [0, self.t])
        return self.line1,

    def updateReader(self,angle):
        plt.pause(0.05)
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line2.set_data([newvalue, newvalue], [0, 40000])
        return self.line2,



'''
Class which is necessary for using PyQt5 widget. It defines refresh rate and the data to show. 
The library has been choosen for the sake of high performance way higher than matplotlib offers.
'''
class SecondWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SecondWindow, self).__init__(parent)
        self.setupUi(self)

    def setSource(self, shared_array, lockArrInd , noOfSamples, noOfRecords, modeVis, requestor):
        self.shared_array = shared_array
        self.lockArrInd = lockArrInd
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords
        #visualisation of the buffer on/off
        self.modeVis = modeVis
        self.requestor=requestor
        if(self.modeVis==True):
            self.bufferStVis = BufferStateVisualiser(self.lockArrInd, 'Vis', self.shared_array, self.noOfSamples, self.noOfRecords)
        '''
                Initialize variables necessary for the circular buffer.
        '''
        self.oneFrame = self.noOfSamples * self.noOfRecords
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        self.lenArray = len(self.shared_array)
        # index of the reader
        self.indexRead = 0

        # uncomment for performance optimization
        self.im_widget.getImageItem().setAutoDownsample(True)

        data = np.arange(0, self.noOfSamples * self.noOfRecords)
        data = (data/(self.noOfSamples*self.noOfRecords)) * 28000
        self.im_widget.setImage((data.reshape(self.noOfSamples, self.noOfRecords)), autoLevels=False,
                            levels=[-32767, 32767], autoHistogramRange=False)

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)

        self.im_widget = pg.ImageView(self)


        # uncomment to hide histogram

        self.im_widget.ui.histogram.hide()
        view = self.im_widget.getView()
        view.setRange(xRange=[1000, 16000])
        self.im_widget.setPredefinedGradient('flame')
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.initialisationFigure()
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.majFigure)
        self.timer.start(1000)
        self.timer2 = QtCore.QTimer(self)
        self.timer2.timeout.connect(self.NumberRefreshPerSecond)
        self.timer2.start(1000)

    def NumberRefreshPerSecond(self):
        # print(self.count)
        self.count = 0


    def majFigure(self):
        self.count = self.count + 1
        if(self.modeVis==True):
            self.data_16bit = self.requestor.readWithBufVis(self.bufferStVis)

        else:
            self.data_16bit = self.requestor.read()


        #some optimizations during setting the image
        self.im_widget.setImage((self.data_16bit.reshape(self.noOfSamples, self.noOfRecords)), autoLevels=False,
                                levels=[-32767, 32767], autoHistogramRange=False)

    def initialisationFigure(self):
        self.count = 0
        self.im_widget.show()

    def closeEvent(self, event):
        self.timer.stop()


'''
Animated version of Second Window
Requires: multiple of 16 as number of records.
'''

class SecondWindowAnimated(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SecondWindowAnimated, self).__init__(parent)
        self.setupUi(self)

    def setSource(self, shared_array, lockArrInd , noOfSamples, noOfRecords, modeVis, requestor):

        # arbitrary number of 16 chunks of data - to animate 16 times per frame
        self.recordsPerChange = int(noOfRecords / 16)
        #Array of 16 sets of records - work like fifo - first in first out and plot the state
        data_16bit = np.zeros((noOfRecords * noOfSamples), dtype=np.int16)  # prefilled array
        data_16bit = np.ascontiguousarray(data_16bit, dtype=np.int16)  # making it contiguous
        data_16bit = np.reshape(data_16bit, (16, (int(noOfRecords * noOfSamples / 16))))
        self.ArrayOfChunksOfImage = data_16bit
        # print(np.shape(self.ArrayOfChunksOfImage))
        # print(self.recordsPerChange)

        self.shared_array = shared_array
        self.lockArrInd = lockArrInd
        self.noOfSamples = noOfSamples

        self.noOfRecords = noOfRecords
        #visualisation of the buffer on/off
        self.modeVis = modeVis
        self.requestor=requestor
        self.indexRead = 0

        if(self.modeVis==True):
            self.bufferStVis = BufferStateVisualiser(self.lockArrInd, 'Vis', self.shared_array, self.noOfSamples, self.noOfRecords)
        '''
                Initialize variables necessary for the circular buffer.
        '''
        # populate all the chunks and plot and test
        data = 0
        for x in range(0, 16):
            data=data+220
            for y in range(int(noOfRecords * noOfSamples/16)):
                self.ArrayOfChunksOfImage[x][y]=data


        self.final = self.ArrayOfChunksOfImage.reshape(int(self.noOfRecords), int( self.noOfSamples))
        self.im_widget.setImage(self.final, autoLevels=False,
                            levels=[0, 28000], autoHistogramRange=False)


        # Get the colormap from matplotlib
        # colormap = matplotlib.get_cmap("plasma")  # cm.get_cmap("CMRmap")
        # colormap._init()
        # lut = (colormap._lut * 1000).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        # self.im_widget.getImageItem().setLookupTable(lut)
        #uncomment for performance optimization
        self.im_widget.getImageItem().setAutoDownsample(True)
        #to turn on antialiasing
        #pg.setConfigOptions(antialias=True)


        # very important - changing the mode from printing the whole packet to acquiring only 1/16 and animating
        #reinitialize containers and variables
        print(self.requestor.noOfRecords)
        self.requestor.noOfRecords = self.recordsPerChange
        print(self.requestor.noOfRecords)
        self.requestor.oneFrame = self.noOfSamples * self.requestor.noOfRecords
        self.requestor.data_16bit = np.zeros(self.requestor.oneFrame, dtype=np.int16)  # prefilled array
        self.requestor.data_16bit = np.ascontiguousarray(self.requestor.data_16bit, dtype=np.int16)  # making it contiguous




    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)

        self.im_widget = pg.ImageView(self)
        # uncomment to hide histogram

        self.im_widget.ui.histogram.hide()
        view = self.im_widget.getView()
        view.setRange(xRange=[1000, 16000])


        self.im_widget.setPredefinedGradient('flame')

        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.initialisationFigure()
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.majFigure)
        self.timer.start(100)
        self.timer2 = QtCore.QTimer(self)
        self.timer2.timeout.connect(self.NumberRefreshPerSecond)
        self.timer2.start(500)

    def NumberRefreshPerSecond(self):
        # print(self.count)
        self.count = 0


    def majFigure(self):
        self.count = self.count + 1

        if(self.modeVis==True):
            self.data_16bit = self.requestor.readWithBufVis(self.bufferStVis)

        else:
            self.data_16bit = self.requestor.read()




        #self.final[:-self.recordsPerChange] = self.final[self.recordsPerChange:]
        self.final = np.roll(self.final,-self.recordsPerChange,axis=0)

        #add new
        # print(np.shape(self.final))
        # print(np.shape(self.data_16bit))
        # print(self.recordsPerChange*15)
        self.final[self.recordsPerChange*15:]= np.reshape(self.data_16bit,(self.recordsPerChange,self.noOfSamples))


        #reshaping in this place is unoptimal
        ###         four different of ways f shifting array of 16 vectors - ineffective          ###
        # self.ArrayOfChunksOfImage[0:15] = self.ArrayOfChunksOfImage[1:16]
        # self.ArrayOfChunksOfImage = np.roll(self.ArrayOfChunksOfImage, 1)
        # self.ArrayOfChunksOfImage[:-1] = self.ArrayOfChunksOfImage[1:]
        # for x in range(0, 15):
        #     self.ArrayOfChunksOfImage[x]=self.ArrayOfChunksOfImage[x+1]
        # final = self.ArrayOfChunksOfImage.reshape(int(self.noOfRecords), int(self.noOfSamples))
        #some optimizations during setting the image
        print(np.min(self.final))
        print(np.max(self.final))
        self.im_widget.setImage(self.final, autoLevels=False,
                                levels=[-32767, 32767], autoHistogramRange=False)

    def initialisationFigure(self):
        self.count = 0
        self.im_widget.show()

    def closeEvent(self, event):
        self.timer.stop()



'''
The following class extends the reader class and is used for plotting 2d graph as a gradient of
beam profile intensity during many turns.
'''
class ReaderGradientPlot(Reader):

    #modeVis  - set visualisation mode on or of
    def run(self):
        app = QtWidgets.QApplication(sys.argv)
        form = SecondWindow()
        print(self.name)
        form.setSource(self.shared_array, self.lockArrInd , self.noOfSamples, self.noOfRecords, self.modeVis, self)
        form.show()
        sys.exit(app.exec_())
'''
Animated version of GradientPlot
'''
class ReaderGradientPlotAnimated(Reader):

    #modeVis  - set visualisation mode on or of
    def run(self):
        app = QtWidgets.QApplication(sys.argv)
        form = SecondWindowAnimated()
        print(self.name)
        # set source of data
        form.setSource(self.shared_array, self.lockArrInd , self.noOfSamples, self.noOfRecords, self.modeVis, self)
        # update
        form.show()
        sys.exit(app.exec_())


'''
The following class extends the reader class and is used for storing data in HDD.
According to the post: https://stackoverflow.com/a/41425878
The binary method is the fastest. My implementation stores data in binary format.
If somebody wants to have another format then a simple converter is needed.
"Any language can read binary files if they just know the shape, data type and whether it's row or column based."
'''
class StoreToDisk(Reader):


    def run(self):

        '''
        Initialize variables needed to update animation of the buffer.
        '''
        print('1')
        # dot
        self.r = 0  # 90  * (np.pi/180)
        # the length of the arrow
        self.t = 50000
        # max as a rotation angle
        self.maxN = 2 * np.pi

        self.fig = plt.figure()
        self.ax = self.fig.gca(projection='polar')
        self.fig.canvas.set_window_title('Circular Buffer')
        self.ax.plot(self.r, self.t, color='b', marker='o', markersize='3')
        self.ax.set_theta_zero_location('N')
        self.ax.set_theta_direction(-1)
        self.ax.set_ylim(0, 1.02 * self.t)
        self.ax.set_yticklabels([])
        print('2')
        # set labels
        self.label_pos = np.linspace(0.0, 2 * np.pi, 4, endpoint=False)
        self.ax.set_xticks(self.label_pos)
        self.label_cols = [0, self.lenArray / 4, self.lenArray / 2, self.lenArray - self.lenArray / 4]
        self.ax.set_xticklabels(self.label_cols, size=24)

        print('3')
        # defining two lines corresponding to writer and reader indices
        self.line1, = self.ax.plot([0, 0], [0, self.t], color='b', linewidth=1, label='Producer')
        self.line2, = self.ax.plot([0, 0], [0, self.t], color='r', linewidth=1, label='Consumer')
        self.ax.legend(loc=4)

        '''
        Variables specifically for storing the data
        '''
        self.storefrom=0
        self.storeNo=0



        # buttons
        class Index(object):
            ind = 0

            def __init__(self,requestor):
                self.requestor=requestor


            def START(self, event):
                print('START')
                #perform storing operation
                self.requestor.store()


        print('4')
        callback = Index(self)
        astart = plt.axes([0.82, 0.05, 0.1, 0.075])
        bstart = Button(astart, 'START')
        bstart.on_clicked(callback.START)

        #No of records to acquire box
        axbox = plt.axes([0.22, 0.9, 0.22, 0.075])
        text_box = TextBox(axbox, 'No. Of Rec. To Acq.', initial='0')
        text_box.on_submit(self.submitNoOfRec)


        #From which record to acquire
        axbox2 = plt.axes([0.76, 0.9, 0.22, 0.075])
        self.text_box2 = TextBox(axbox2, 'From which rec.', initial='0')
        self.text_box2.on_submit(self.submitFromRec)

        #the name o the stored file
        self.FileName = 'initialName'
        axbox3 = plt.axes([0.825, 0.80, 0.15, 0.075])
        self.text_box3 = TextBox(axbox3, 'From which rec.', initial=self.FileName)
        self.text_box3.on_submit(self.submitFileName)


        print('5')

        self.updateWriter(2000)

        plt.ion()
        print('6')
        plt.show()
        while True:
            plt.pause(0.1)
            #update writer
            with self.lockArrInd.get_lock():
                # currIndex - index of the location in memory where the last produced samples are situated
                indexWrit = self.lockArrInd.value
                self.updateWriter(indexWrit)



    def updateWriter(self, angle):
        plt.pause(0.05)
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line1.set_data([newvalue, newvalue], [0, self.t])
        return self.line1,

    def updateReader(self, angle):
        #plt.pause(0.05)
        angle = angle % self.lenArray
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line2.set_data([newvalue, newvalue], [0, 40000])
        return self.line2,
        # np.save()

    def submitNoOfRec(self,text):
        self.storeNo=int(text)
        self.noOfRecords=self.storeNo
        self.oneFrame=self.noOfRecords*self.noOfSamples
        # change size of temp array
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        print(text)

    def submitFromRec(self, text):
        self.indexRead=int(text)
        self.updateReader(self.indexRead)
        print(text)

    def submitFileName(self, text):
        self.FileName=text
        print(text)

    def store(self):
        data = self.read()
        print('len of acq arr ', len(data))
        dirname = os.path.dirname(__file__)
        name='file1'
        filename = os.path.join(dirname, '/StoredData/', self.FileName)
        pathset = os.path.join(dirname, "/StoredData/")
        # check the directory does not exist
        if not (os.path.exists(pathset)):
            # create the directory you want to save to
            os.mkdir(pathset)
        print('the file is stored : ', filename)
        np.save(filename, data)

        #update both index and hand of the clock
        self.indexRead=(self.indexRead+len(data)) % self.lenArray
        self.updateReader(self.indexRead)
        print('index of next Read: ' , self.indexRead)
        self.text_box2.set_val(self.indexRead)
        #self.text_box2.on_submit(self.indexRead)





if __name__ == '__main__':
    __spec__ = None
    #dimensions of record list
    noOfSamples = 2240 #[samples]
    noOfRecords = 2240 #[records]
    #shared variable index
    pointerToLastProduced = Value('i', 0)
    #big array of samples 
    global ArrayOfSamples
    ArrayOfSamples =   mp.RawArray(ct.c_ushort, 200000000)

    #producer which writes  (Shared memory objects must be used as initialization arguments to the Process object.)
    #start prducer 1
    p1 = WriterADQ14(lockArrInd=pointerToLastProduced,name='ADQ14',shared_array=ArrayOfSamples,noOfSamples=noOfSamples, noOfRecords=noOfRecords)

    #start consumer 3
    # p3 = ReaderGradientPlot(lockArrInd=pointerToLastProduced, name='Plot Gradient Process', shared_array=ArrayOfSamples,
    #                 noOfSamples=noOfSamples, noOfRecords=noOfRecords, modeVis = True)
    # p3.daemon = True

    # start consumer 4
    # p4 = ReaderGradientPlotAnimated(lockArrInd=pointerToLastProduced, name='Animated Plot Gradient Process', shared_array=ArrayOfSamples,
    #                         noOfSamples=noOfSamples, noOfRecords=noOfRecords, modeVis=False)
    # p4.daemon = True

    # start consumer 5
    p5 = StoreToDisk(lockArrInd=pointerToLastProduced, name='Storing to Disk Process', shared_array=ArrayOfSamples,
                             noOfSamples=noOfSamples, noOfRecords=noOfRecords, modeVis=False)

    p1.start()
    time.sleep(2)
    # p3.start()
    # p4.start()
    p5.start()
    print('started')

    p1.join()
    # p3.join()
    # p4.join()
    p5.join()




    
    
'''
Benchmarks
'''   
    
#packetSize=10    
#arraySize=90
#indexOfProducer=16
#index = 84
#
#start_time = time.time()
#for x in range(0, 300000):
#    (arraySize - index + indexOfProducer)%arraySize
#elapsed_time = time.time() - start_time
#print(elapsed_time)
#start_time = time.time()
#
#
##sligthly slower solution but better in this case because it directly splits into two different situations
#for x in range(0, 300000):
#    if(index+packetSize<=arraySize):
#        indexOfProducer-index
#    else:
#        arraySize-index + indexOfProducer
#elapsed_time = time.time() - start_time
#print(elapsed_time)

'''
Animation of circular buffer
'''
# https://stackoverflow.com/questions/47557130/matplotlib-line-rotation-or-animation

# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation
# import time
#
# #dot
# r = 0#90  * (np.pi/180)
# #the length of the arrow
# t = 50000
#
# fig = plt.figure()
# ax = fig.gca(projection = 'polar')
# fig.canvas.set_window_title('Circular Buffer')
# ax.plot(r, t, color ='b', marker = 'o', markersize = '3')
# ax.set_theta_zero_location('N')
# ax.set_theta_direction(-1)
# ax.set_ylim(0,1.02*t)
#
# line1, = ax.plot([0, 0],[0,t], color = 'b', linewidth = 1)
# line2, = ax.plot([0, 0],[0,t], color = 'r', linewidth = 1)
#
# def updateWriter(angle):
#     line1.set_data([angle, angle],[0,t])
#     return line1,
#
# def updateReader(angle):
#     line2.set_data([angle, angle],[0,40000])
#     return line2,
#
# frames = np.array([np.linspace(0,2*np.pi,1000),np.linspace(2*np.pi,0,1000)])
# print(np.shape(frames))
#
# plt.show()
#
# #plt.ion()
# #ani = matplotlib.animation.FuncAnimation(fig, update, frames=frames, blit=True, interval=10)
# for x in range(0, 500):
#     plt.pause(0.1)
#     for y in range(0,2):
#         if(y==0):
#             updateWriter(frames[y][x])
#         if(y==1):
#             updateReader(frames[y][x])
