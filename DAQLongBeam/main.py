'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN

Version: v.2.0
Improvements: Visualisation of Circular buffer is now available to each readProcess to run as addition. Two read methods added - one with
visualisation and one without. Now every class which inherits from Reader will have access to it.

Script for acquisition and consumption of longitudinal beam profile data from Proton Synchrotron through ADQ14. Object oriented version.

Note: The multiprocessing features sometimes doesn't work perfectly on Windows machines, the solution can be using PyCharm instead of
Spyder.
'''

from multiprocessing import Process, Value
import numpy as np
import multiprocessing as mp
import ctypes  as ct
import time

from CustomReaders.ReaderGradientPlot import ReaderGradientPlot
from CustomReaders.ReaderGradientPlotAnimated import ReaderGradientPlotAnimated
from CustomReaders.StoreToDisk import StoreToDisk
from CustomReaders.BunchLengthEstimator import BunchLengthEstimator
from CustomWriters.WriterADQ14 import WriterADQ14


if __name__ == '__main__':
    __spec__ = None
    # dimensions of record list
    noOfSamples = 2240  # [samples]
    noOfRecords = 2240  # [records]
    # shared variable index
    pointerToLastProduced = Value('i', 0)
    # big array of samples
    global ArrayOfSamples
    ArrayOfSamples = mp.RawArray(ct.c_ushort, 200000000)

    # producer which writes  (Shared memory objects must be used as initialization arguments to the Process object.)
    # start prducer 1
    p1 = WriterADQ14(lockArrInd=pointerToLastProduced, name='ADQ14', shared_array=ArrayOfSamples,
                     noOfSamples=noOfSamples, noOfRecords=noOfRecords, cpp=False)

    # start consumer 3
    # p3 = ReaderGradientPlot(lockArrInd=pointerToLastProduced, name='Plot Gradient Process', shared_array=ArrayOfSamples,
    #                 noOfSamples=noOfSamples, noOfRecords=noOfRecords, modeVis = False)
    # p3.daemon = True

    # start consumer 4
    p4 = ReaderGradientPlotAnimated(lockArrInd=pointerToLastProduced, name='Animated Plot Gradient Process',
                                    shared_array=ArrayOfSamples,
                                    noOfSamples=noOfSamples, noOfRecords=noOfRecords, modeVis=False)
    p4.daemon = True

    # start consumer 5
    # p5 = StoreToDisk(lockArrInd=pointerToLastProduced, name='Storing to Disk Process', shared_array=ArrayOfSamples,
    #                  noOfSamples=noOfSamples, noOfRecords=noOfRecords, modeVis=False)

    #start consumer 6
    p6 = BunchLengthEstimator(lockArrInd=pointerToLastProduced, name='Bunch Length Measurement', shared_array=ArrayOfSamples,
                     noOfSamples=noOfSamples, noOfRecords=noOfRecords/16, modeVis=True)


    p1.start()
    p6.start()
    time.sleep(2)
    # p3.start()
    p4.start()
    # p5.start()
    print('started')

    p1.join()
    # p3.join()
    p4.join()
    # p5.join()
    p6.join()

'''
Benchmarks
'''

# packetSize=10
# arraySize=90
# indexOfProducer=16
# index = 84
#
# start_time = time.time()
# for x in range(0, 300000):
#    (arraySize - index + indexOfProducer)%arraySize
# elapsed_time = time.time() - start_time
# print(elapsed_time)
# start_time = time.time()
#
#
##sligthly slower solution but better in this case because it directly splits into two different situations
# for x in range(0, 300000):
#    if(index+packetSize<=arraySize):
#        indexOfProducer-index
#    else:
#        arraySize-index + indexOfProducer
# elapsed_time = time.time() - start_time
# print(elapsed_time)

'''
Animation of circular buffer
'''
# https://stackoverflow.com/questions/47557130/matplotlib-line-rotation-or-animation

# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation
# import time
#
# #dot
# r = 0#90  * (np.pi/180)
# #the length of the arrow
# t = 50000
#
# fig = plt.figure()
# ax = fig.gca(projection = 'polar')
# fig.canvas.set_window_title('Circular Buffer')
# ax.plot(r, t, color ='b', marker = 'o', markersize = '3')
# ax.set_theta_zero_location('N')
# ax.set_theta_direction(-1)
# ax.set_ylim(0,1.02*t)
#
# line1, = ax.plot([0, 0],[0,t], color = 'b', linewidth = 1)
# line2, = ax.plot([0, 0],[0,t], color = 'r', linewidth = 1)
#
# def updateWriter(angle):
#     line1.set_data([angle, angle],[0,t])
#     return line1,
#
# def updateReader(angle):
#     line2.set_data([angle, angle],[0,40000])
#     return line2,
#
# frames = np.array([np.linspace(0,2*np.pi,1000),np.linspace(2*np.pi,0,1000)])
# print(np.shape(frames))
#
# plt.show()
#
# #plt.ion()
# #ani = matplotlib.animation.FuncAnimation(fig, update, frames=frames, blit=True, interval=10)
# for x in range(0, 500):
#     plt.pause(0.1)
#     for y in range(0,2):
#         if(y==0):
#             updateWriter(frames[y][x])
#         if(y==1):
#             updateReader(frames[y][x])
