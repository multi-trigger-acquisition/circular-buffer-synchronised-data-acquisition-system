# -*- coding: utf-8 -*-
"""
Created on Wed Jul 18 09:22:26 2018

@author: Pawel Kozlowski, Alexandre Lasheen

"""

from __future__ import print_function

import numpy as np
import numpy.ctypeslib as ctl
import os
import sys
import subprocess
import ctypes
import argparse
import ctypes as ct
import multiprocessing
import multiprocessing as mp


def compile():
    # from blond import basepath
    path = os.path.realpath(__file__)
    basepath = os.sep.join(path.split(os.sep)[:-1])

    print(basepath)
    print(os.listdir(basepath))
    print(os.listdir(basepath+'/cpp_routines'))


    parser = argparse.ArgumentParser(description='Run python setup_cpp.py to'
                                     ' compile the cpp routines needed from BLonD')

    parser.add_argument('-p', '--parallel',
                        default=False, action='store_true',
                        help='Produce Multi-threaded code. Use the environment'
                        ' variable OMP_NUM_THREADS=xx to control the number of'
                        ' threads that will be used.'
                        ' Default: Serial code')

    parser.add_argument('-b', '--boost', type=str, nargs='?', const='',
                        help='Use boost library to speedup synchrotron radiation'
                        ' routines. If the installation path of boost differs'
                        ' from the default, you have to pass it as an argument.'
                        ' Default: Boost will not be used')

    parser.add_argument('-c', '--compiler', type=str, default='g++',
                        help='C++ compiler that will be used to compile the'
                        ' source files. Default: g++')

    # If True you can launch with 'OMP_NUM_THREADS=xx python MAIN_FILE.py'
    # where xx is the number of threads that you want to launch
    parallel = False

    # If True, the boost library would be used
    boost = False
    # Path to the boost library if not in your CPATH (recommended to use the
    # latest version)
    boost_path = None

    # EXAMPLE FLAGS: -Ofast -std=c++11 -fopt-info-vec -march=native
    #                -mfma4 -fopenmp -ftree-vectorizer-verbose=1
    cflags = ['-Ofast', '-std=c++11', '-shared']

    cpp_files = [
        # 'cpp_routines/mean_std_whereint.cpp',
        # os.path.join(basepath, 'cpp_routines/convolution.cpp'),
        os.path.join(basepath, 'cpp_routines/circBuffer.cpp')
    ]



    args = parser.parse_args()
    parallel = args.parallel
    if(args.boost is not None):
        boost = True
        if(args.boost):
            boost_path = os.path.abspath(args.boost)
        else:
            boost_path = ''
        cflags += ['-I', boost_path]
    compiler = args.compiler

    print('Enable Multi-threaded code: ', parallel)
    print('Using boost: ', boost)
    print('Boost installation path: ', boost_path)
    print('C++ Compiler: ', compiler)
    subprocess.call([compiler, '--version'])
    print('dne')
    try:
        os.remove(os.path.join(basepath, 'cpp_routines/circBuffer.so'))
    except OSError as e:
        pass

    if (parallel is True):
        cflags += ['-fopenmp', '-DPARALLEL', '-D_GLIBCXX_PARALLEL']

    if ('posix' in os.name):
        cflags += ['-fPIC']
        libname = os.path.join(basepath, 'cpp_routines/circBuffer.so')
        command = [compiler] + cflags + ['-o', libname] + cpp_files
        subprocess.call(command)

        print('\nIF THE COMPILATION IS CORRECT A FILE NAMED circBuffer.so SHOULD'
              ' APPEAR IN THE cpp_routines FOLDER. OTHERWISE YOU HAVE TO'
              ' CORRECT THE ERRORS AND COMPILE AGAIN.')

    elif ('win' in sys.platform):

        libname = os.path.join(basepath, 'cpp_routines/circBuffer.dll')

        command = [compiler] + cflags + ['-o', libname] + cpp_files
        subprocess.call(command)

        print('\nIF THE COMPILATION IS CORRECT A FILE NAMED circBuffer.dll SHOULD'
              ' APPEAR IN THE cpp_routines FOLDER. OTHERWISE YOU HAVE TO'
              ' CORRECT THE ERRORS AND COMPILE AGAIN.')

    else:
        print(
            'YOU DO NOT HAVE A WINDOWS OR LINUX OPERATING SYSTEM. ABORTING...')
        sys.exit()






def adqapi_load(path=''):
    if os.name == 'nt':
      if path == '':
          accLib = ct.cdll.LoadLibrary('cpp_routines/circBuffer.dll')
      else:
          accLib = ct.cdll.LoadLibrary(path)
    else:
      if path == '':
          accLib = ct.cdll.LoadLibrary('cpp_routines/circBuffer.so')
      else:
          accLib = ct.cdll.LoadLibrary(path)
    return accLib






def runCompilationCircBuffer(Array):

    compile()
    accLib = adqapi_load()

    class CircBuffer(object):
        obj = None

        def __init__(self, Array, numberOfCells, writerPointer):
            self.obj = accLib.CircBuffer_new(Array, numberOfCells, writerPointer)

        def write(self, ArrayIn, numberOfCellsIn):
            accLib.CircBuffer_write(self.obj, ArrayIn, numberOfCellsIn)

        def read(self, reqDatArr, length):
            reqestedArray = accLib.CircBuffer_read(self.obj, reqDatArr, length)
            return reqestedArray

        def status(self):
            accLib.CircBuffer_status(self.obj)

    ##https://stackoverflow.com/questions/13754220/ctypes-c-segfault-accessing-member-variables
    ##explicit declaration of restype and argtype
    accLib.CircBuffer_new.restype = ct.c_void_p
    # ctypes.addressof()

    ArrayND = np.frombuffer(Array,dtype=np.int16)
    #ArrayND = np.ascontiguousarray(np.zeros(100000000), dtype=np.int16)

    print(type(ArrayND))
    accLib.CircBuffer_new.argtypes = [ctl.ndpointer(np.int16,flags='aligned, c_contiguous'), ctypes.c_int, ctypes.POINTER(ctypes.c_int)]

    accLib.CircBuffer_read.restype = None
    accLib.CircBuffer_read.argtypes = [ctypes.POINTER(CircBuffer.obj),ctl.ndpointer(np.int16,flags='aligned, c_contiguous'), ctypes.c_int]

    accLib.CircBuffer_write.restype = None
    accLib.CircBuffer_write.argtypes =[ctypes.POINTER(CircBuffer.obj),ctl.ndpointer(np.int16,flags='aligned, c_contiguous'), ctypes.c_int]

    accLib.CircBuffer_status.restype = None
    accLib.CircBuffer_status.argtypes =[ct.c_void_p]



    initialReadPtr = 0 # pythonic variable

    #initialize writer counter (indication where the writer should start to write)
    writerPtr=0 #pythonic variable
    WRITERPTR = ct.POINTER(ct.c_int) #type
    WRITERPTR_INST = WRITERPTR(ct.c_int(writerPtr))  #instance of the type

    #container for the requested data
    # noOfSamples=3
    # entries = range(noOfSamples) # 1 million entries
    # reqDatArr_16bit = np.zeros((len(entries)),dtype=np.int16) # prefilled array
    # reqDatArr_16bit = np.ascontiguousarray(reqDatArr_16bit, dtype=np.int16) #making it contiguous
    print(type(ArrayND))
    circBufferCpp = CircBuffer(ArrayND, len(ArrayND), WRITERPTR_INST)
    print('initialized')

    return circBufferCpp

# f.write(B,len(B))
# f.status()
# f.write(C,len(C))
# f.status()
# f.write(D,len(D))
# f.status()
# f.write(E,len(E))
# f.status()
# f.write(B,len(B))
# # f.status()
# # f.write(D,len(D))
# # f.status()
# # f.write(E,len(E))
# f.status()
# #print(A)
# print('before reading 3 elements', reqDatArr_16bit)
# f.read(reqDatArr_16bit, noOfSamples)
# print('after reading 3 elements', reqDatArr_16bit)
# f.read(reqDatArr_16bit, noOfSamples)
# print('after reading 3 elements', reqDatArr_16bit)
# f.read(reqDatArr_16bit, noOfSamples)
# print('after reading 3 elements', reqDatArr_16bit)
# f.read(reqDatArr_16bit, noOfSamples)
# print('after reading 3 elements', reqDatArr_16bit)
# f.read(reqDatArr_16bit, noOfSamples)
# print('after reading 3 elements', reqDatArr_16bit)
#
# f.write(C,len(C))
# f.status()
# f.read(reqDatArr_16bit, noOfSamples)
# print('after reading 3 elements', reqDatArr_16bit)

