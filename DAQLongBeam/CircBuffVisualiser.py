import matplotlib.pyplot as plt
import matplotlib
import numpy as np
'''
Circ. Buffer Visualiser class.
'''
class BufferStateVisualiser():

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords):

        '''
        Initialize variables necessary for the circular buffer.
        '''
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords

        self.oneFrame = noOfSamples * noOfRecords
        self.lenArray = len(shared_array)



        # index of the reader
        indexRead = 0

        '''
        Initialize variables needed to update animation of the buffer.
        '''
        # dot
        self.r = 0  # 90  * (np.pi/180)
        # the length of the arrow
        self.t = 50000
        # max as a rotation angle
        self.maxN = 2 * np.pi

        self.fig = plt.figure()
        self.ax = self.fig.gca(projection='polar')
        self.fig.canvas.set_window_title('Circular Buffer')
        self.ax.plot(self.r, self.t, color='b', marker='o', markersize='3')
        self.ax.set_theta_zero_location('N')
        self.ax.set_theta_direction(-1)
        self.ax.set_ylim(0, 1.02 * self.t)
        self.ax.set_yticklabels([])

        # set labels
        self.label_pos = np.linspace(0.0, 2 * np.pi, 4, endpoint=False)
        self.ax.set_xticks(self.label_pos)
        self.label_cols = [0, self.lenArray / 4, self.lenArray / 2, self.lenArray - self.lenArray / 4]
        self.ax.set_xticklabels(self.label_cols, size=24)

        # defining two lines corresponding to writer and reader indices
        self.line1, = self.ax.plot([0, 0], [0, self.t], color='b', linewidth=1, label='Producer')
        self.line2, = self.ax.plot([0, 0], [0, self.t], color='r', linewidth=1, label='Consumer')
        self.ax.legend(loc=4)

        plt.ion()
        plt.show()

    def updateWriter(self,angle):
        plt.pause(0.05)
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line1.set_data([newvalue, newvalue], [0, self.t])
        return self.line1,

    def updateReader(self,angle):
        plt.pause(0.05)
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line2.set_data([newvalue, newvalue], [0, 40000])
        return self.line2,