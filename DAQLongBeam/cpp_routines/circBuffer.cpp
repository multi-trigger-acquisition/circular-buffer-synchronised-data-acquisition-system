#include <iostream>
using namespace std;

/**

Manual compile steps on Linux
cd C:\MinGW\bin\
g++.exe -c -fPIC C:\ADQAPI_python\FWDAQ\CppCircBuff\cpp_routines\hello_world.cpp -o C:\ADQAPI_python\FWDAQ\CppCircBuff\cpp_routines\hello_world.o
g++.exe -shared -Wl,-soname,C:\ADQAPI_python\FWDAQ\CppCircBuff\libhello_world.so -o C:\ADQAPI_python\FWDAQ\CppCircBuff\libhello_world.so  C:\ADQAPI_python\FWDAQ\CppCircBuff\hello_world.o


How to use:
1. Initialise circular buffer and atomic variable (pointer of the begining of the next chunk of data)
2. Use write function to write new data
3. Use read function to read new data

**/

#include <iostream>



//global variables - to be done in the future
//CircBuffer* bufferObj;
//&int writerPointer;

class CircBuffer{
    public:
        CircBuffer(short int * array, int N, int * wrPtr);
        void read(short int * reqArray, int len);
        void write(short int * array, int Nin);
        void status(){
            //for debugging purpose
            std::cout << "Status : read Ptr: " << this->readPtr << std::endl;
            std::cout << "WrPtr: " << * wrPtr << std::endl;
            //limit output to avoid the burst of huge amount of samples
            if(N>10){
                for (int i=0; i<10; i++)
                cout << i << " " << this->array[i] << endl;
            }
            else{
                for (int i=0; i<N; i++)
                cout << i << " " << this->array[i] << endl;
            }

        }
    private:
        //next sample to read (counter)
        int readPtr;
        //next sample to write (counter)
        int * wrPtr;
        //array pointer
        short int* array;
        //number of cells
        int N;

};

//constructor of circular buffer
CircBuffer::CircBuffer(short int * array, int N, int * wrPtr) {
   //change the following
   this->readPtr = 0;
   cout << "Object is being created, read pointer = " << this->readPtr << " wrPtr: "<<*wrPtr << endl;
   this->wrPtr = wrPtr;
   this->array = array;
   this->N = N;
}

void CircBuffer::read(short int * reqArray, int len) {

    cout << "Read, length = " << len << "Read pointer on the cell "  << this->readPtr << endl;
    //infinite loop to loop over
    bool done = false;
    int producedSamples;
    while (!done)
    {
        //if it is not split
        if(readPtr<=(*wrPtr)){
            producedSamples = (*wrPtr)-readPtr;
            //If produced samples are sufficient to proceed
            if(producedSamples>len){
                //simple operation, just copy data from big buffer to a temporary one
                    for(int i=0; i<len; i++)
                    {
                        reqArray[i] = array[readPtr+i];
                    }
                    readPtr=readPtr+len;
                    done=true;

            }
        }
        //If the data is split
        else{
            producedSamples = (N-readPtr)+ (*wrPtr);
            int first_part;
            int second_part;
            //If produced samples are sufficient to proceed, check if the interesting part is split
            if(producedSamples>len){
                int predictedIndex = readPtr+len;
                //interest. part split
                if(predictedIndex<=N){
                    //proceed in the same way as before
                    for(int i=0; i<len; i++)
                    {
                        reqArray[i] = array[readPtr+i];
                    }
                    readPtr=readPtr+len;
                    done=true;
                }
                //predictedIndex>N
                else{
                    //#first_part & second_part are indices , #find the first part - the end of the buffer/beginning of temporary array
                    first_part = N - readPtr;
                    for(int i=0; i<first_part; i++)
                    {
                        reqArray[i] = array[readPtr+i];
                    }
                    //second part thrown to the beginning
                    second_part = len - first_part;
                    for(int i=0; i<second_part; i++)
                    {
                        reqArray[first_part+i] = array[i];
                    }
                    readPtr=second_part;
                    done=true;

                }

            }
        }
    }

}






void CircBuffer::write(short int * arrayIn, int Nin) {

    //there is no need to go back to the beginning of the circular buffer
    cout << " start " << endl;
    if(*wrPtr + Nin <= N){
    cout << " 1 " << endl;
        for(int i=0; i<Nin; i++)
            {
            array[(*wrPtr)+i]=arrayIn[i];
            //(*wrPtr)++
            }
        *wrPtr = (*wrPtr) + Nin;
    }
    else{
    cout << " 2 " << endl;
        //there is a need for slicing - the circular buffer is full, go to the beginning
        int nextIter = Nin - (N - (*wrPtr));
        int thisIter = Nin - nextIter;

        //write some new data to fill the buffer to the end
        for(int i=0; i<thisIter; i++)
            {
            cout << " 3 " << endl;
            array[(*wrPtr)+i]=arrayIn[i];
            //(*wrPtr)++
            }
        *wrPtr = 0;
        //write the rest of new data to the beginning of the buffer
        cout << " 4 " << endl;
        for(int i=0; i<nextIter; i++)
            {
            array[(*wrPtr)+i]=arrayIn[thisIter + i];
            //(*wrPtr)++
            }
        *wrPtr = (*wrPtr) + nextIter;

    }
    std::cout << "Written" << std::endl;
}





extern "C" {
    //Foo_new = new Foo();
    CircBuffer* CircBuffer_new(short int* array, int N, int * wrPtr){
    return new CircBuffer(array, N, wrPtr);
    }
    void CircBuffer_read(CircBuffer * circBuf, short int *  reqArray, int len){
    return circBuf->read(reqArray, len);
    }
    void CircBuffer_write(CircBuffer * circBuf, short int * arrayIn, int Nin){
    return circBuf->write(arrayIn, Nin);
    }
    void CircBuffer_status(CircBuffer * circBuf){
    circBuf->status();
    }
}

