from multiprocessing import Process
from compileCpp import runCompilationCircBuffer
'''
The process below is responsible for moving data from digitizer memory into circular buffer which is available for other processes.
The availability is due to the fact that the circular buffer is based on shared multiprocessing array. The multiprocessing array
should be wide enough to fully utilize local computer memory.
'''

class Writer(Process):

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords,cpp):
        super(Writer, self).__init__()
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.index = 0
        self.maxLen=len(self.shared_array)
        self.cpp=cpp
        self.circBufferObject=None
        if(self.cpp == True):
            self.compileBuffer()




    # a function to be overwritten
    def run(self):
        print(self.name)

    def compileBuffer(self):
        if (self.cpp == True):
            self.circBufferObject = runCompilationCircBuffer(self.shared_array)
            #print('init of writer done')

    def addToSharedArray(self,dataPacket):
        '''
        The function adds a snippet of irregular data to the circular buffer.
        dataPacket - int16 array
        '''
        #print('self.cpp', self.cpp)
        if self.cpp == True:
            '''
                    Version written in C++
            '''
            print("write with CPP")
            lenPac = len(dataPacket)
            self.circBufferObject.write(dataPacket,lenPac)
            with self.lockArrInd.get_lock():
                self.lockArrInd.value = self.lockArrInd.value+lenPac
        else:
            # print("write with python")
            '''
                    Version writtern in Python
            '''
            # print('produce')
            # sys.stdout.flush()
            # generate random length of array to add
            sizeOfNewPacket = len(dataPacket)
            #print('size of new packet ', sizeOfNewPacket)
            # check if the data shouldn't be passed to the beinning of the circular buffer (split between beginning and the end)
            if (self.index + sizeOfNewPacket <= self.maxLen):
                # the longer the parts of data the less overhead of slicing the data
                # write some new data
                # print(len(shared_array[index:index+sizeOfNewPacket]))
                # print(len(x))
                self.shared_array[self.index:self.index + sizeOfNewPacket] = dataPacket
                #print('added to shared array : ',self.index, ' : ', self.index + sizeOfNewPacket )
                self.index = self.index + sizeOfNewPacket
                # take lock and assign the new value
                with self.lockArrInd.get_lock():
                    self.lockArrInd.value = self.index

            else:  # there is a need for slicing - the circular buffer is full, go to the beginning
                toNextIter = sizeOfNewPacket - (self.maxLen - self.index)
                thisIter = sizeOfNewPacket-toNextIter
                # write some new data to fill the buffer to the end
                self.shared_array[self.index:self.maxLen] = dataPacket[0:thisIter]
                # write the rest of new data to the beginning of the buffer
                self.shared_array[0:toNextIter] = dataPacket[thisIter:sizeOfNewPacket]
                self.index = toNextIter
                # take lock and increment counter
                with self.lockArrInd.get_lock():
                    self.lockArrInd.value = self.index


