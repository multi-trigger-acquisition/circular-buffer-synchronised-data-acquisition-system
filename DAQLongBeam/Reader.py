from multiprocessing import Process
import numpy as np
'''
The process below is simply a consumer of data produced by writer. The only information it uses is the index on which the
writer has written data lately. On the base of the information reader tries to catch up with writer but not overtake the writer.
This is a kind of consumption driven by writer. The data is consumed from shared circular buffer.
'''
class Reader(Process):

    def __init__(self, lockArrInd, name, shared_array, noOfSamples, noOfRecords, modeVis):
        super(Reader, self).__init__()
        self.lockArrInd = lockArrInd
        self.name = name
        self.shared_array = shared_array
        self.noOfSamples = int(noOfSamples)
        self.noOfRecords = int(noOfRecords)
        self.modeVis = modeVis

        '''
                Initialize variables necessary for the circular buffer.
        '''
        self.oneFrame = int(self.noOfSamples * self.noOfRecords)
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        self.lenArray = len(self.shared_array)
        # index of the reader
        self.indexRead = 0

    def run(self):
        #initialisation done
        print(self.name)

    '''
    This is a default - optimized version of reading operation from shared array. The reader reads a given part of shared array.
    The number of records to be read is defined during initialisation of the reader class.
    '''
    def read(self):
        """
        Try to acquire data until it is acquired
        """
        done = False
        # print('Gradient plot: start iteration')
        while done == False:
            # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
            # time.sleep(0.01)
            #with self.lockArrInd.get_lock():
            # currIndex - index of the location in memory where the last produced samples are situated
            indexWrit = self.lockArrInd.value
            # print('Gradient plot: lock taken ', indexWrit)

            # 1. check if there is enough samples for acquisition
            # if it is not split
            # print('Gradient plot: indexRead ', self.indexRead, 'Gradient plot: indexWrit', indexWrit)
            if (self.indexRead <= indexWrit):
                # print('Gradient plot: not split')
                producedSamples = indexWrit - self.indexRead
                # 2. If produced samples are sufficient to proceed
                if (producedSamples > self.oneFrame):
                    # print('read')
                    # consume them in an simple way
                    # simple operation, just copy data from big buffer to a temporary one
                    self.data_16bit[0:self.oneFrame] = self.shared_array[self.indexRead:self.indexRead + self.oneFrame]
                    # print(np.shape(self.shared_array[self.indexRead:self.indexRead + self.oneFrame]))
                    # print(np.shape(self.data_16bit))
                    # print('Gradient plot: simple case')
                    # increase the pointer of consumer to the array
                    self.indexRead = self.indexRead + self.oneFrame
                    done = True
            # If the data is split
            else:
                # print('Gradient plot: split')
                producedSamples = self.lenArray - self.indexRead + indexWrit
                # print('producedSamples ', producedSamples)

                # 2 If produced samples are sufficient to proceed
                if (producedSamples > self.oneFrame):
                    # 3. check if the interesting part is split
                    # if it is not the case
                    # print('Gradient plot: interest. part split')
                    predictedIndex = self.indexRead + self.oneFrame
                    if (predictedIndex <= self.lenArray):
                        # print('1')
                        # proceed in the same way as before
                        self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                           self.indexRead:self.indexRead + self.oneFrame]
                        # increase the pointer of consumer to the array
                        self.indexRead = self.indexRead + self.oneFrame
                        # print('Gradient plot: split buffer but array complete')
                        done = True
                    # if so
                    elif (predictedIndex > self.lenArray):
                        # print('2')
                        # first_part & second_part are indices
                        # find the first part - the end of the buffer/beginning of temporary array
                        first_part = self.lenArray - self.indexRead
                        # print(first_part)
                        self.data_16bit[0:first_part] = self.shared_array[self.indexRead:self.lenArray]
                        # find the second part - the the beginning of the buffer/ the end of the temporary array
                        second_part = self.oneFrame - first_part
                        # print('Gradient plot: second_part', second_part)
                        self.data_16bit[first_part:self.oneFrame] = self.shared_array[0:second_part]
                        self.indexRead = second_part
                        # print('Gradient plot: both buffer and array split')
                        done = True

        return self.data_16bit



    '''
    The following method enable not only to read data from shared circular buffer but also plot the state of pointer of consumer relative
    to the pointer of producer. This will also do some performance calculations. Make sure that Circ. Buffer Visualiser object has been initialised before.
    bufferStVis = BufferStateVisualiser(lockArrInd, name, shared_array, noOfSamples, noOfRecords)
    '''
    def readWithBufVis(self, TB=None):
        # print('try to read with vis')
        if(TB==None):
            print('Initialize Buffer beforehand')
        else:
            #print('TB!=None')
            """
                    Try to acquire data until it is acquired
            """
            done = False
            # print('Gradient plot: start iteration')
            while done == False:
                # (arraySize - index + indexOfProducer)%arraySize - distance from pointer of consumer to pointer of producer
                # time.sleep(0.01)
                with self.lockArrInd.get_lock():
                    # currIndex - index of the location in memory where the last produced samples are situated
                    indexWrit = self.lockArrInd.value
                    TB.updateWriter(indexWrit)
                    # print('indexWrit loop')
                # 1. check if there is enough samples for acquisition
                # if it is not split
                # print('Gradient plot: indexRead ', self.indexRead, 'Gradient plot: indexWrit', indexWrit)
                if (self.indexRead <= indexWrit):
                    # print('Gradient plot: not split')
                    producedSamples = indexWrit - self.indexRead
                    # 2. If produced samples are sufficient to proceed
                    if (producedSamples >= self.oneFrame):
                        # consume them in an simple way
                        # simple operation, just copy data from big buffer to a temporary one
                        self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                           self.indexRead:self.indexRead + self.oneFrame]
                        # increase the pointer of consumer to the array
                        self.indexRead = self.indexRead + self.oneFrame
                        done = True
                        TB.updateReader(self.indexRead)
                # If the data is split
                else:
                    # print('Gradient plot: split')
                    producedSamples = (self.lenArray - self.indexRead) + indexWrit
                    # print('producedSamples', producedSamples)
                    # print('self.lenArray', self.lenArray)
                    # print('self.indexRead', self.indexRead)
                    # print('indexWrit', indexWrit)
                    # 2 If produced samples are sufficient to proceed
                    if (producedSamples > self.oneFrame):
                        # 3. check if the interesting part is split
                        # if it is not the case
                        # print('Gradient plot: interest. part split')
                        predictedIndex = self.indexRead + self.oneFrame
                        if (predictedIndex <= self.lenArray):
                            # print('predictedIndex', predictedIndex)
                            # proceed in the same way as before
                            self.data_16bit[0:self.oneFrame] = self.shared_array[
                                                               self.indexRead:self.indexRead + self.oneFrame]
                            # increase the pointer of consumer to the array
                            self.indexRead = self.indexRead + self.oneFrame
                            # print('Gradient plot: split buffer but array complete')
                            done = True
                            TB.updateReader(self.indexRead)
                        # if so
                        elif (predictedIndex > self.lenArray):
                            # first_part & second_part are indices
                            # find the first part - the end of the buffer/beginning of temporary array
                            first_part = self.lenArray - self.indexRead
                            # print(first_part)
                            self.data_16bit[0:first_part] = self.shared_array[self.indexRead:self.lenArray]
                            # find the second part - the the beginning of the buffer/ the end of the temporary array
                            second_part = self.oneFrame - first_part
                            # print('Gradient plot: second_part', second_part)
                            self.data_16bit[first_part:self.oneFrame] = self.shared_array[0:second_part]
                            self.indexRead = second_part
                            # print('Gradient plot: both buffer and array split')
                            done = True
                            TB.updateReader(self.indexRead)

            return self.data_16bit