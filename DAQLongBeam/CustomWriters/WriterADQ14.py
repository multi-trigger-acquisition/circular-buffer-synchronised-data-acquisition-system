'''
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN, Warsaw University Of Technology

Disclaimer : the data acquisition part of the code was written and belongs to SP-Devices (Teledyne Signal Processing Devices Sweden AB).
'''

import numpy as np
import time
import ctypes  as ct
import Writer
import sys
import os
from Writer import Writer
import matplotlib.pyplot as plt
from matplotlib.widgets import Button

#ADQ14 libraries

sys.path.insert(1, os.path.dirname(os.path.realpath(__file__))+'/../../..')
from modules.example_helpers import *



class WriterADQ14(Writer):

    def run(self):

        #compile c++ buffer
        self.compileBuffer()

        print(self.name)
        # Record settings
        '''
        Note: self.noOfRecords in readers correspond to the matrix image vertical size. Here It is more of the length
        of execution - how long the digitizer has to read
        '''
        number_of_records = 10000
#         number_of_records = 33089*5 + 10000
        samples_per_record = self.noOfSamples  # 2300

        # Print metadata in headers
        print_headers = False

        # DMA transfer buffer settings
        transfer_buffer_size = 65536
        num_transfer_buffers = 8

        # DMA flush timeout in seconds
        flush_timeout = 0.5

        # Load ADQAPI
        ADQAPI = adqapi_load()

        # Create ADQControlUnit
        adq_cu = ct.c_void_p(ADQAPI.CreateADQControlUnit())

        # Enable error logging from ADQAPI
        ADQAPI.ADQControlUnit_EnableErrorTrace(adq_cu, 3, '.')

        # Find ADQ devices
        ADQAPI.ADQControlUnit_FindDevices(adq_cu)
        n_of_ADQ = ADQAPI.ADQControlUnit_NofADQ(adq_cu)
        print('Number of ADQ found:  {}'.format(n_of_ADQ))

        # Exit if no devices were found
        if n_of_ADQ < 1:
            print('No ADQ connected.')
            ADQAPI.DeleteADQControlUnit(adq_cu)
            adqapi_unload(ADQAPI)
            sys.exit(1)

        # Select ADQ
        if n_of_ADQ > 1:
            adq_num = int(input('Select ADQ device 1-{:d}: '.format(n_of_ADQ)))
        else:
            adq_num = 1

        print_adq_device_revisions(ADQAPI, adq_cu, adq_num)

        # Set clock source
        ADQ_CLOCK_INT_INTREF = 0
        ADQAPI.ADQ_SetClockSource(adq_cu, adq_num, ADQ_CLOCK_INT_INTREF)

        # Maximum number of channels for ADQ14 FWPD is four but we use only one
        max_number_of_channels = 1

        # check if usb3 is on
        usb3 = ADQAPI.ADQ_IsUSB3Device(adq_cu, adq_num)
        if (usb3 == 1):
            print('USB3 connected')
        else:
            print('USB3 not connected')

        # Setup test pattern
        #     0 enables the analog input from the ADCs
        #   > 0 enables a specific test pattern
        # Note: Default is to enable a test pattern (4) and disconnect the
        #       analog inputs inside the FPGA.
        ADQAPI.ADQ_SetTestPatternMode(adq_cu, adq_num, 0)

        '''
        Set gain and offset.
        unsigned int SetGainAndOﬀset (unsigned char Channel, int Gain, int Oﬀset)
        The gain value to set, normalized to 10 bits. A value of 1024 corresponds to unity gain. The allowed range is -32768 to 32767
        The oﬀset value to set. An oﬀset value of 8 will changed the oﬀset by 8 codes (multiplied by the gain setting). The allowed range is -32768 to 32767.
        '''
        ch = 1
        ADQAPI.ADQ_SetGainAndOffset(adq_cu, adq_num, ch,
                                    1024,
                                    0)
        '''
        Set bias offset - The DC-biasing circuitry is heavily ﬁltered, and a change in bias level will typically take around 3-4
        seconds to take eﬀect. Also limits settling time to 100ms.
        '''
        adjustable_bias = -17000  # Codes
        ADQAPI.ADQ_SetAdjustableBias(adq_cu, adq_num, 1, adjustable_bias)

        # Set trig mode
        SW_TRIG = 1
        EXT_TRIG_1 = 2
        EXT_TRIG_2 = 7
        EXT_TRIG_3 = 8
        LVL_TRIG = 3
        INT_TRIG = 4
        LVL_FALLING = 0
        LVL_RISING = 1
        trig_type = EXT_TRIG_1
        success = ADQAPI.ADQ_SetTriggerMode(adq_cu, adq_num, trig_type)
        if (success == 0):
            print('ADQ_SetTriggerMode failed.')
        success = ADQAPI.ADQ_SetLvlTrigLevel(adq_cu, adq_num, 0)
        if (success == 0):
            print('ADQ_SetLvlTrigLevel failed.')
        success = ADQAPI.ADQ_SetTrigLevelResetValue(adq_cu, adq_num, 1000)
        if (success == 0):
            print('ADQ_SetTrigLevelResetValue failed.')
        success = ADQAPI.ADQ_SetLvlTrigChannel(adq_cu, adq_num, 1)
        if (success == 0):
            print('ADQ_SetLvlTrigChannel failed.')
        success = ADQAPI.ADQ_SetLvlTrigEdge(adq_cu, adq_num, LVL_RISING)
        if (success == 0):
            print('ADQ_SetLvlTrigEdge failed.')


        # delay
        x = ct.c_char(37)
        success = ADQAPI.ADQ_SetExternalTriggerDelay(adq_cu, adq_num, x)
        if (success == 0):
            print('ADQ_SetExternalTriggerDelay failed.')
        y= ct.c_int32(1)
        success = ADQAPI.ADQ_SetExternTrigEdge(adq_cu, adq_num, y)
        if (success == 0):
            print('ADQ_SetExternTrigEdge failed.')


        # Setup acquisition
        channels_mask = 0x1
        # noOfHoldOffSamples may be useful to shift every acquisition for some samplesto center on the beam profile
        noOfHoldOffSamples = 0
        # may be useful when triggering on the threshold of the beam profile
        noOfPreTriggerSamples = 0
        ADQAPI.ADQ_TriggeredStreamingSetup(adq_cu, adq_num, number_of_records, samples_per_record,
                                           noOfPreTriggerSamples, noOfHoldOffSamples, channels_mask)

        success = ADQAPI.ADQ_SetStreamStatus(adq_cu, adq_num, 1);
        if (success == 0):
            print('setting stream status to 1 (turning on) failed.')
        # success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 1,0);
        # if (success == 0):
        #    print('using DRAM as FIFO failed')
        # success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 2,1);
        # if (success == 0):
        #    print('turning off headers - setting to raw data failed')
        # success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 3,1);
        # if (success == 0):
        #    print('setting the stream channel mask failed')

        # Get number of channels from device
        number_of_channels = 1  # ADQAPI.ADQ_GetNofChannels(adq_cu, adq_num)

        # Setup size of transfer buffers
        print('Setting up streaming...')
        ADQAPI.ADQ_SetTransferBuffers(adq_cu, adq_num, num_transfer_buffers, transfer_buffer_size)

        '''
        Create button for safe disconnection of the device
        '''
        # # button for save closing from the application
        # class Index(object):
        #     ind = 0
        #
        #     def __init__(self, requestor):
        #         self.requestor = requestor
        #
        #     def CLOSE(self, event):
        #         print('Disconnect ADQ14')
        #         # Stop streaming
        #         ADQAPI.ADQ_StopStreaming(adq_cu, adq_num)
        #         # Delete ADQ device handle
        #         ADQAPI.ADQControlUnit_DeleteADQ(adq_cu, adq_num)
        #         # Delete ADQControlunit
        #         ADQAPI.DeleteADQControlUnit(adq_cu)
        #
        #
        # # stop button
        # callback = Index(self)
        # astart = plt.axes([0.82, 0.05, 0.1, 0.075])
        # self.bstart = Button(astart, 'CLOSE')
        # self.bstart.on_clicked(callback.CLOSE)
        # plt.pause(0.01)
        # plt.ion()
        # plt.show()

        '''
        Continue setting the ADQ14
        '''

        # Start streaming
        print('Collecting data, please wait...')
        ADQAPI.ADQ_StopStreaming(adq_cu, adq_num)
        ADQAPI.ADQ_StartStreaming(adq_cu, adq_num)

        # Allocate target buffers for intermediate data storage one buffer for one channel
        #
        Int16Arr = ct.c_int16 * transfer_buffer_size
        # pointer to the array
        Int16ArrPtr = ct.POINTER(Int16Arr)
        # array of pointers (of buffers)
        Int16ArrPtrArr = Int16ArrPtr * number_of_channels
        # new type is defined (human readable name)
        BUFFERS = Int16ArrPtrArr
        # instantiate the type
        target_buffers = BUFFERS()
        print('len of target buffers', len(target_buffers))

        for bufp in target_buffers:
            bufp.contents = (ct.c_int16 * transfer_buffer_size)()

        # Create some buffers for the full records
        data_16bit = [np.array([], dtype=np.int16)]

        ## 2D array of
        ## Allocate target buffers for headers
        # headerbuf_list = [(HEADER*number_of_records)()]
        ## Create an C array of pointers to header buffers (by ensuring a space)
        # headerbufp_list = ((ct.POINTER(HEADER*number_of_records)))()

        # create array of headers
        HeadArr = HEADER * number_of_records

        # Allocate target buffers for headers
        # instantiate header array
        HeaderArray = HeadArr()
        headerbuf_list = [HeaderArray for ch in range(number_of_channels)]

        # Create an C array of pointers to header buffers
        # create pointer to the array
        HeadArrPtr = ct.POINTER(HeadArr)
        # create array of pointers to headers
        HeadArrPtrArr = HeadArrPtr * number_of_channels
        # new type is defined (human readable name)
        HEADER_BUFFERS = HeadArrPtrArr
        # instantiate the type
        headerbufp_list = HEADER_BUFFERS()
        # Initiate pointers with allocated header buffers
        for ch, headerbufp in enumerate(headerbufp_list):
            headerbufp.contents = headerbuf_list[ch]

        # Create a second level pointer to each buffer pointer,
        # these will only be used to change the bufferp_list pointer values
        headerbufvp_list = [ct.cast(ct.pointer(headerbufp_list[ch]), ct.POINTER(ct.c_void_p)) for ch in
                            range(number_of_channels)]

        # Allocate length output variable
        # make a space for 4 elements and populate the elements
        # create type
        samples_added = (1 * ct.c_uint)()
        # populate the type
        for ind in range(len(samples_added)):
            samples_added[ind] = 0

        headers_added = (1 * ct.c_uint)()
        for ind in range(len(headers_added)):
            headers_added[ind] = 0

        header_status = (1 * ct.c_uint)()
        for ind in range(len(header_status)):
            header_status[ind] = 0

        # Generate triggers if software trig is used
        # why the trigger is not armed beforehand?
        # if (trig_type == 1):
        #  for trig in range(number_of_records):
        #    ADQAPI.ADQ_SWTrig(adq_cu, adq_num)
        #
        # print('Waiting for data...')

        # Collect data until all requested records have been recieved
        records_completed = 0
        headers_completed = 0
        records_completed_cnt = 0
        ltime = time.time()
        buffers_filled = ct.c_uint(0)


        # Read out data until records_completed for ch A is number_of_records
        while (number_of_records > records_completed):
            #if safe interrupt is on
            # plt.pause(0.0005)
            buffers_filled.value = 0
            collect_result = 1
            poll_time_diff_prev = time.time()
            # Wait for next data buffer
            while ((buffers_filled.value == 0) and (collect_result)):
                collect_result = ADQAPI.ADQ_GetTransferBufferStatus(adq_cu, adq_num,
                                                                    ct.byref(buffers_filled))
                poll_time_diff = time.time()

                if ((poll_time_diff - poll_time_diff_prev) > flush_timeout):
                    # Force flush
                    # print('No data for {}s, flushing the DMA buffer.'.format(flush_timeout))
                    # status = ADQAPI.ADQ_FlushDMA(adq_cu, adq_num);
                    # print('ADQAPI.ADQ_FlushDMA returned {}'.format(adq_status(status)))
                    poll_time_diff_prev = time.time()
                    # streamOverflow = ADQAPI.ADQ_GetStreamOverflow(adq_cu, adq_num)
                    # print('ADQAPI.ADQ_GetStreamOverflow returned {}'.format(streamOverflow))

            # Fetch data and headers into target buffers
            status = ADQAPI.ADQ_GetDataStreaming(  # adq_cu, adq_num - handles to the device cu - control unit
                adq_cu, adq_num,
                target_buffers,
                #  The data and metadata is delivered to the user buﬀers: target_buﬀers and target_headers, respectively.
                headerbufp_list,
                #  suppress data collection from specific channels
                channels_mask,
                # byref works almost like pointer() , but with when we dont want to access it from python
                # samples_added - how many samples that were added to the user data buffers
                # This allows the user to
                # increment the data buﬀer pointers accordingly before the next call to the GetDataStreaming() function.
                ct.byref(samples_added),
                # headers_added and header_status provide the user with information to determine if the header
                # buﬀer pointers should be incremented.
                # The parameter headers_added contains the number of initialized
                # headers as a result of this call to GetDataStreaming()
                ct.byref(headers_added),
                ct.byref(header_status))
            # The header_status parameter is used to denote if the
            # last header is complete or not. E.g. a return value of headers_added = 4 and header_status = 1 signiﬁes 4
            # complete headers, while headers_added = 4 and header_status = 0 would signify 3 complete headers and one
            # incomplete header.

            if status == 0:
                print('GetDataStreaming failed!')
                sys.exit()
            ch = 0
            # for ch in range(number_of_channels):
            if (headers_added[ch] > 0):
                # The last call to GetDataStreaming has generated header data
                if (header_status[ch]):
                    headers_done = headers_added[ch]
                else:
                    # One incomplete header
                    headers_done = headers_added[ch] - 1
                # Update counter counting completed records
                headers_completed += headers_done

                # Update the number of completed records if at least one header has completed
                if (headers_done > 0):
                    records_completed = headerbuf_list[ch][headers_completed - 1].RecordNumber + 1

                # Update header pointer so that it points to the current header
                headerbufvp_list[ch].contents.value += headers_done * ct.sizeof(headerbuf_list[ch]._type_)
                if (headers_done > 0) and (records_completed - records_completed_cnt) > 1000:
                    dtime = time.time() - ltime
                    if (dtime > 0):
                        print('{:d} {:.2f} MB/s'.format(records_completed,
                                                        ((samples_per_record
                                                          * 2
                                                          * (records_completed - records_completed_cnt))
                                                         / (dtime)) / (1024 * 1024)))
                    sys.stdout.flush()
                    records_completed_cnt = records_completed
                    ltime = time.time()

            if (samples_added[ch] > 0):
                # Copy channel data to continuous buffer
                data_buf = np.frombuffer(target_buffers[ch].contents, dtype=np.int16, count=samples_added[ch])
                #print('size of dat buff ', len(data_buf))
                self.addToSharedArray(data_buf)
