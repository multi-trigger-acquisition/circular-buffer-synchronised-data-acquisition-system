from Reader import Reader
#GUI libraries
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import numpy as np
import sys
from CircBuffVisualiser import BufferStateVisualiser

'''
The following class extends the reader class and is used for plotting 2d graph as a gradient of
beam profile intensity during many turns.
'''
class ReaderGradientPlot(Reader):

    #modeVis  - set visualisation mode on or of
    def run(self):
        app = QtWidgets.QApplication(sys.argv)
        form = SecondWindow()
        print(self.name)
        form.setSource(self.shared_array, self.lockArrInd , self.noOfSamples, self.noOfRecords, self.modeVis, self)
        form.show()
        sys.exit(app.exec_())


'''
Class which is necessary for using PyQt5 widget. It defines refresh rate and the data to show. 
The library has been choosen for the sake of high performance way higher than matplotlib offers.
'''
class SecondWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SecondWindow, self).__init__(parent)
        self.setupUi(self)

    def setSource(self, shared_array, lockArrInd , noOfSamples, noOfRecords, modeVis, requestor):
        self.shared_array = shared_array
        self.lockArrInd = lockArrInd
        self.noOfSamples = noOfSamples
        self.noOfRecords = noOfRecords
        #visualisation of the buffer on/off
        self.modeVis = modeVis
        self.requestor=requestor
        if(self.modeVis==True):
            self.bufferStVis = BufferStateVisualiser(self.lockArrInd, 'Vis', self.shared_array, self.noOfSamples, self.noOfRecords)
        '''
                Initialize variables necessary for the circular buffer.
        '''
        self.oneFrame = self.noOfSamples * self.noOfRecords
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        self.lenArray = len(self.shared_array)
        # index of the reader
        self.indexRead = 0

        # uncomment for performance optimization
        self.im_widget.getImageItem().setAutoDownsample(True)

        data = np.arange(0, self.noOfSamples * self.noOfRecords)
        data = (data/(self.noOfSamples*self.noOfRecords)) * 28000
        self.im_widget.setImage((data.reshape(self.noOfSamples, self.noOfRecords)), autoLevels=False,
                            levels=[-32767, 32767], autoHistogramRange=False)

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)

        self.im_widget = pg.ImageView(self)


        # uncomment to hide histogram

        self.im_widget.ui.histogram.hide()
        view = self.im_widget.getView()
        view.setRange(xRange=[1000, 16000])
        self.im_widget.setPredefinedGradient('flame')
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.initialisationFigure()
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.majFigure)
        self.timer.start(1000)
        self.timer2 = QtCore.QTimer(self)
        self.timer2.timeout.connect(self.NumberRefreshPerSecond)
        self.timer2.start(1000)

    def NumberRefreshPerSecond(self):
        # print(self.count)
        self.count = 0


    def majFigure(self):
        self.count = self.count + 1
        if(self.modeVis==True):
            self.data_16bit = self.requestor.readWithBufVis(self.bufferStVis)

        else:
            self.data_16bit = self.requestor.read()

        final = self.data_16bit.reshape(self.noOfSamples, self.noOfRecords)
        print('reshaped', np.shape(final))
        print(np.max(final))
        print(np.min(final))
        #some optimizations during setting the image
        self.im_widget.setImage(final, autoLevels=False,
                                levels=[-32767, 32767], autoHistogramRange=False)

    def initialisationFigure(self):
        self.count = 0
        self.im_widget.show()

    def closeEvent(self, event):
        self.timer.stop()
