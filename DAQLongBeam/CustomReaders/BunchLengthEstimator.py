from Reader import Reader
import numpy as np
from CircBuffVisualiser import BufferStateVisualiser
from numpy import *
import numpy as np
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import random
import matplotlib.pyplot as plt

class BunchLengthEstimator(Reader):

    def run(self):
        global ArrayOfCurves, PtrArr, XmData

        # mode of calulating the threshold - derivative/original signal based
        # 0 - updateThresholdDefault
        # 1 - updateThresholdDerivativePyQT
        # 2 - updateThresholdDerivativeMatplotLib
        functionThres=2



        # 1. create visualiser
        if (self.modeVis == True):
            self.bufferStVis = BufferStateVisualiser(self.lockArrInd, 'Vis', self.shared_array, self.noOfSamples,
                                                     self.noOfRecords)

        # max number of bunches is 84 therefore the number of plots will be like so
        noOfPlots = 84

        # noOfRecords to skip
        # records to skip
        self.RecSkip = 1

        # arrays corresponding to separate plots
        ArrayOfCurves = np.array([None] * noOfPlots)
        ValueToAdd = np.array([0] * noOfPlots)
        # array of pointers to positions
        PtrArr = np.array([0] * noOfPlots)
        # array of data
        XmData = np.array([None] * noOfPlots)
        windowWidth = 500  # width of the window displaying the curve

        if(functionThres==0 or functionThres==1):
            ### START QtApp #####
            app = QtGui.QApplication([])  # you MUST do this once (initialize things)
            ####################

            win = pg.GraphicsWindow(title="Longitudinal bunch length")  # creates a window


            # Xm = linspace(0,0,windowWidth)          # create array that will contain the relevant time series
            # ptr = -windowWidth                      # set first x position
            plot = win.addPlot(title="Bunches",enableMenu=False,name='Longitudinal bunch length')  # creates empty space for the plot in the window
            plot.setLabels(left=('Length', 'units'))
            # plot.setClipToView(1)
            plot.disableAutoRange(axis=1)

            for x in range(0, noOfPlots):
                ArrayOfCurves[x] = plot.plot(pen=(x, noOfPlots))  # create an empty "plot" (a curve to plot)
                PtrArr[x] = -windowWidth
                XmData[x] = linspace(0, 0, windowWidth)

        turnCounter = 0

        # prepare container for new data
        self.oneFrame = int(self.noOfSamples * self.noOfRecords)
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous

        def signaltonoise(a, axis=0, ddof=0):
            a = np.asanyarray(a)
            m = a.mean(axis)
            sd = a.std(axis=axis, ddof=ddof)
            return np.where(sd == 0, 0, m / sd)

        # Realtime data plot. Each time this function is called, the data display is updated
        def updateThresholdDefault():

            if (self.modeVis == True):
                self.data_16bit = self.readWithBufVis(self.bufferStVis)
            else:
                self.data_16bit = self.read()

            datamatrix = self.data_16bit.reshape(self.noOfRecords, self.noOfSamples)

            for noOfRecordx in range(0, self.noOfRecords, self.RecSkip):
                # find intersect points (indices of them)
                # x = np.arange(0, self.noOfSamples)
                g = datamatrix[noOfRecordx]
                minV = np.min(g)
                maxV = np.max(g)
                ptp = maxV - minV
                print(ptp)
                levelOfintersection = minV + 0.4 * ptp
                # f = np.array([levelOfintersection] * self.noOfSamples)
                # filter out noisy turns
                if (ptp < 1300):
                    arr = []
                    idx = np.array(arr)
                else:
                    idx = np.argwhere(np.diff(np.sign(levelOfintersection - g))).flatten()


                # #for debugging purposes
                # if (noOfRecordx == 100):
                #     f=np.array([levelOfintersection]*self.noOfSamples)
                #     x = np.arange(0, self.noOfSamples)
                #     plt.plot(x, f, '-')
                #     plt.plot(x, g, '-')
                #     plt.plot(x[idx], f[idx], 'ro')
                #     plt.show()
                #     print(signaltonoise(g))
                print(idx)
                '''
                    filter wrong results
                '''
                if (g[0] > levelOfintersection):
                    # filter if bunch is incomplete on the begining
                    idx = idx[1:]

                if (g[self.noOfSamples - 1] > levelOfintersection):
                    # filter if bunch is incomplete at the end
                    idx = idx[:len(idx) - 1]

                # if (snr < -100):
                #     arr = []
                #     idx = np.array(arr)
                print(idx)
                counterIdx = 0
                # print(len(idx))
                for x in range(0, noOfPlots):
                    XmData[x][:-1] = XmData[x][1:]  # shift data in the temporal mean 1 sample left

                    if (counterIdx + 2 < len(idx)):
                        value = idx[counterIdx + 1] - idx[counterIdx]  # read line (single value) from the serial port
                        counterIdx += 2
                    else:
                        value = 0
                    if(value>6):
                        XmData[x][-1] = int(value)  # vector containing the instantaneous values
                    else:
                        XmData[x][-1] = int(0)  # vector containing the instantaneous values
                    # print(XmData[x][-1])
                    PtrArr[x] += 1  # update x position for displaying the curve

                    ArrayOfCurves[x].setData(XmData[x])  # set the curve with this data
                    ArrayOfCurves[x].setPos(PtrArr[x], 0)  # set x position in the graph to 0

                QtGui.QApplication.processEvents()  # you MUST process the plot now


        #this function is used to calculate drivativ based threshold bunch detection
        tempArray = np.array([0] * self.noOfSamples)
        def CalcArrOfLen(Arr):
            gx = Arr
            # gx = datamatrix[2700]
            tempArray[1:] = np.diff(gx)
            tempArray[0] = tempArray[1]
            gx = tempArray

            minV = np.min(gx)
            maxV = np.max(gx)
            ptp = maxV - minV
            levelOfintersectionUp = 150  # int(minV + 0.25*ptp)
            levelOfintersectionDown = -150  # int(maxV - 0.25*ptp)

            # gx[(gx > levelOfintersectionUp) & (gx < levelOfintersectionDown)] = 0

            thoseUp = (gx < levelOfintersectionUp)
            thoseDown = (gx > levelOfintersectionDown)

            listOfBunches = []

            # state 0 = zero level, 0/1 - first up, 1/2 second up, 2/3 first down, 3/0 second down, back to 0
            state = 0

            first = None
            last = None

            # is the bunch to be ignored?
            ignore = False
            # check what is on the beginning
            if (thoseUp[0] == False):
                # this is upper diff
                ignore = True
                state = 2
            elif (thoseDown[0] == False):
                # this is lower diff
                ignore = True
                state = 3

            #
            ##optimisation by embedding one if on another
            # Finite state machine
            NoEl = 1
            while NoEl < self.noOfSamples:
                if (state == 0):
                    if (thoseUp[NoEl] == False):
                        # print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                        first = NoEl
                        # number of samples to ignore
                        NoEl = NoEl + 22
                        state = 1
                elif (state == 1):
                    if (thoseUp[NoEl] == True):
                        # print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                        # number of samples to ignore
                        NoEl = NoEl + 22
                        state = 2
                elif (state == 2):
                    if (thoseDown[NoEl] == False):
                        # number of samples to ignore
                        # print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                        NoEl = NoEl + 18
                        state = 3
                elif (state == 3):
                    if (thoseDown[NoEl] == True):
                        # number of samples to ignore
                        # print('state', state,'NoEl',NoEl,'thoseUp[NoEl]',str(thoseUp[NoEl]))
                        last = NoEl
                        if (ignore == False):
                            # add to list
                            listOfBunches.append([first, last])
                        else:
                            ignore = False
                        state = 0
                        NoEl = NoEl + 22
                # always increment by one
                NoEl = NoEl + 1

            return listOfBunches


        def updateThresholdDerivativePyQT():
            # check if it has to be visualised or not
            if (self.modeVis == True):
                self.data_16bit = self.readWithBufVis(self.bufferStVis)
            else:
                self.data_16bit = self.read()

            # acquire packet of data
            datamatrix = self.data_16bit.reshape(self.noOfRecords, self.noOfSamples)

            global ArrayOfCurves, PtrArr, XmData, turnCounter

            for noOfRecordx in range(0, self.noOfRecords, self.RecSkip):
                # array lengths for the turn
                listOfPoints = CalcArrOfLen(datamatrix[noOfRecordx])

                # counterid
                counterIdx = 0

                # updateplots for each turn
                for x in range(0, noOfPlots):
                    XmData[x][:-1] = XmData[x][1:]  # shift data in the temporal mean 1 sample left
                    # print(listOfPoints)
                    if (counterIdx + 2 < len(listOfPoints)):
                        value = listOfPoints[counterIdx][1] - listOfPoints[counterIdx][
                            0]  # calculate lengths by taking 2 consecutive points distance
                        counterIdx += 2
                    else:
                        value = 0
                    XmData[x][-1] = float(value)  # vector containing the instantaneous values
                    # print(XmData[x][-1])
                    PtrArr[x] += 1  # update x position for displaying the curve

                    ArrayOfCurves[x].setData(XmData[x])  # set the curve with this data
                    # ArrayOfCurves[x].setPos(PtrArr[x], 0)  # set x position in the graph to 0

                # Increase the iuncrement value in order to increase performance
                if(turnCounter%100==0):
                    QtGui.QApplication.processEvents()  # you MUST process the plot now

        import matplotlib.pyplot as plt
        import random

        def initialisationMatPlotLib():
            global line_list, ax_1, PtrArr, XmData

            plt.ion()
            self.fig = plt.figure('animation')
            self.fig.clf()

            for x in range(0, noOfPlots):
                #    ArrayOfCurves[x] = plot.plot(pen=(x,noOfPlots)) # create an empty "plot" (a curve to plot)
                PtrArr[x] = -windowWidth
                XmData[x] = np.linspace(0, 0, windowWidth)

            ax_1 = self.fig.add_subplot(111)

            line_list = ax_1.plot(np.zeros((windowWidth, noOfPlots)))

            ax_1.set_xlim((np.arange(windowWidth)[0], np.arange(windowWidth)[-1]))
            # ax_1.set_yticks([])
            ax_1.set_ylim((0, 300))
            # ax_1.set_xlabel('Time [$\\mathrm{\\mu s}$]')

            # plt.tight_layout()
            plt.show(block=False)

            self.fig.canvas.draw()

            self.background = self.fig.canvas.copy_from_bbox(ax_1.bbox)

        def updateThresholdDerivativeMatplotLib():
            global line_list, ax_1, PtrArr, XmData, turnCounter

            if (self.modeVis == True):
                self.data_16bit = self.readWithBufVis(self.bufferStVis)
            else:
                self.data_16bit = self.read()

            datamatrix = self.data_16bit.reshape(self.noOfRecords, self.noOfSamples)


            # updateplots for each turn

            for noOfRecordx in range(0, self.noOfRecords, self.RecSkip):
                # array lengths for the turn
                listOfPoints = CalcArrOfLen(datamatrix[noOfRecordx])

                # counterid
                counterIdx = 0

                for x in range(0, noOfPlots):
                    XmData[x][:-1] = XmData[x][1:]  # shift data in the temporal mean 1 sample left
                    # print(listOfPoints)
                    if (counterIdx + 2 < len(listOfPoints)):
                        value = listOfPoints[counterIdx][1] - listOfPoints[counterIdx][
                            0]  # calculate lengths by taking 2 consecutive points distance
                        counterIdx += 2
                    else:
                        value = 0
                    #        value=x
                    XmData[x][-1] = float(value)  # vector containing the instantaneous values
                    #        print(XmData[x][-1])
                    PtrArr[x] += 1  # update x position for displaying the curve

                    if (noOfRecordx % 100 == 0):
                        line_list[x].set_ydata(XmData[x])
                        if x == 0:
                            #                fig.canvas.restore_region(background)
                            ax_1.draw_artist(ax_1.patch)
                            ax_1.draw_artist(line_list[x])

                #        ArrayOfCurves[x].setData(XmData[x])                     # set the curve with this data
                # ArrayOfCurves[x].setPos(PtrArr[x],0)                   # set x position in the graph to 0


                if (noOfRecordx % 100 == 0):
                        #        fig.canvas.blit(ax_1.bbox)
                    self.fig.canvas.update()
                    self.fig.canvas.flush_events()


        ### MAIN PROGRAM #####
        # this is a brutal infinite loop calling your realtime data plot
        if(functionThres==0):
            while True: updateThresholdDefault()
        elif(functionThres==1):
            while True: updateThresholdDerivativePyQT()
        elif(functionThres==2):
            initialisationMatPlotLib()
            while True: updateThresholdDerivativeMatplotLib()


        ### END QtApp ####
        pg.QtGui.QApplication.exec_()  # you MUST put this at the end
