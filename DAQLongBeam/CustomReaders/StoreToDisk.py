'''
The following class extends the reader class and is used for storing data in hard drive.
According to the post: https://stackoverflow.com/a/41425878
The binary method is the fastest. My implementation stores data in binary format.
If somebody wants to have another format then a simple converter is needed.
"Any language can read binary files if they just know the shape, data type and whether it's row or column based."
'''

import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.widgets import Button, TextBox
from Reader import Reader
import h5py as hp
import time
import datetime

class StoreToDisk(Reader):


    def run(self):

        '''
        Initialize variables needed to update animation of the buffer.
        '''
        # self.autoMode=True
        # self.FileName='auto'
        # dot
        self.r = 0  # 90  * (np.pi/180)
        # the length of the arrow
        self.t = 50000
        # max as a rotation angle
        self.maxN = 2 * np.pi

        self.fig = plt.figure()
        self.ax = self.fig.gca(projection='polar')
        self.fig.canvas.set_window_title('Circular Buffer')
        self.ax.plot(self.r, self.t, color='b', marker='o', markersize='3')
        self.ax.set_theta_zero_location('N')
        self.ax.set_theta_direction(-1)
        self.ax.set_ylim(0, 1.02 * self.t)
        self.ax.set_yticklabels([])

        # set labels
        self.label_pos = np.linspace(0.0, 2 * np.pi, 4, endpoint=False)
        self.ax.set_xticks(self.label_pos)
        self.label_cols = [0, self.lenArray / 4, self.lenArray / 2, self.lenArray - self.lenArray / 4]
        self.ax.set_xticklabels(self.label_cols, size=24)

        # defining two lines corresponding to writer and reader indices
        self.line1, = self.ax.plot([0, 0], [0, self.t], color='b', linewidth=1, label='Producer')
        self.line2, = self.ax.plot([0, 0], [0, self.t], color='r', linewidth=1, label='Consumer')
        self.ax.legend(loc=4)

        '''
        Variables specifically for storing the data
        '''
        self.storefrom=0
        self.storeNo=0

        global continueStoring

        # buttons
        class Index(object):
            ind = 0

            def __init__(self,requestor):
                self.requestor=requestor


            def START(self, event):
                print('START')
                #perform storing operation
                self.requestor.store()

            def STARTCONTINOUSLY(self, event):
                print('START CONTINOUSLY')
                #perform storing operation in a continuous mode
                self.requestor.storeContinously()

            def STOPCONTINOUSLY(self, event):
                global continueStoring
                print('STOP CONTINOUSLY')
                continueStoring = False


        #stert button
        callback = Index(self)
        astart = plt.axes([0.82, 0.05, 0.1, 0.075])
        self.bstart = Button(astart, 'START')
        self.bstart.on_clicked(callback.START)

        #continuous store buttons
        callback1 = Index(self)
        astart1 = plt.axes([0.04, 0.05, 0.25, 0.075])
        self.bstart1 = Button(astart1, 'START CONTINOUSLY')
        self.bstart1.on_clicked(callback1.STARTCONTINOUSLY)

        callback2 = Index(self)
        astart2 = plt.axes([0.04, 0.125, 0.25, 0.075])
        self.bstart2 = Button(astart2, 'STOP CONTINOUSLY')
        self.bstart2.on_clicked(callback2.STOPCONTINOUSLY)
        plt.pause(0.01)

        #No of records to acquire box
        axbox = plt.axes([0.22, 0.9, 0.22, 0.075])
        text_box = TextBox(axbox, 'No. Of Rec. To Acq.', initial='0')
        text_box.on_submit(self.submitNoOfRec)

        #From which record to acquire
        axbox2 = plt.axes([0.76, 0.9, 0.22, 0.075])
        self.text_box2 = TextBox(axbox2, 'From which rec.', initial='0')
        self.text_box2.on_submit(self.submitFromRec)

        #the name o the stored file
        self.FileName = 'initialName'
        axbox3 = plt.axes([0.825, 0.80, 0.15, 0.075])
        self.text_box3 = TextBox(axbox3, 'From which rec.', initial=self.FileName)
        self.text_box3.on_submit(self.submitFileName)

        self.updateWriter(2000)
        plt.ion()

        plt.show()
        while True:
            plt.pause(0.01)
            #update writer
            #with self.lockArrInd.get_lock():
            # currIndex - index of the location in memory where the last produced samples are situated
            indexWrit = self.lockArrInd.value
            self.updateWriter(indexWrit)
            # if(self.autoMode==True):
            #     self.storeContinously()




    def updateWriter(self, angle):
        #plt.pause(0.05)
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line1.set_data([newvalue, newvalue], [0, self.t])
        return self.line1,

    def updateReader(self, angle):
        #plt.pause(0.05)
        angle = angle % self.lenArray
        newvalue = (self.maxN) / (self.lenArray) * (angle - self.lenArray) + self.maxN
        self.line2.set_data([newvalue, newvalue], [0, 40000])
        return self.line2,
        # np.save()

    def submitNoOfRec(self,text):
        self.storeNo=int(text)
        self.noOfRecords=self.storeNo
        self.oneFrame=self.noOfRecords*self.noOfSamples
        # change size of temp array
        self.data_16bit = np.zeros(self.oneFrame, dtype=np.int16)  # prefilled array
        self.data_16bit = np.ascontiguousarray(self.data_16bit, dtype=np.int16)  # making it contiguous
        # print(text)

    def submitFromRec(self, text):
        self.indexRead=int(text)
        self.updateReader(self.indexRead)
        # print(text)

    def submitFileName(self, text):
        self.FileName=text
        # print(text)

    def store(self):
        self.bstart.label.set_text("WAIT")
        time.sleep(0.01)
        plt.pause(0.001)
        self.data = self.read()
        # print('len of acq arr ', len(data))
        dirname = os.path.dirname(__file__)
        self.UnixTime = time.time()
        self.dateX = datetime.datetime.fromtimestamp(self.UnixTime).strftime('%Y-%m-%d_%H-%M-%S')
        name = self.FileName + '_' + str(self.dateX)
        filename = os.path.join(dirname, '/StoredData/', name)
        pathset = os.path.join(dirname, "/StoredData/")
        # check the directory does not exist
        if not (os.path.exists(pathset)):
            # create the directory you want to save to
            os.mkdir(pathset)
        # print('the file is stored : ', filename)
        storeFormat = '.npz'

        if (storeFormat == '.npy'):
            '''
            Store in npy format
            '''
            np.save(filename, self.data)
        elif (storeFormat == '.npz'):
            '''
            Store in npz format
            '''
            np.savez_compressed(filename, data=self.data, UnixTime=self.UnixTime, dateStr=self.dateX)
        elif (storeFormat == '.hdf5'):
            '''
            save in h5py format
            '''
            self.storeH5(filename)

        #update both index and hand of the clock
        #self.indexRead=(self.indexRead+len(data)) % self.lenArray
        self.updateReader(self.indexRead)
        # print('index of next Read: ' , self.indexRead)
        self.text_box2.set_val(self.indexRead)
        #self.text_box2.on_submit(self.indexRead)

        self.bstart.label.set_text("START")
        plt.pause(0.01)

    #continuous mode start/stop
    def storeContinously(self):
        global continueStoring
        continueStoring=True
        self.bstart1.label.set_text("IN PROGRESS")
        #self.continueStoring=True

        counter=0
        while continueStoring==True:
            #read data from circ buffer
            self.data = self.read()
            #write to disk
            # print('len of acq arr ', len(data))
            dirname = os.path.dirname(__file__)
            self.UnixTime=time.time()
            self.dateX = datetime.datetime.fromtimestamp(self.UnixTime).strftime('%Y-%m-%d_%H-%M-%S')
            name = self.FileName + '_' + str(counter).zfill(4) + '_' + str(self.dateX)
            type(name)
            counter+=1
            filename = os.path.join(dirname, '/StoredData/', name)
            pathset = os.path.join(dirname, "/StoredData/")
            # check the directory does not exist
            if not (os.path.exists(pathset)):
                # create the directory you want to save to
                os.mkdir(pathset)
            # print('the file is stored : ', filename)

            storeFormat = '.npz'

            if(storeFormat == '.npy'):
                '''
                Store in npy format
                '''
                np.save(filename, self.data)
            elif (storeFormat == '.npz'):
                '''
                Store in npz format
                '''
                np.savez_compressed(filename, data=self.data, UnixTime=self.UnixTime, dateStr=self.dateX)
            elif(storeFormat == '.hdf5'):
                '''
                save in h5py format
                '''
                self.storeH5(filename)

            '''
            Update both index (for proper logic) and hand of the clock for visual effect
            '''
            #self.indexRead = (self.indexRead + len(data)) % self.lenArray
            self.updateReader(self.indexRead)
            indexWrit = self.lockArrInd.value
            self.updateWriter(indexWrit)
            print('index of next Read: ', self.indexRead)
            self.text_box2.set_val(self.indexRead)
            plt.pause(0.01)
            #update consumer's hand of the clock



        self.bstart1.label.set_text("START CONTINOUSLY")


    def storeH5(self,filename):
        # save mode is write
        saveMode = 'w'
        # save mode is append
        # saveMode = 'a'
        saveFile = hp.File(filename + '.hdf5', saveMode)
        listAttributesToSave = ['data', 'UnixTime']

        #
        for attributeToSaveString in listAttributesToSave:
            attributeToSave = getattr(self, attributeToSaveString)
            saveFile.create_dataset(attributeToSaveString,
                                        data=attributeToSave,
                                        compression="gzip",
                                        compression_opts=9,
                                        dtype=np.float64)

        saveFile.close()
