from Reader import Reader
#GUI libraries
from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import sys
import numpy as np
from CircBuffVisualiser import BufferStateVisualiser

'''
Animated version of GradientPlot
'''
class ReaderGradientPlotAnimated(Reader):

    #modeVis  - set visualisation mode on or of
    def run(self):
        app = QtWidgets.QApplication(sys.argv)
        form = SecondWindowAnimated()
        print(self.name)
        # set source of data
        form.setSource(self.shared_array, self.lockArrInd , self.noOfSamples, self.noOfRecords, self.modeVis, self)
        # update
        form.show()
        sys.exit(app.exec_())

'''
Animated version of Second Window
Requires: multiple of 16 as number of records.
'''

class SecondWindowAnimated(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SecondWindowAnimated, self).__init__(parent)
        self.setupUi(self)

    def setSource(self, shared_array, lockArrInd , noOfSamples, noOfRecords, modeVis, requestor):

        # arbitrary number of 16 chunks of data - to animate 16 times per frame
        self.recordsPerChange = int(noOfRecords / 16)
        #Array of 16 sets of records - work like fifo - first in first out and plot the state
        data_16bit = np.zeros((noOfRecords * noOfSamples), dtype=np.int16)  # prefilled array
        data_16bit = np.ascontiguousarray(data_16bit, dtype=np.int16)  # making it contiguous
        data_16bit = np.reshape(data_16bit, (16, (int(noOfRecords * noOfSamples / 16))))
        self.ArrayOfChunksOfImage = data_16bit
        # print(np.shape(self.ArrayOfChunksOfImage))
        # print(self.recordsPerChange)

        self.shared_array = shared_array
        self.lockArrInd = lockArrInd
        self.noOfSamples = noOfSamples

        self.noOfRecords = noOfRecords
        #visualisation of the buffer on/off
        self.modeVis = modeVis
        self.requestor=requestor
        self.indexRead = 0

        if(self.modeVis==True):
            self.bufferStVis = BufferStateVisualiser(self.lockArrInd, 'Vis', self.shared_array, self.noOfSamples, self.noOfRecords)
        '''
                Initialize variables necessary for the circular buffer.
        '''
        # populate all the chunks and plot and test
        data = 0
        for x in range(0, 16):
            data=data+220
            for y in range(int(noOfRecords * noOfSamples/16)):
                self.ArrayOfChunksOfImage[x][y]=data


        self.final = self.ArrayOfChunksOfImage.reshape(int(self.noOfRecords), int( self.noOfSamples))

        #change of autorange
        self.im_widget.setImage(self.final, autoLevels=False,
                            levels=[0, 28000], autoHistogramRange=False)
        # self.im_widget.setImage(self.final)

        # Get the colormap from matplotlib
        # colormap = matplotlib.get_cmap("plasma")  # cm.get_cmap("CMRmap")
        # colormap._init()
        # lut = (colormap._lut * 1000).view(np.ndarray)  # Convert matplotlib colormap from 0-1 to 0 -255 for Qt
        # self.im_widget.getImageItem().setLookupTable(lut)
        #uncomment for performance optimization
        self.im_widget.getImageItem().setAutoDownsample(True)
        #to turn on antialiasing
        #pg.setConfigOptions(antialias=True)


        # very important - changing the mode from printing the whole packet to acquiring only 1/16 and animating
        #reinitialize containers and variables
        print(self.requestor.noOfRecords)
        self.requestor.noOfRecords = self.recordsPerChange
        print(self.requestor.noOfRecords)
        self.requestor.oneFrame = self.noOfSamples * self.requestor.noOfRecords
        self.requestor.data_16bit = np.zeros(self.requestor.oneFrame, dtype=np.int16)  # prefilled array
        self.requestor.data_16bit = np.ascontiguousarray(self.requestor.data_16bit, dtype=np.int16)  # making it contiguous




    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)

        self.im_widget = pg.ImageView(self)
        # uncomment to hide histogram

        self.im_widget.ui.histogram.hide()
        view = self.im_widget.getView()
        view.setRange(xRange=[1000, 16000])


        self.im_widget.setPredefinedGradient('flame')

        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.initialisationFigure()
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.majFigure)
        self.timer.start(100)
        self.timer2 = QtCore.QTimer(self)
        self.timer2.timeout.connect(self.NumberRefreshPerSecond)
        self.timer2.start(500)

    def NumberRefreshPerSecond(self):
        # print(self.count)
        self.count = 0


    def majFigure(self):
        self.count = self.count + 1

        if(self.modeVis==True):
            self.data_16bit = self.requestor.readWithBufVis(self.bufferStVis)

        else:
            self.data_16bit = self.requestor.read()




        #self.final[:-self.recordsPerChange] = self.final[self.recordsPerChange:]
        self.final = np.roll(self.final,-self.recordsPerChange,axis=0)

        #add new
        # print(np.shape(self.final))
        # print(np.shape(self.data_16bit))
        # print(self.recordsPerChange*15)
        self.final[self.recordsPerChange*15:]= np.reshape(self.data_16bit,(self.recordsPerChange,self.noOfSamples))


        #reshaping in this place is unoptimal
        ###         four different of ways f shifting array of 16 vectors - ineffective          ###
        # self.ArrayOfChunksOfImage[0:15] = self.ArrayOfChunksOfImage[1:16]
        # self.ArrayOfChunksOfImage = np.roll(self.ArrayOfChunksOfImage, 1)
        # self.ArrayOfChunksOfImage[:-1] = self.ArrayOfChunksOfImage[1:]
        # for x in range(0, 15):
        #     self.ArrayOfChunksOfImage[x]=self.ArrayOfChunksOfImage[x+1]
        # final = self.ArrayOfChunksOfImage.reshape(int(self.noOfRecords), int(self.noOfSamples))
        #some optimizations during setting the image
        # print(np.min(self.final))
        # print(np.max(self.final))

#        self.im_widget.setImage(self.final, autoLevels=False,
#                                levels=[-32767, 32767], autoHistogramRange=False)

        self.im_widget.setImage(self.final, autoLevels=True, autoHistogramRange=True)

    def initialisationFigure(self):
        self.count = 0
        self.im_widget.show()

    def closeEvent(self, event):
        self.timer.stop()