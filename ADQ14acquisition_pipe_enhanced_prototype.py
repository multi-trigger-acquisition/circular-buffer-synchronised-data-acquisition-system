'''
<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
>>>>>>> 2cb50c2d56c27eb6b8d6378ce508fe26ae7367eb
Author: Pawel Kozlowski
Supervision: Alexandre Lasheen
Affiliation: CERN, Warsaw University Of Technology

Disclaimer : the data acquisition part of the code was written and belongs to SP-Devices (Teledyne Signal Processing Devices Sweden AB).

<<<<<<< HEAD
>>>>>>> 2cb50c2... Initial commit
=======
>>>>>>> 2cb50c2d56c27eb6b8d6378ce508fe26ae7367eb
Comments:
    
This mode is prefered if acquiring small records, the total size is higher than ADQ DRAM - DRAM utilised!
Data rate has to be kept below the speed  of the read out process - with usb3 impossible to get a good quality
Record size has to be smaller than FPGA FIFO.
<<<<<<< HEAD
<<<<<<< HEAD
=======


>>>>>>> 2cb50c2... Initial commit
=======


>>>>>>> 2cb50c2d56c27eb6b8d6378ce508fe26ae7367eb
'''
import numpy as np
import ctypes as ct
import matplotlib.pyplot as plt
import sys
import time
import os


import sys, random, matplotlib
from PyQt5 import QtCore, QtGui, QtWidgets
from multiprocessing import Pool, Process, Lock, Queue, Pipe
import time
import sys
import pyqtgraph as pg
sys.path.insert(1, os.path.dirname(os.path.realpath(__file__))+'/..')
from modules.example_helpers import *

def writer(count, pipe):
    ## assign the pipe
    p_output, p_input = pipe
    print('hello count ', count)
    sys.stdout.flush()
    p_output.close()
    
    # Record settings
    number_of_records  = 100000
    samples_per_record = 2300 # 2300
    
    # Print metadata in headers
    print_headers = False
    
    # DMA transfer buffer settings
    transfer_buffer_size = 65536
    num_transfer_buffers = 8
    
    # DMA flush timeout in seconds
    flush_timeout        = 0.5
    
    # Load ADQAPI
    ADQAPI = adqapi_load()
    
    # Create ADQControlUnit
    adq_cu = ct.c_void_p(ADQAPI.CreateADQControlUnit())
    
    # Enable error logging from ADQAPI
    ADQAPI.ADQControlUnit_EnableErrorTrace(adq_cu, 3, '.')
    
    # Find ADQ devices
    ADQAPI.ADQControlUnit_FindDevices(adq_cu)
    n_of_ADQ  = ADQAPI.ADQControlUnit_NofADQ(adq_cu)
    print('Number of ADQ found:  {}'.format(n_of_ADQ))
    
    # Exit if no devices were found
    if n_of_ADQ < 1:
        print('No ADQ connected.')
        ADQAPI.DeleteADQControlUnit(adq_cu)
        adqapi_unload(ADQAPI)
        sys.exit(1)
    
    # Select ADQ
    if n_of_ADQ > 1:
        adq_num = int(input('Select ADQ device 1-{:d}: '.format(n_of_ADQ)))
    else:
        adq_num = 1
    
    print_adq_device_revisions(ADQAPI, adq_cu, adq_num)
    
    # Set clock source
    ADQ_CLOCK_INT_INTREF = 0
    ADQAPI.ADQ_SetClockSource(adq_cu, adq_num, ADQ_CLOCK_INT_INTREF)
    
    # Maximum number of channels for ADQ14 FWPD is four but we use only one
    max_number_of_channels = 1
    
    # check if usb3 is on
    usb3 = ADQAPI.ADQ_IsUSB3Device(adq_cu, adq_num)
    if(usb3 == 1):
        print('USB3 connected')
    else:
        print('USB3 not connected')
    
    # Setup test pattern
    #     0 enables the analog input from the ADCs
    #   > 0 enables a specific test pattern
    # Note: Default is to enable a test pattern (4) and disconnect the
    #       analog inputs inside the FPGA.
    ADQAPI.ADQ_SetTestPatternMode(adq_cu, adq_num, 0)
    
    '''
    Set gain and offset.
    unsigned int SetGainAndOﬀset (unsigned char Channel, int Gain, int Oﬀset)
    '''
    ch=1
    ADQAPI.ADQ_SetGainAndOffset(adq_cu, adq_num,ch,
                                             8192,
                                             0)
    '''
    Set bias offset - The DC-biasing circuitry is heavily ﬁltered, and a change in bias level will typically take around 3-4
    seconds to take eﬀect. Also limits settling time to 100ms.
    '''
    adjustable_bias = 1000 # Codes
    ADQAPI.ADQ_SetAdjustableBias(adq_cu, adq_num,1, adjustable_bias)
    
    
    # Set trig mode
    SW_TRIG = 1
    EXT_TRIG_1 = 2
    EXT_TRIG_2 = 7
    EXT_TRIG_3 = 8
    LVL_TRIG = 3
    INT_TRIG = 4
    LVL_FALLING = 0
    LVL_RISING = 1
    trig_type = EXT_TRIG_1
    success = ADQAPI.ADQ_SetTriggerMode(adq_cu, adq_num, trig_type)
    if (success == 0):
        print('ADQ_SetTriggerMode failed.')
    success = ADQAPI.ADQ_SetLvlTrigLevel(adq_cu, adq_num, 0)
    if (success == 0):
        print('ADQ_SetLvlTrigLevel failed.')
    success = ADQAPI.ADQ_SetTrigLevelResetValue(adq_cu, adq_num, 1000)
    if (success == 0):
        print('ADQ_SetTrigLevelResetValue failed.')
    success = ADQAPI.ADQ_SetLvlTrigChannel(adq_cu, adq_num, 1)
    if (success == 0):
        print('ADQ_SetLvlTrigChannel failed.')
    success = ADQAPI.ADQ_SetLvlTrigEdge(adq_cu, adq_num, LVL_RISING)
    if (success == 0):
        print('ADQ_SetLvlTrigEdge failed.')
    
    # Setup acquisition
    channels_mask = 0x1
    # noOfHoldOffSamples may be useful to shift every acquisition for some samplesto center on the beam profile
    noOfHoldOffSamples = 0
    # may be useful when triggering on the threshold of the beam profile
    noOfPreTriggerSamples = 0
    ADQAPI.ADQ_TriggeredStreamingSetup(adq_cu, adq_num, number_of_records, samples_per_record, noOfPreTriggerSamples, noOfHoldOffSamples, channels_mask)
    
    success = ADQAPI.ADQ_SetStreamStatus(adq_cu, adq_num, 1);
    if (success == 0):
        print('setting stream status to 1 (turning on) failed.')
    #success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 1,0);
    #if (success == 0):
    #    print('using DRAM as FIFO failed')
    #success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 2,1);
    #if (success == 0):
    #    print('turning off headers - setting to raw data failed')
    #success = ADQAPI.ADQ_SetStreamConfig(adq_cu, adq_num, 3,1);
    #if (success == 0):
    #    print('setting the stream channel mask failed')
    
    # Get number of channels from device
    number_of_channels = 1 # ADQAPI.ADQ_GetNofChannels(adq_cu, adq_num)
    
    # Setup size of transfer buffers
    print('Setting up streaming...')
    ADQAPI.ADQ_SetTransferBuffers(adq_cu, adq_num, num_transfer_buffers, transfer_buffer_size)
    
    # Start streaming
    print('Collecting data, please wait...')
    ADQAPI.ADQ_StopStreaming(adq_cu, adq_num)
    ADQAPI.ADQ_StartStreaming(adq_cu, adq_num)
    
    # Allocate target buffers for intermediate data storage one buffer for one channel
    # 
    Int16Arr = ct.c_int16 * transfer_buffer_size
    # pointer to the array
    Int16ArrPtr = ct.POINTER(Int16Arr)
    # array of pointers (of buffers)
    Int16ArrPtrArr = Int16ArrPtr * number_of_channels
    # new type is defined (human readable name)
    BUFFERS = Int16ArrPtrArr
    # instantiate the type  
    target_buffers = BUFFERS()
    print('len of target buffers', len(target_buffers))
    
    for bufp in target_buffers:
        bufp.contents = (ct.c_int16*transfer_buffer_size)()
    
    
    # Create some buffers for the full records
    data_16bit = [np.array([], dtype=np.int16)]
    
    ## 2D array of 
    ## Allocate target buffers for headers
    #headerbuf_list = [(HEADER*number_of_records)()]
    ## Create an C array of pointers to header buffers (by ensuring a space)
    #headerbufp_list = ((ct.POINTER(HEADER*number_of_records)))()
    
    # create array of headers
    HeadArr = HEADER*number_of_records
    
    # Allocate target buffers for headers
    # instantiate header array
    HeaderArray = HeadArr()
    headerbuf_list = [HeaderArray for ch in range(number_of_channels)]
    
    # Create an C array of pointers to header buffers
    # create pointer to the array
    HeadArrPtr = ct.POINTER(HeadArr)
    # create array of pointers to headers
    HeadArrPtrArr = HeadArrPtr * number_of_channels
    # new type is defined (human readable name)
    HEADER_BUFFERS = HeadArrPtrArr
    # instantiate the type  
    headerbufp_list = HEADER_BUFFERS()
    # Initiate pointers with allocated header buffers
    for ch,headerbufp in enumerate(headerbufp_list):
        headerbufp.contents = headerbuf_list[ch]
    
    # Create a second level pointer to each buffer pointer,
    # these will only be used to change the bufferp_list pointer values
    headerbufvp_list = [ct.cast(ct.pointer(headerbufp_list[ch]), ct.POINTER(ct.c_void_p)) for ch in range(number_of_channels)]
            
    
    
    # Allocate length output variable
    # make a space for 4 elements and populate the elements
    # create type
    samples_added = (1*ct.c_uint)()
    # populate the type
    for ind in range(len(samples_added)):
      samples_added[ind] = 0
    
    headers_added = (1*ct.c_uint)()
    for ind in range(len(headers_added)):
      headers_added[ind] = 0
    
    header_status = (1*ct.c_uint)()
    for ind in range(len(header_status)):
      header_status[ind] = 0
    
    # Generate triggers if software trig is used
    # why the trigger is not armed beforehand?
    #if (trig_type == 1):
    #  for trig in range(number_of_records):
    #    ADQAPI.ADQ_SWTrig(adq_cu, adq_num)
    #
    #print('Waiting for data...')
    
    # Collect data until all requested records have been recieved
    records_completed = 0
    headers_completed = 0
    records_completed_cnt = 0
    ltime = time.time()
    buffers_filled = ct.c_uint(0)
    
    
    # Preallocate memory
    # entries = range(number_of_records*samples_per_record) # 1 million entries
    # array for 50 temprary records
    temporaryRecords =  2000
    
#    lastElOfTempArray = temporaryRecords*samples_per_record-1
#    last_sample_of_record = samples_per_record-1
#    last_record_of_acquisition = temporaryRecords-1
#    
#    entries = temporaryRecords*samples_per_record
#    data_16bit = np.zeros((len(entries)),dtype=np.int16) # prefilled array
#    data_16bit = data_16bit.reshape(temporaryRecords, samples_per_record)
#    # the general counter used when filling the data buffer
#    general_counter = 0
#    record_counter = 0
#    record_sample_counter = 0

    
    general_counter = 0
    entries = temporaryRecords*samples_per_record
    #two tables of iteration
    next_iter_t = [None,None]
    next_iter_t[0] = np.zeros((entries),dtype=np.int16)
    next_iter_t[1] = np.zeros((entries),dtype=np.int16)
    # what is the content already loaded to the next iter array
    next_iter_taken = [0,0]  
    
    
    # Read out data until records_completed for ch A is number_of_records
    while (number_of_records > records_completed):
      buffers_filled.value = 0
      collect_result = 1
      poll_time_diff_prev = time.time()
      # Wait for next data buffer
      while ((buffers_filled.value == 0) and (collect_result)):
        collect_result = ADQAPI.ADQ_GetTransferBufferStatus(adq_cu, adq_num,
                                                            ct.byref(buffers_filled))
        poll_time_diff = time.time()
    
        if ((poll_time_diff - poll_time_diff_prev) > flush_timeout):
          # Force flush
          print('No data for {}s, flushing the DMA buffer.'.format(flush_timeout))
          status = ADQAPI.ADQ_FlushDMA(adq_cu, adq_num);
          print('ADQAPI.ADQ_FlushDMA returned {}'.format(adq_status(status)))
          poll_time_diff_prev = time.time()
    
    
      # Fetch data and headers into target buffers
      status = ADQAPI.ADQ_GetDataStreaming(#  adq_cu, adq_num - handles to the device cu - control unit
                                           adq_cu, adq_num,
                                           target_buffers,
                                           #  The data and metadata is delivered to the user buﬀers: target_buﬀers and target_headers, respectively.
                                           headerbufp_list,
                                           #  suppress data collection from specific channels
                                           channels_mask,
                                           # byref works almost like pointer() , but with when we dont want to access it from python
                                           # samples_added - how many samples that were added to the user data buffers
                                           # This allows the user to
                                           # increment the data buﬀer pointers accordingly before the next call to the GetDataStreaming() function.
                                           ct.byref(samples_added),
                                           # headers_added and header_status provide the user with information to determine if the header
                                           # buﬀer pointers should be incremented.
                                           # The parameter headers_added contains the number of initialized
                                           # headers as a result of this call to GetDataStreaming()
                                           ct.byref(headers_added),
                                           ct.byref(header_status))
                                           # The header_status parameter is used to denote if the
                                           # last header is complete or not. E.g. a return value of headers_added = 4 and header_status = 1 signiﬁes 4
                                           # complete headers, while headers_added = 4 and header_status = 0 would signify 3 complete headers and one
                                           # incomplete header. 
      
      
      
      if status == 0:
        print('GetDataStreaming failed!')
        sys.exit()
      ch=0
      # for ch in range(number_of_channels):
      if (headers_added[ch] > 0):
          # The last call to GetDataStreaming has generated header data
          if (header_status[ch]):
            headers_done = headers_added[ch]
          else:
            # One incomplete header
            headers_done = headers_added[ch]-1
          # Update counter counting completed records
          headers_completed += headers_done
    
          # Update the number of completed records if at least one header has completed
          if (headers_done > 0):
            records_completed = headerbuf_list[ch][headers_completed-1].RecordNumber + 1
    
          # Update header pointer so that it points to the current header
          headerbufvp_list[ch].contents.value += headers_done*ct.sizeof(headerbuf_list[ch]._type_)
          if (headers_done > 0) and (records_completed-records_completed_cnt) > 1000:
              dtime = time.time()-ltime
              if (dtime > 0):
                  print('{:d} {:.2f} MB/s'.format(records_completed,
                        ((samples_per_record
                          *2
                          *(records_completed-records_completed_cnt))
                          /(dtime))/(1024*1024)))
              sys.stdout.flush()
              records_completed_cnt = records_completed
              ltime = time.time()
    
      if (samples_added[ch] > 0):
          # Copy channel data to continuous buffer
          data_buf          = np.frombuffer(target_buffers[ch].contents, dtype=np.int16, count=samples_added[ch])

          #print(len(data_buf))
          print(samples_added[ch])
          # highly uneffective 
          # data_16bit    = np.append(data_16bit, data_buf)
          '''
          the version doesn't rely on vectorised way of performance improvement - data consumption - 4.50 MB/s
          '''
#          for idx, entry in enumerate(data_buf):
#              # assign new entry to 2d array
#              data_16bit[record_counter][record_sample_counter] = entry
#              
#              # increment if it is not yet the end of array of records, otherwise reset the general counter and send image to the screen
#              if(general_counter==lastElOfTempArray):
#                  general_counter=0
#                  p_input.send(data_16bit)
#              else:
#                  general_counter+= 1
#              
#              # if in this moment is the last sample of record reset the counter and start new record
#              if(record_sample_counter==last_sample_of_record):
#                  record_sample_counter = 0
#                  record_counter += 1
#              else:
#                  record_sample_counter+= 1
#              
#              if(record_counter==last_record_of_acquisition):
#                  record_counter = 0
          '''
          The version that rely on vectorised way of performance improvement - data consumption - up to 150 MB/s.
          The transfer rate depends on - acquisition of samples by hardware (number of triggers and data length)
          and of course the consumption by the reader (PyQT gui)
          '''
          #check space in the current iter. array
          #e.g. 2040 - 65
          toNextIteration =  samples_added[ch] + next_iter_taken[general_counter] - entries 
          #print(toNextIteration)
          if(toNextIteration>=0):
              #take only the part of data that is needed to completely fill the current array
              LastIndex = samples_added[ch]-toNextIteration
              next_iter_t[general_counter][next_iter_taken[general_counter]:entries]=data_buf[0:LastIndex]
              #if the consumption is too slow it will couse buffer of digitizer overflow. In pipe consumer drives the throughput.
              p_input.send(next_iter_t[general_counter])
              #update taken
              next_iter_taken[general_counter]=0
              #swap from 0 to 1 and opposite
              general_counter = 1 - general_counter
              #add to next iteration array
              next_iter_t[general_counter][0:toNextIteration]=data_buf[LastIndex:samples_added[ch]]
              #update taken
              next_iter_taken[general_counter]=toNextIteration
          else:#stay in the same iteration , just accumulate some more samples
              #add content to the iteration array
              #print(next_iter_taken[general_counter])
              to=next_iter_taken[general_counter]+samples_added[ch]
              next_iter_t[general_counter][next_iter_taken[general_counter]:to]=data_buf[0:samples_added[ch]]
              #update taken
              next_iter_taken[general_counter]=next_iter_taken[general_counter] + samples_added[ch]
              #print('add')
          




class SecondWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SecondWindow, self).__init__(parent)
        self.setupUi(self)
        
    def setPipe(self,pipe):
        self.p_output, self.p_input = pipe
        self.p_input.close()

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(800, 600)

        self.im_widget = pg.ImageView(self)
        # uncomment to hide histogram
        
        self.im_widget.ui.histogram.hide()
        view = self.im_widget.getView()
        view.setRange(xRange=[1000,16000])
        self.im_widget.setPredefinedGradient('flame')
        
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.im_widget)

        self.initialisationFigure()

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.majFigure)
        self.timer.start(100)

        self.timer2 = QtCore.QTimer(self)
        self.timer2.timeout.connect(self.NumberRefreshPerSecond)
        self.timer2.start(1000)

    def NumberRefreshPerSecond(self):
        #print(self.count)
        self.count = 0
        

    def majFigure(self):
        self.count = self.count + 1

        temporaryRecords = 2000
        samples_per_record = 2300
        print('receive')
        data = self.p_output.recv()
        
        self.im_widget.setImage((data.reshape(temporaryRecords, samples_per_record)),autoLevels=False,levels=[1000,28000],autoHistogramRange=False)
        

    def initialisationFigure(self):
        self.count = 0
        self.im_widget.show()

    def closeEvent(self, event):
        self.timer.stop()


def reader(count, pipe):
    app = QtWidgets.QApplication(sys.argv)
    #print(5)
    form = SecondWindow()
    form.setPipe(pipe)
    #print(6)
    form.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    temporaryRecords=2000
    samples_per_record=2300
    #initialise synchronisation mechanisms
    p_output, p_input = Pipe()
    print(1)
     # Send a lot of stuff to consumer 
    count=100
    print(2)
    time.sleep(3)
    reader_r = Process(target=reader, args=(count, (p_output, p_input),))
    reader_r.daemon = True
    reader_r.start() 
    
    writer(count, (p_output, p_input))

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    