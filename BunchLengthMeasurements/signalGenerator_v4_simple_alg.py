from numpy import *
import numpy as np
import io as io
import matplotlib.pyplot as plt
from scipy import signal


bunchLen=100
noOfBunches=4
distance=400
baseline = 3000
'''
Bunches
'''
t = np.linspace(-1, 1,  bunchLen, endpoint=False)

## Gaussian signal from numpy package
# i, q, e = signal.gausspulse(t, fc=5, retquad=True, retenv=True)

## gaussian signal from binomial form
plt.figure()
Tau=2
mi=1.5
e = (1 - 4*(t/Tau)**2)**mi
plt.plot(e, '-')
#
plt.show()

#real component, imaginary component, and envelope for a 5 Hz pulse, sampled at 100 Hz for 2 seconds:
#plt.plot(t, i, t, q, t, e, '--')
#start = int(bunchLen/2)
#stop = int((2*bunchLen)-(bunchLen/2))
#eCut=e[start:stop]

#the baseline has to be moved a bit
#max 16384
eCut=e
eCut=eCut*(16383 - baseline) #-4000 for safety
#add baseline
eCut=eCut + baseline
#skewed hostogram
plt.figure()
plt.plot(eCut, '-')
#
plt.show()
#inverted parabola

'''
Distance
'''
dist = [int(baseline)]*distance

'''
Timeline
'''
t = np.linspace(0, 2, bunchLen, endpoint=False)

plt.plot(t, eCut, '--')

plt.figure()

'''
Combination
'''
e2 = np.concatenate((dist,eCut,dist,eCut,dist,eCut,dist,eCut))
#adding signal to noise
amountOfNoise=200
snr  =  int(np.max(e2/amountOfNoise))
noise = np.random.normal(0,snr, e2.shape)
#noise = noise + baseline
#add noise and change the type to the one consistent with digitizer
noise=noise.astype(int).astype(np.uint16)
e2=e2.astype(int).astype(np.uint16)
'''
-White noise has equally random amounts of all frequencies
-“Colored” noise has unequal amount for different frequencies
-Since signals often have more low frequencies than high, the effect of
white noise is usually greatest for high frequencies

Therefore bunch shouldn't be covered with noise to the same
'''
e2 = e2 + noise


#decibel attenuation dBv = 20log10(Vout/Vin)
#so to reduce the signal by 2 dB (attenuation of cable) multiply the amplitude of each sample by:
attenuation=-1
#gain vout/vin => 
gain = 10 ** (attenuation / 10)
e2=e2*gain


t2 = np.linspace(0, (noOfBunches*bunchLen) + (distance*noOfBunches), (noOfBunches*bunchLen) + (distance*noOfBunches), endpoint=False)
plt.plot(t2, e2, '--')

'''
Output file
'''

f = open("signalForTB.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("%d " %noOfBunches)
f.write("%d\n" %bunchLen)
#writing waveform
for i in e2:
     f.write("%d\n" % (i+1))
f.close()


'''
Plot diff, gradient
'''

e2=e2.astype(int)
noEl=int(noOfBunches*bunchLen) + (distance*noOfBunches)
tempArray=np.array([0]*noEl)
#gx = datamatrix[2700]
tempArray[1:] = np.diff(e2)
tempArray[0] = tempArray[1]

diffSignal=tempArray
gradientSignal = np.gradient(e2)

#downsample
#diffSignal=diffSignal[::5]
#gradientSignal=gradientSignal[::5]

plt.figure()
plt.plot(diffSignal, '-')
plt.xlabel('t [ns]')
plt.ylabel('diff(Signal)')
#
plt.show()

plt.figure()
plt.plot(gradientSignal, '-')
plt.xlabel('t [ns]')
plt.ylabel('grad(Signal)')
#
plt.show()

'''
Store real data to testbench readable format
'''
noOfSamples=2240
noOfRecs=10000
data = np.load('initialName0.npy')
datamatrix = np.reshape(data,(noOfRecs,noOfSamples))
wave =  np.concatenate((datamatrix[6250],datamatrix[6251]))
wave=(wave+28920)
wave = np.array(wave,dtype=uint16)
plt.figure()
plt.plot(wave, '-')
plt.xlabel('t [ns]')
plt.ylabel('grad(Signal)')
plt.show()

#set manually what is expected
noOfBunches=4
bunchLen=80

f = open("signalForTB.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("%d " %noOfBunches)
f.write("%d\n" %bunchLen)
#writing waveform
for i in wave:
     f.write("%d\n" % (i+1))
f.close()

'''
Convert from tomoscope file into testbench readable format
'''

from numpy import *
import numpy as np
import io as io
import matplotlib.pyplot as plt
from scipy import signal

from io import StringIO  

filename="C2595allb_C40-77-78_MHFB_on_C80-88-89_BU4_3x3.5kV_4_1890E10ppp.dat"
data = np.loadtxt(filename, delimiter="\n")

dataRes=np.reshape(data,(100,8000))
#shift baseline up
baseline = np.min(dataRes)
dataRes = -baseline+dataRes
#normalize 0-1
dataRes = dataRes/np.max(dataRes)
#max range on 16 bits - 65536 
dataRes = 65535 * dataRes# Now scale by 255
data16 = dataRes.astype(np.uint16)

#plot
dataFinal = np.array(data16[0])
plt.figure()
plt.plot(dataFinal, '-')
plt.xlabel('t [ns]')
plt.ylabel('grad(Signal)')
plt.show()

#save
f = open("signalForTB.txt", "w+", encoding="utf-8")
#write noOfBunches
#write start and stop points of the bunches
f.write("%d " %noOfBunches)
f.write("%d\n" %bunchLen)
#writing waveform
for i in dataFinal:
     f.write("%d\n" % (i+1))
f.close()



#t = np.linspace(0, 2, 2 * 100, endpoint=False)
#
#t2 = np.concatenate((t, t))
#s2 = np.concatenate((e, e))
#
#plt.plot(t2, s2, '--')





#class SignalGenerator:
#
#    def __init__(self, name):
#        self.name = name
#        self.tricks = []    # creates a new empty list for each dog
#
#    def add_trick(self, trick):
#        self.tricks.append(trick)
#        
#        
#class Signal:
#
#    def __init__(self, name):
#        self.name = name
#        self.tricks = []    # creates a new empty list for each dog
#
#    def add_trick(self, trick):
#        self.tricks.append(trick)
#
#
