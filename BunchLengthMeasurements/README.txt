This folder contains scripts which were used for evaluation of bunch length and position. 
The files beginning with guilenmeas (GUI for bunch length measurement) were used to visualise
the bunch length live and the best algorithm was implemented in circular buffer based acquisition
tool. The algorithm is based on FSM which changes state after crossing threshold of derivative
of original signal. For more reliability one can use filtering but as the operation is time
consuming I have not included the improvement in the final version.